using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;
using Data_Access_Layer;
using System.Collections;
using Backend;

namespace Buisness_Layer
{
    public class Buisness : Buisness_Interface
    {
        private Data_Access_Interface Dal;
        //private List<int> bestSellersProductsQuantity = new List<int>();
        //public List<int> BestSellersProductsQuantity
        //{
        //    get { return bestSellersProductsQuantity; }
        //}

        public Buisness(Data_Access_Interface Dal)
        {
            this.Dal = Dal;
        }

        #region Employee

        public ReturnMessage EmployeeAdd(Employee employee)
        {
            if (EmployeeGet(employee, -1, -1).Count != 0)
                return new ReturnMessage(false, Properties.StringResource.EmployeeExist);

            if (employee.DepartmentID != -1 && Dal.Get<Department>(employee.DepartmentID) == null)
                return new ReturnMessage(false, Properties.StringResource.DepartmentNotExist);

            if (employee.SuperVisorID != -1 && Dal.Get<Employee>(employee.SuperVisorID) == null)
                return new ReturnMessage(false, Properties.StringResource.EmployeeSupervisorNotExist);

            Dal.Add<Employee>(employee);
            return new ReturnMessage(true, Properties.StringResource.EmployeeAdded);    
        }

        public bool EmployeeExist(int teudatZ)
        {
            if (Dal.Get<Employee>(teudatZ) != null)
                return true;
            return false;
        }
        public ReturnMessage EmployeeEdit(Employee employee)
        {
            if (employee.DepartmentID != -1 && Dal.Get<Department>(employee.DepartmentID) == null)
                return new ReturnMessage(false, Properties.StringResource.DepartmentNotExist);

            if (employee.TehudatZehut != -1 && Dal.Get<Employee>(employee.SuperVisorID) == null)
                return new ReturnMessage(false, Properties.StringResource.EmployeeSupervisorNotExist);

            if(Dal.Get<Employee>(employee.TehudatZehut) == null)
                return new ReturnMessage(false, Properties.StringResource.EmployeeNotExist);

            Dal.Edit<Employee>(employee);
            return new ReturnMessage(true, Properties.StringResource.EmployeeEdited);
                
        }

        public Employee EmployeeGetById(int id)
        {
            return Dal.Get<Employee>(id);
        }

        public List<Employee> EmployeeGet(Employee employee, int salaryFrom, int salaryTo)
        {
            List<TypeValue<EmployeeFields>> employeeFieldsList = new List<TypeValue<EmployeeFields>>();
            if (employee.TehudatZehut != -1)
                employeeFieldsList.Add(new TypeValue<EmployeeFields>(EmployeeFields.TehudatZehut, employee.TehudatZehut));
            if (!employee.FirstName.Equals(""))
                employeeFieldsList.Add(new TypeValue<EmployeeFields>(EmployeeFields.firstName, employee.FirstName));
            if (!employee.LastName.Equals(""))
                employeeFieldsList.Add(new TypeValue<EmployeeFields>(EmployeeFields.lastName, employee.LastName));
            if (employee.DepartmentID != -1)
                employeeFieldsList.Add(new TypeValue<EmployeeFields>(EmployeeFields.departmentID, employee.DepartmentID));
            if (employee.Salary != -1)
                employeeFieldsList.Add(new TypeValue<EmployeeFields>(EmployeeFields.salary, employee.Salary));
            if (!employee.Gender.Equals(Employee.GenderTypes.Empty))
                employeeFieldsList.Add(new TypeValue<EmployeeFields>(EmployeeFields.gender, employee.Gender));
            if (employee.SuperVisorID != -1)
                employeeFieldsList.Add(new TypeValue<EmployeeFields>(EmployeeFields.supervisor, employee.SuperVisorID));

            return (salaryFrom != -1 && salaryTo != -1) ? (from e in Dal.Get<Employee, EmployeeFields>(employeeFieldsList) where (e.Salary >= salaryFrom && e.Salary <= salaryTo) select e).ToList() : Dal.Get<Employee, EmployeeFields>(employeeFieldsList);
        }

        public Employee EmployeeGet(int userID)
        {
            return Dal.Get<Employee>(userID);
        }

        public ReturnMessage EmployeeRemove(int TehudatZehut)
        {
            if (TehudatZehut != -1 || Dal.Get<Employee>(TehudatZehut) == null)
                return new ReturnMessage(false, Properties.StringResource.EmployeeNotExist);

            Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.supervisor, Dal.Get<Employee>(TehudatZehut).SuperVisorID) });

            // changes all the employees supervisor to 'no supervisor'
            List<Employee> studentsEmployees = Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.supervisor, Dal.Get<Employee>(TehudatZehut).SuperVisorID) });
            foreach(Employee studentsEmployee in studentsEmployees)
            {
                studentsEmployee.SuperVisorID = -1;
                Dal.Edit<Employee>(studentsEmployee);
            }
            
            Dal.Remove<Employee>(TehudatZehut);
            return new ReturnMessage(true, Properties.StringResource.EmployeeRemoved);
        }

        #endregion

        #region ClubMember

        public ReturnMessage ClubMemberAdd(ClubMember clubMember)
        {
            if (ClubMemberGet(clubMember, DateTime.MinValue, DateTime.MinValue, -1, -1, -1).Count == 0)
            {
                Dal.Add<ClubMember>(clubMember);
                return new ReturnMessage(true, Properties.StringResource.ClubMemberAdded);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ClubMemberExist);
        }

        public ReturnMessage ClubMemberEdit(ClubMember clubMember)
        {
            if (Dal.Get<ClubMember>(clubMember.Id) != null)
            {
                Dal.Edit<ClubMember>(clubMember);
                return new ReturnMessage(true, Properties.StringResource.ClubMemberEdited);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ClubMemberExist);
        }
        
        public List<ClubMember> ClubMemberGet(ClubMember clubMember, DateTime fromDateOfBirth, DateTime toDateOfBirth, int year, int month, int day)
        {
            List<TypeValue<ClubMemberFields>> clubMemberFieldsList = new List<TypeValue<ClubMemberFields>>();
            if (clubMember.TehudatZehut != -1)
                clubMemberFieldsList.Add(new TypeValue<ClubMemberFields>(ClubMemberFields.teudatZehutID, clubMember.TehudatZehut));
            if (!clubMember.FirstName.Equals(""))
                clubMemberFieldsList.Add(new TypeValue<ClubMemberFields>(ClubMemberFields.firstName, clubMember.FirstName));
            if (!clubMember.LastName.Equals(""))
                clubMemberFieldsList.Add(new TypeValue<ClubMemberFields>(ClubMemberFields.lastName, clubMember.LastName));
            if (!clubMember.Gender.Equals(ClubMember.GenderTypes.Empty))
                clubMemberFieldsList.Add(new TypeValue<ClubMemberFields>(ClubMemberFields.gender, clubMember.Gender));
            if (!clubMember.Payment.PaymentMethodType.Equals(ClubMember.paymentMethodTypes.Empty))
                clubMemberFieldsList.Add(new TypeValue<ClubMemberFields>(ClubMemberFields.payment, clubMember.Payment.PaymentMethodType));

            return (fromDateOfBirth != DateTime.MinValue || toDateOfBirth != DateTime.MinValue || year != -1 || month != -1 || day != -1) 
                ? 
                Dal.GetRangeByDateTime<ClubMember>(-1, -1, fromDateOfBirth, toDateOfBirth, year, month, day, -1, -1).Intersect(Dal.Get<ClubMember, ClubMemberFields>(clubMemberFieldsList)).ToList() 
                : 
                Dal.Get<ClubMember, ClubMemberFields>(clubMemberFieldsList);
        }

        public ClubMember ClubMemberGet(int memberID)
        {
            return Dal.Get<ClubMember>(memberID);
        }

        public ReturnMessage ClubMemberRemove(int clubMemberMemberID)
        {
            if (clubMemberMemberID != -1 && Dal.Get<ClubMember>(clubMemberMemberID) != null)
            {
                // move all transactions from the club member to the default club member
                List<int> transactionList = Dal.Get<ClubMember>(clubMemberMemberID).TransactionHistory;
                ClubMember notRegisteredClubMember = Dal.Get<ClubMember>(0);
                foreach(int currentTransactionID in transactionList)
                    notRegisteredClubMember.TransactionHistory.Add(currentTransactionID);
                Dal.Edit<ClubMember>(notRegisteredClubMember);

                Dal.Remove<ClubMember>(clubMemberMemberID);

                return new ReturnMessage(true, Properties.StringResource.ClubMemberRemoved);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ClubMemberNotExist);
        }

        public List<Transaction> ClubMemberGetTransactions(int clubMemberMemberID)
        {
            if (Dal.Get<ClubMember>(clubMemberMemberID) != null)
            {
                ClubMember clubMemberFromDB = Dal.Get<ClubMember>(clubMemberMemberID);
                List<Transaction> clubMemberTransactionList = new List<Transaction>();

                foreach (int currentTransactionID in clubMemberFromDB.TransactionHistory)
                    clubMemberTransactionList.Add(Dal.Get<Transaction>(currentTransactionID));
                return clubMemberTransactionList;
            }
            else
                return new List<Transaction>();
        }

        #endregion

        #region Customer

        public ReturnMessage CustomerAdd(Customer customer)
        {
            if (CustomerGet(customer).Count == 0)
            {
                Dal.Add<Customer>(customer);
                return new ReturnMessage(true, Properties.StringResource.ClubMemberAdded);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ClubMemberExist);
        }

        public ReturnMessage CustomerEdit(Customer customer)
        {
            if (Dal.Get<Customer>(customer.Id) != null)
            {
                Dal.Edit<Customer>(customer);
                return new ReturnMessage(true, Properties.StringResource.ClubMemberEdited);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ClubMemberExist);
        }
        //public List<Customer> CustomerGetAll()
        //{
        //    return Dal.GetAll<Customer>();
        //}
        public List<Customer> CustomerGet(Customer customer)
        {
            List<TypeValue<CustomerFields>> customerFieldsList = new List<TypeValue<CustomerFields>>();
            if (customer.Id != -1)
                customerFieldsList.Add(new TypeValue<CustomerFields>(CustomerFields.memberID, customer.Id));
            if (!customer.FirstName.Equals(""))
                customerFieldsList.Add(new TypeValue<CustomerFields>(CustomerFields.firstName, customer.FirstName));
            if (!customer.LastName.Equals(""))
                customerFieldsList.Add(new TypeValue<CustomerFields>(CustomerFields.lastName, customer.LastName));
            if (!customer.Payment.PaymentMethodType.Equals(Customer.paymentMethodTypes.Empty))
                customerFieldsList.Add(new TypeValue<CustomerFields>(CustomerFields.payment, customer.Payment.PaymentMethodType));

            return Dal.Get<Customer, CustomerFields>(customerFieldsList);
        }
        public Customer CustomerGet(int customerID)
        {
            return Dal.Get<Customer>(customerID);
        }
        public Customer CustomerGetByUser(User user)
        {
            user = Dal.GetUser(user);
            return Dal.Get<Customer>(user.Id);
        }

        public ClubMember ClubMemberGetByUser(User user)
        {
            user = Dal.GetUser(user);
            return Dal.Get<ClubMember>(user.Id);
        }

        public ReturnMessage CustomerRemove(int customerMemberID)
        {
            if (customerMemberID != -1 && Dal.Get<Customer>(customerMemberID) != null)
            {
                Dal.Remove<Customer>(customerMemberID);

                return new ReturnMessage(true, Properties.StringResource.ClubMemberRemoved);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ClubMemberNotExist);
        }

        public List<Transaction> CustomerGetTransactions(int customerMemberID)
        {
            if (Dal.Get<Customer>(customerMemberID) != null)
            {
                Customer customerFromDB = Dal.Get<Customer>(customerMemberID);
                List<Transaction> customerTransactionList = new List<Transaction>();

                foreach (int currentTransactionID in customerFromDB.TransactionHistory)
                    customerTransactionList.Add(Dal.Get<Transaction>(currentTransactionID));
                return customerTransactionList;
            }
            else
                return new List<Transaction>();
        }

        #endregion

        #region Transaction

        public ReturnMessage TransactionAdd(Transaction transaction)
        {
            // adds transaction to club member
            //Customer currentCustomer;
            //currentCustomer = Dal.Get<Customer>(memberID);
            
            foreach (Transaction.ProductInReceipt receipt in transaction.Receipt)
            {
                Product tempProduct = Dal.Get<Product>(receipt.InventoryID);
                if (tempProduct.StockCount < receipt.Quantity)
                    return new ReturnMessage(false, "There is not enough in the stock of the product " + tempProduct.Name);
            }

            Dal.Add<Transaction>(transaction);

            foreach(Transaction.ProductInReceipt receipt in transaction.Receipt)
            {
                Product tempProduct = Dal.Get<Product>(receipt.InventoryID);
                Dal.Edit<Product>(new Product(receipt.InventoryID, tempProduct.Name, tempProduct.Type, tempProduct.Location, tempProduct.StockCount - receipt.Quantity, tempProduct.Price, tempProduct.ImageURL));
            }

            return new ReturnMessage(true, Properties.StringResource.TransactionAdded);
        }

        public ReturnMessage TransactionEdit(Transaction transaction)
        {
            if (Dal.Get<Transaction>(transaction.TransactionID) != null)
            {
                Dal.Edit<Transaction>(transaction);
                return new ReturnMessage(true, Properties.StringResource.TransactionEdited);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.TransactionExist);
        }

        public List<Transaction> TransactionGet(Transaction transaction, DateTime dateTimeBegin, DateTime dateTimeEnd, int year, int month, int day)
        {
            List<TypeValue<TransactionFields>> transactionFieldsList = new List<TypeValue<TransactionFields>>();
            if (transaction.TransactionID != -1)
                transactionFieldsList.Add(new TypeValue<TransactionFields>(TransactionFields.transactionID, transaction.TransactionID));
            if (!transaction.IsAReturn.Equals(Transaction.ReturnTypes.Empty))
                transactionFieldsList.Add(new TypeValue<TransactionFields>(TransactionFields.isAReturn, transaction.IsAReturn));
            if (!transaction.Payment.PaymentMethodType.Equals(Transaction.paymentMethodTypes.Empty))
                transactionFieldsList.Add(new TypeValue<TransactionFields>(TransactionFields.PaymentMethod, transaction.Payment.PaymentMethodType));

            //return (dateTimeBegin != DateTime.MinValue || dateTimeEnd != DateTime.MinValue || year != -1 || month != -1 || day != -1) 
            //    ?
            //    Dal.GetRangeByDateTime<Transaction>(-1, -1, dateTimeBegin, dateTimeEnd, year, month, day, -1, -1).Intersect(Dal.Get<Transaction, TransactionFields>(transactionFieldsList)).ToList()
            //    :
                return Dal.Get<Transaction, TransactionFields>(transactionFieldsList);
        }

        public Transaction TransactionGetByID(int transactionID)
        {
            return Dal.Get<Transaction>(transactionID);
        }

        public ReturnMessage TransactionRemove(int transactionID, int memberID)
        {
            if (Dal.Get<Customer>(memberID) != null)
            {
                if (Dal.Get<Customer>(memberID) != null)
                {
                    // removes transaction from club member
                    Customer customerWithTransaction = Dal.Get<Customer>(memberID);
                    customerWithTransaction.TransactionHistory.RemoveAt(customerWithTransaction.TransactionHistory.FindIndex(e => e == transactionID));
                    Dal.Edit<Customer>(customerWithTransaction);

                    Dal.Remove<Transaction>(transactionID);
                    return new ReturnMessage(true, Properties.StringResource.TransactionRemoved);
                }
                else
                    return new ReturnMessage(false, Properties.StringResource.TransactionNotExist);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ClubMemberNotExist);
        }

        #endregion

        #region Product

        public ReturnMessage ProductAdd(Product product)
        {
            Dal.Add<Product>(product);
            return new ReturnMessage(true, Properties.StringResource.ProductAdded);
        }

        public ReturnMessage ProductEdit(Product product)
        {
            if (Dal.Get<Product>(product.InventoryID) != null)
            {
                Dal.Edit<Product>(product);
                //updateBestSeller();
                return new ReturnMessage(true, Properties.StringResource.ProductEdited);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ProductNotExist);
        }

        public List<Product> ProductGet(Product product, int priceFrom, int priceTo)
        {
            //updateBestSeller();
            List<TypeValue<ProductFields>> productFieldsList = new List<TypeValue<ProductFields>>();
            if (product.InventoryID != -1)
                productFieldsList.Add(new TypeValue<ProductFields>(ProductFields.inventoryID, product.InventoryID));
            if (!product.Name.Equals(""))
                productFieldsList.Add(new TypeValue<ProductFields>(ProductFields.name, product.Name));
            if (!product.Type.Equals(Product.productTypes.Empty))
                productFieldsList.Add(new TypeValue<ProductFields>(ProductFields.type, product.Type));
            if (product.Location != -1)
                productFieldsList.Add(new TypeValue<ProductFields>(ProductFields.location, product.Location));
            if (product.StockCount != -1)
                productFieldsList.Add(new TypeValue<ProductFields>(ProductFields.stockCount, product.StockCount));
            if (product.Price != -1)
                productFieldsList.Add(new TypeValue<ProductFields>(ProductFields.price, product.Price));

            //return Dal.GetAll<Product>();
            
            return (priceFrom != -1 && priceTo != -1) ? (from e in Dal.Get<Product, ProductFields>(productFieldsList) where (e.Price >= priceFrom && e.Price <= priceTo) select e).ToList() : Dal.Get<Product, ProductFields>(productFieldsList);
        }

        public Product ProductGetByID(int inventoryID)
        {
            //updateBestSeller();
            return Dal.Get<Product>(inventoryID);
        }

        public ReturnMessage ProductRemove(int inventoryID)
        {
            if (Dal.Get<Product>(inventoryID) != null)
            {
                Dal.Remove<Product>(inventoryID);
                return new ReturnMessage(true, Properties.StringResource.ProductRemoved);
            }
            else
                return new ReturnMessage(false, Properties.StringResource.ProductNotExist);
        }

        #endregion

        #region Deparment

        public ReturnMessage DepartmentAdd(Department department)
        {
            if (department.Name == null || department.Name.Equals(""))
                return new ReturnMessage(false, Properties.StringResource.DeparmentYouMustEnterName);

            List<TypeValue<DepartmentFields>> departmentFieldsList = new List<TypeValue<DepartmentFields>>{new TypeValue<DepartmentFields>(DepartmentFields.name, department.Name)};
            if (Dal.Get<Department, DepartmentFields>(departmentFieldsList).Count != 0)
                return new ReturnMessage(false, Properties.StringResource.DepartmentExist);

            Dal.Add<Department>(new Department(-1, department.Name));
            return new ReturnMessage(true, Properties.StringResource.DepartmentAdded);
        }

        public ReturnMessage DepartmentEdit(Department department)
        {
            if (Dal.Get<Department>(department.ID) == null)
                return new ReturnMessage(false, Properties.StringResource.DepartmentNotExist);

            Dal.Edit<Department>(department);
            return new ReturnMessage(true, Properties.StringResource.DepartmentAdded);
        }

        public List<Department> DepartmentGet(Department department)
        {
            Console.WriteLine(department.ID + "::" + department.Name);
            List<TypeValue<DepartmentFields>> departmentFieldsList = new List<TypeValue<DepartmentFields>>();
            if (department.ID != -1)
                departmentFieldsList.Add(new TypeValue<DepartmentFields>(DepartmentFields.id, department.ID));
            if (!department.Name.Equals(""))
                departmentFieldsList.Add(new TypeValue<DepartmentFields>(DepartmentFields.name, department.Name));

            return Dal.Get<Department, DepartmentFields>(departmentFieldsList);
        }

        public Department DepartmentGetByID(int id)
        {
            return Dal.Get<Department>(id);
        }

        public ReturnMessage DepartmentRemove(int departmentID)
        {
            if (departmentID != -1 && Dal.Get<Department>(departmentID) != null)
                return new ReturnMessage(false, Properties.StringResource.DepartmentNotExist);
            
            Dal.Remove<Department>(departmentID);
            return new ReturnMessage(true, Properties.StringResource.DepartmentRemoved);
        }

        #endregion

        #region User

        public User getUser(User user)
        {
            return Dal.GetUser(user);
        }

        public ReturnMessage UserAny()
        {
            if (Dal.UserAny())
                return new ReturnMessage(true, Properties.StringResource.UserExist);
            else
                return new ReturnMessage(false, Properties.StringResource.UserNoExist);
        }

        public User UserCheckCredentials(User user)
        {
            return Dal.UserCheckCredentials(user);
        }

        #endregion

        private enum EntitiesTypes { Employee, ClubMember, Customer, Transaction, Product, Department, User }

        public List<object> GetQueries(string query)
        {
            string[] queryByCommands = query.Split(' ');
            for(int i = 0 ; i < queryByCommands.Count(); i++)
                queryByCommands[i] = queryByCommands[i].ToLower();

            //foreach(string command in queryByCommands)
            //{
            //    if (command.Equals("user") || command.Equals("users"))
            //        return Dal.GetAllUsers().ToList<object>();
            //}


            List<EntitiesTypes> entitiesList = GetQueriesFirstStage(queryByCommands);
            
            return GetQueriesSecondStage( entitiesList, queryByCommands);
        }

        // filters for what you need
        private List<EntitiesTypes> GetQueriesFirstStage(string[] queryByCommands)
        {
            List<EntitiesTypes> entitiesList = new List<EntitiesTypes>
            {
                EntitiesTypes.Employee,
                EntitiesTypes.ClubMember,
                EntitiesTypes.Transaction,
                EntitiesTypes.Product,
                EntitiesTypes.Department
            };

            for (int i = 0; i < queryByCommands.Count(); i++)
            {
                if (entitiesList.Count <= 1)
                    break;

                switch (queryByCommands[i].ToLower())
                {
                    case "supervisor":
                    case "salary":
                    case "salarys":
                    case "salary's":
                    case "employee":
                    case "employees":
                    case "employee's":
                        entitiesList.Remove(EntitiesTypes.ClubMember);
                        entitiesList.Remove(EntitiesTypes.Product);
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Department);
                        break;

                    case "club":
                        if (i + 1 >= queryByCommands.Count() || (!queryByCommands[i + 1].Equals("member") && !queryByCommands[i + 1].Equals("members")))
                            break;
                        i++;
                        entitiesList.Remove(EntitiesTypes.Employee);
                        entitiesList.Remove(EntitiesTypes.Product);
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Department);
                        break;

                    case "member":
                    case "members":
                    case "clubmember":
                    case "clubmembers":
                        entitiesList.Remove(EntitiesTypes.Employee);
                        entitiesList.Remove(EntitiesTypes.Product);
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Department);
                        break;

                    case "cash":
                    case "check":
                    case "visa":
                    case "mastercard":
                    case "paypal":
                    case "greencard":
                    case "receipt":
                    case "receipts":
                    case "purchase":
                    case "purchases":
                    case "payment":
                    case "payments":
                    case "paymentmethod":
                    case "paymentmethods":
                    case "return":
                    case "returns":
                    case "transaction":
                    case "transactions":
                        entitiesList.Remove(EntitiesTypes.Employee);
                        entitiesList.Remove(EntitiesTypes.ClubMember);
                        entitiesList.Remove(EntitiesTypes.Product);
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Department);
                        break;

                    case "stock":
                    case "stocks":
                    case "instock":
                    case "instocks":
                    case "stockcount":
                    case "stockcounts":
                    case "inventory":
                    case "inventorys":
                    case "inventoryid":
                    case "inventoryids":
                    case "inventoryid's":
                    case "product":
                    case "products":
                    case "antique":
                    case "antiques":
                    case "art":
                    case "arts":
                    case "baby":
                    case "babies":
                    case "book":
                    case "books":
                    case "camera":
                    case "cameras":
                    case "cell":
                    case "cells":
                    case "cellphone":
                    case "cellphones":
                    case "clothing":
                    case "clothings":
                    case "computer":
                    case "computers":
                    case "movie":
                    case "movies":
                    case "motor":
                    case "motors":
                    case "home":
                    case "homes":
                    case "garden":
                    case "gardens":
                    case "jewelry":
                    case "jewelries":
                    case "watch":
                    case "watches":
                    case "music":
                    case "musics":
                    case "pet":
                    case "pets":
                    case "sport":
                    case "sports":
                    case "stamp":
                    case "stamps":
                    case "toy":
                    case "toys":
                    case "else":
                        entitiesList.Remove(EntitiesTypes.Employee);
                        entitiesList.Remove(EntitiesTypes.ClubMember);
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Department);
                        break;

                    case "department":
                    case "departments":
                        if (i + 1 >= queryByCommands.Count() || (!queryByCommands[i + 1].Equals("id") && !queryByCommands[i + 1].Equals("ids")))
                        {
                            entitiesList.Remove(EntitiesTypes.ClubMember);
                            entitiesList.Remove(EntitiesTypes.Transaction);
                            entitiesList.Remove(EntitiesTypes.User);
                        }
                        else
                        {
                            entitiesList.Remove(EntitiesTypes.Employee);
                            entitiesList.Remove(EntitiesTypes.ClubMember);
                            entitiesList.Remove(EntitiesTypes.Transaction);
                            entitiesList.Remove(EntitiesTypes.User);
                            entitiesList.Remove(EntitiesTypes.Product);
                        }
                        i++;
                        break;

                    case "teudat":
                    case "teudats":
                        if (i + 1 >= queryByCommands.Count() || (!queryByCommands[i + 1].Equals("zehute") && !queryByCommands[i + 1].Equals("zehut") && !queryByCommands[i + 1].Equals("zehutes") && !queryByCommands[i + 1].Equals("zehuts")))
                            break;
                        i++;
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        entitiesList.Remove(EntitiesTypes.Department);
                        entitiesList.Remove(EntitiesTypes.Product);
                        break;

                    case "first":
                    case "firsts":
                    case "last":
                    case "lasts":
                        if (i + 1 >= queryByCommands.Count() || (!queryByCommands[i + 1].Equals("name") && !queryByCommands[i + 1].Equals("names")))
                            break;
                        i++;
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        entitiesList.Remove(EntitiesTypes.Department);
                        entitiesList.Remove(EntitiesTypes.Product);
                        break;

                    case "male":
                    case "males":
                    case "female":
                    case "females":
                    case "gender":
                    case "genders":
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        entitiesList.Remove(EntitiesTypes.Department);
                        entitiesList.Remove(EntitiesTypes.Product);
                        break;

                    case "year":
                    case "month":
                    case "day":
                    case "date":
                    case "dates":
                        if (i + 2 >= queryByCommands.Count() && queryByCommands[i + 1].Equals("of") && (queryByCommands[i + 2].Equals("birth") || queryByCommands[i + 2].Equals("births")))
                        {
                            i += 2;
                            entitiesList.Remove(EntitiesTypes.User);
                            entitiesList.Remove(EntitiesTypes.Employee);
                            entitiesList.Remove(EntitiesTypes.Transaction);
                            entitiesList.Remove(EntitiesTypes.Department);
                            entitiesList.Remove(EntitiesTypes.Product);
                            break;
                        }
                        if (i + 1 >= queryByCommands.Count() && (queryByCommands[i + 1].Equals("birth") || queryByCommands[i + 1].Equals("births")))
                        {
                            i++;
                            entitiesList.Remove(EntitiesTypes.User);
                            entitiesList.Remove(EntitiesTypes.Employee);
                            entitiesList.Remove(EntitiesTypes.Transaction);
                            entitiesList.Remove(EntitiesTypes.Department);
                            entitiesList.Remove(EntitiesTypes.Product);
                            break;
                        }
                        entitiesList.Remove(EntitiesTypes.Employee);
                        entitiesList.Remove(EntitiesTypes.Department);
                        entitiesList.Remove(EntitiesTypes.Product);
                        entitiesList.Remove(EntitiesTypes.User);
                        break;

                    case "id":
                    case "ids":
                    case "id's":
                    case "bigger":
                    case "smaller":
                    case "higher":
                    case "lower":
                        entitiesList.Remove(EntitiesTypes.User);
                        break;

                    case "name":
                    case "names":
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        break;

                    case "from":
                    case "to":
                        entitiesList.Remove(EntitiesTypes.Employee);
                        entitiesList.Remove(EntitiesTypes.Department);
                        break;

                    case "type":
                    case "types":
                    case "price":
                    case "prices":
                        entitiesList.Remove(EntitiesTypes.Employee);
                        entitiesList.Remove(EntitiesTypes.ClubMember);
                        entitiesList.Remove(EntitiesTypes.Department);
                        entitiesList.Remove(EntitiesTypes.User);
                        break;

                    case "location":
                    case "locations":
                        entitiesList.Remove(EntitiesTypes.Employee);
                        entitiesList.Remove(EntitiesTypes.ClubMember);
                        entitiesList.Remove(EntitiesTypes.Transaction);
                        entitiesList.Remove(EntitiesTypes.User);
                        entitiesList.Remove(EntitiesTypes.Product);
                        break;
                }
            }
            //if(entitiesList.Contains(EntitiesTypes.Product))
            //    updateBestSeller();
            return entitiesList;
        }

        //gets the data
        private List<object> GetQueriesSecondStage(List<EntitiesTypes> entitiesList, string[] queryByCommands)
        {
            List<Employee> employeeList = new List<Employee>();
            List<ClubMember> clubMemberList = new List<ClubMember>();
            List<Customer> customerList = new List<Customer>();
            List<Transaction> transactionList = new List<Transaction>();
            List<Product> productList = new List<Product>();
            List<Department> departmentList = new List<Department>();

            if (entitiesList.Contains(EntitiesTypes.ClubMember))
                clubMemberList = ClubMemberGet(new ClubMember(-1, "", "", User.PermissionType.Worker, -1, "", "", ClubMember.GenderTypes.Empty, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, "")), DateTime.MinValue, DateTime.MinValue, -1, -1, -1);
            if (entitiesList.Contains(EntitiesTypes.Customer))
                customerList = CustomerGet(new Customer(-1, "", "", User.PermissionType.Worker, "a", "a", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), new List<Customer.ProductIdQuantity>()));
            if(entitiesList.Contains(EntitiesTypes.Employee))
                employeeList = EmployeeGet(new Employee(-1, "", "", User.PermissionType.Worker, -1, "", "", -1, -1, Employee.GenderTypes.Empty, -1), -1, -1);
            if(entitiesList.Contains(EntitiesTypes.Transaction))
                transactionList = TransactionGet(new Transaction(DateTime.MinValue, -1, Transaction.ReturnTypes.Empty,new List<Transaction.ProductInReceipt>(),new Transaction.PaymentMethod(Transaction.paymentMethodTypes.Empty, "")),DateTime.MinValue, DateTime.MinValue,-1,-1,-1);
            if(entitiesList.Contains(EntitiesTypes.Product))
                productList = ProductGet(new Product(-1,"",Product.productTypes.Empty,-1,-1,-1),-1,-1);
            if(entitiesList.Contains(EntitiesTypes.Department))
                departmentList = DepartmentGet(new Department(-1, ""));

            //EntitiesClasses entitiesClasses = new EntitiesClasses();
            int tempInt = -1;
            int tempInt2 = -2;
            int tempInt3 = -3;

            for (int i = 0; i < queryByCommands.Count(); i++)
            {
                try
                {
                    switch (queryByCommands[i].ToLower())
                    {
                        case "supervisor":
                            if (i + 1 < queryByCommands.Count() && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.supervisor, tempInt) })).ToList<Employee>();
                            break;

                        case "salary":
                        case "salarys":
                        case "salary's":
                            if (i + 1 < queryByCommands.Count() && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.salary, tempInt) })).ToList<Employee>();
                            break;

                        case "club":
                            if (i + 1 < queryByCommands.Count())
                                if (queryByCommands[i + 1].Equals("member") || queryByCommands[i + 1].Equals("members"))
                                {
                                    i++;
                                    if (i + 1 < queryByCommands.Count())
                                    {
                                        clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.firstName, queryByCommands[i + 1]) }).Union(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.lastName, queryByCommands[i + 1]) })).ToList<ClubMember>()).ToList<ClubMember>();

                                        if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        {
                                            clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.teudatZehutID, tempInt) }).Union(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.memberID, tempInt) })).ToList<ClubMember>()).ToList<ClubMember>();
                                        }
                                    }
                                    else
                                        i--;
                                }
                            break;

                        case "member":
                        case "members":
                        case "clubmember":
                        case "clubmembers":
                            if (i + 1 < queryByCommands.Count())
                            {
                                clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.firstName, queryByCommands[i + 1]) }).Union(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.lastName, queryByCommands[i + 1]) })).ToList<ClubMember>()).ToList<ClubMember>();
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                {
                                    clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.teudatZehutID, tempInt) }).Union(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.memberID, tempInt) })).ToList<ClubMember>()).ToList<ClubMember>();
                                }
                            }
                            break;

                        case "customer":
                        case "customers":
                            if (i + 1 < queryByCommands.Count())
                            {
                                customerList = customerList.Intersect(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.firstName, queryByCommands[i + 1]) }).Union(Dal.Get<Customer, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.lastName, queryByCommands[i + 1]) })).ToList<Customer>()).ToList<Customer>();
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                {
                                    customerList = customerList.Intersect(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.memberID, tempInt) }).Union(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.memberID, tempInt) })).ToList<Customer>()).ToList<Customer>();
                                }
                            }
                            break;

                        case "mastercard":
                            transactionList = transactionList.Intersect(Dal.Get<Transaction, TransactionFields>(new List<TypeValue<TransactionFields>> { new TypeValue<TransactionFields>(TransactionFields.PaymentMethod, Transaction.paymentMethodTypes.masterCard) })).ToList<Transaction>();
                            break;

                        case "visa":
                            transactionList = transactionList.Intersect(Dal.Get<Transaction, TransactionFields>(new List<TypeValue<TransactionFields>> { new TypeValue<TransactionFields>(TransactionFields.PaymentMethod, Transaction.paymentMethodTypes.visa) })).ToList<Transaction>();
                            break;

                        case "purchase":
                            transactionList = transactionList.Intersect(Dal.Get<Transaction, TransactionFields>(new List<TypeValue<TransactionFields>> { new TypeValue<TransactionFields>(TransactionFields.isAReturn, Transaction.ReturnTypes.Purchase) })).ToList<Transaction>();
                            break;

                        case "purchases":
                            transactionList = transactionList.Intersect(Dal.Get<Transaction, TransactionFields>(new List<TypeValue<TransactionFields>> { new TypeValue<TransactionFields>(TransactionFields.isAReturn, Transaction.ReturnTypes.Purchase) })).ToList<Transaction>();
                            break;

                        case "return":
                        case "returns":
                            transactionList = transactionList.Intersect(Dal.Get<Transaction, TransactionFields>(new List<TypeValue<TransactionFields>> { new TypeValue<TransactionFields>(TransactionFields.isAReturn, Transaction.ReturnTypes.Returned) })).ToList<Transaction>();
                            break;

                        case "product":
                        case "products":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.name, queryByCommands[i]) })).ToList<Product>();
                                employeeList = new List<Employee>();
                                clubMemberList = new List<ClubMember>();
                                customerList = new List<Customer>();
                                departmentList = new List<Department>();
                                transactionList = new List<Transaction>();
                                i--;
                            }
                            break;

                        case "instock":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, true) })).ToList<Product>();
                            break;

                        case "outstock":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, false) })).ToList<Product>();
                            break;

                        case "quantity":
                        case "stock":
                        case "stocks":
                        case "stockcount":
                        case "stockcounts":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "empty" }))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, false) })).ToList<Product>();
                                }
                                else if (checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "not", "empty" }))
                                {
                                    i += 2;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, true) })).ToList<Product>();
                                }
                                else if (i + 2 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "equal" }))
                                {
                                    i += 2;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.stockCount, queryByCommands[i]) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "equal" }))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.stockCount, queryByCommands[i]) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "equals" }))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.stockCount, queryByCommands[i]) })).ToList<Product>();
                                }
                                else if (i + 3 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "bigger", "than" }))
                                {
                                    i += 3;
                                    if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(tempInt, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<Product>();
                                }
                                else if (i + 3 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "smaller", "than" }))
                                {
                                    i += 3;
                                    if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(-1, tempInt, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<Product>();
                                }
                                i--;
                            }
                            break;

                        case "is":
                            if (i + 2 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "in", "stock" }))
                            {
                                i += 2;
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, true) })).ToList<Product>();
                            }
                            if (i + 3 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "not", "in", "stock" }))
                            {
                                i += 3;
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, false) })).ToList<Product>();
                            }
                            break;

                        case "in":
                            if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "stock" }))
                            {
                                i++;
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, true) })).ToList<Product>();
                            }
                            break;

                        case "inventory":
                        case "inventorys":
                        case "inventoryid":
                        case "inventoryids":
                        case "inventoryid's":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "equal" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "antique":
                        case "antiques":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Antiques) })).ToList<Product>();
                            break;

                        case "art":
                        case "arts":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Art) })).ToList<Product>();
                            break;

                        case "baby":
                        case "babies":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Baby) })).ToList<Product>();
                            break;

                        case "book":
                        case "books":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Books) })).ToList<Product>();
                            break;

                        case "camera":
                        case "cameras":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Cameras) })).ToList<Product>();
                            break;

                        case "cell":
                        case "cells":
                        case "cellphone":
                        case "cellphones":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Cell) })).ToList<Product>();
                            break;

                        case "clothing":
                        case "clothings":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Clothing) })).ToList<Product>();
                            break;

                        case "computer":
                        case "computers":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Computers) })).ToList<Product>();
                            break;

                        case "movie":
                        case "movies":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Movies) })).ToList<Product>();
                            break;

                        case "motor":
                        case "motors":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Motors) })).ToList<Product>();
                            break;

                        case "home":
                        case "homes":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Home) })).ToList<Product>();
                            break;

                        case "garden":
                        case "gardens":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Garden) })).ToList<Product>();
                            break;

                        case "jewelry":
                        case "jewelries":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Jewelry) })).ToList<Product>();
                            break;

                        case "watch":
                        case "watches":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Watches) })).ToList<Product>();
                            break;

                        case "music":
                        case "musics":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Music) })).ToList<Product>();
                            break;

                        case "pet":
                        case "pets":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Pet) })).ToList<Product>();
                            break;

                        case "sport":
                        case "sports":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Sport) })).ToList<Product>();
                            break;

                        case "stamp":
                        case "stamps":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Stamps) })).ToList<Product>();
                            break;

                        case "toy":
                        case "toys":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Toys) })).ToList<Product>();
                            break;

                        case "else":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Else) })).ToList<Product>();
                            break;

                        case "location":
                        case "locations":
                        case "department":
                        case "departments":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                {
                                    departmentList = departmentList.Intersect(Dal.Get<Department, DepartmentFields>(new List<TypeValue<DepartmentFields>> { new TypeValue<DepartmentFields>(DepartmentFields.id, tempInt) })).ToList<Department>();
                                    employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.departmentID, tempInt) })).ToList<Employee>();
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.location, tempInt) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "id" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "equal" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "teudat":
                        case "teudats":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "zehute" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.TehudatZehut, tempInt) })).ToList<Employee>();
                                    clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.teudatZehutID, tempInt) })).ToList<ClubMember>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "zehut" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.TehudatZehut, tempInt) })).ToList<Employee>();
                                    clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.teudatZehutID, tempInt) })).ToList<ClubMember>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "first":
                        case "firsts":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "name" }))
                                {
                                    i++;
                                    employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.firstName, queryByCommands[i]) })).ToList<Employee>();
                                    clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.firstName, queryByCommands[i]) })).ToList<ClubMember>();
                                    customerList = customerList.Intersect(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.firstName, queryByCommands[i]) })).ToList<Customer>();
                                }
                                else
                                {
                                    employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.firstName, queryByCommands[i]) })).ToList<Employee>();
                                    clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.firstName, queryByCommands[i]) })).ToList<ClubMember>();
                                    customerList = customerList.Intersect(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.firstName, queryByCommands[i]) })).ToList<Customer>();
                                }
                            }
                            break;
                        case "last":
                        case "lasts":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "name" }))
                                {
                                    i++;
                                    employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.lastName, queryByCommands[i]) })).ToList<Employee>();
                                    clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.lastName, queryByCommands[i]) })).ToList<ClubMember>();
                                    customerList = customerList.Intersect(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.lastName, queryByCommands[i]) })).ToList<Customer>();
                                }
                                else
                                {
                                    employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.lastName, queryByCommands[i]) })).ToList<Employee>();
                                    clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.lastName, queryByCommands[i]) })).ToList<ClubMember>();
                                    customerList = customerList.Intersect(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.lastName, queryByCommands[i]) })).ToList<Customer>();
                                }
                            }
                            break;

                        case "date":
                        case "dates":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "from" }))
                                {
                                    if (queryByCommands[i + 1].Contains('\\'))
                                    {
                                        string[] date = queryByCommands[i + 1].Split('\\');
                                        if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                        {
                                            DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                            clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, dt, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<ClubMember>();
                                            transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, dt, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<Transaction>();
                                        }
                                        i++;
                                    }
                                    if (queryByCommands[i + 1].Contains('.'))
                                    {
                                        string[] date = queryByCommands[i + 1].Split('.');
                                        if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                        {
                                            DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                            clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, dt, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<ClubMember>();
                                            transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, dt, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<Transaction>();
                                        }
                                        i++;
                                    }
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "to" }))
                                {
                                    if (queryByCommands[i + 1].Contains('\\'))
                                    {
                                        string[] date = queryByCommands[i + 1].Split('\\');
                                        if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                        {
                                            DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                            clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, DateTime.MinValue, dt, -1, -1, -1, -1, -1)).ToList<ClubMember>();
                                            transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, DateTime.MinValue, dt, -1, -1, -1, -1, -1)).ToList<Transaction>();
                                        }
                                        i++;
                                    }
                                    if (queryByCommands[i + 1].Contains('.'))
                                    {
                                        string[] date = queryByCommands[i + 1].Split('.');
                                        if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                        {
                                            DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                            clubMemberList = clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, DateTime.MinValue, dt, -1, -1, -1, -1, -1)).ToList<ClubMember>();
                                            transactionList = transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, DateTime.MinValue, dt, -1, -1, -1, -1, -1)).ToList<Transaction>();
                                        }
                                        i++;
                                    }
                                }
                                else
                                    i--;
                            }
                            break;

                        case "male":
                        case "males":
                            employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.gender, Employee.GenderTypes.male) })).ToList<Employee>();
                            clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.gender, ClubMember.GenderTypes.male) })).ToList<ClubMember>();
                            break;

                        case "female":
                        case "females":
                            employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.gender, Employee.GenderTypes.female) })).ToList<Employee>();
                            clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.gender, ClubMember.GenderTypes.female) })).ToList<ClubMember>();
                            break;

                        case "year":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                {
                                    clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, DateTime.MinValue, DateTime.MinValue, tempInt, -1, -1, -1, -1)).ToList<ClubMember>();
                                    transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, DateTime.MinValue, DateTime.MinValue, tempInt, -1, -1, -1, -1)).ToList<Transaction>();
                                }
                                else
                                    i--;
                            }
                            break;


                        case "month":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                {
                                    clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, tempInt, -1, -1, -1)).ToList<ClubMember>();
                                    transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, tempInt, -1, -1, -1)).ToList<Transaction>();
                                }
                                else
                                    i--;
                            }
                            break;
                        case "day":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                {
                                    clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, tempInt, -1, -1)).ToList<ClubMember>();
                                    transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, tempInt, -1, -1)).ToList<Transaction>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "id":
                        case "ids":
                        case "id's":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                {
                                    employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.TehudatZehut, tempInt) })).ToList<Employee>();
                                    clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.teudatZehutID, tempInt) })).ToList<ClubMember>();
                                    customerList = customerList.Intersect(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.memberID, tempInt) })).ToList<Customer>();
                                    transactionList = transactionList.Intersect(Dal.Get<Transaction, TransactionFields>(new List<TypeValue<TransactionFields>> { new TypeValue<TransactionFields>(TransactionFields.transactionID, tempInt) })).ToList<Transaction>();
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                    departmentList = departmentList.Intersect(Dal.Get<Department, DepartmentFields>(new List<TypeValue<DepartmentFields>> { new TypeValue<DepartmentFields>(DepartmentFields.id, tempInt) })).ToList<Department>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "employee":
                        case "employees":
                        case "employee's":
                            if (i + 1 < queryByCommands.Count())
                            {
                                employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.firstName, queryByCommands[i + 1]) }).Union(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.lastName, queryByCommands[i + 1]) })).ToList<Employee>()).ToList<Employee>();

                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                    employeeList = employeeList.Union(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.TehudatZehut, tempInt) })).ToList<Employee>();
                            }
                            break;

                        case "name":
                        case "names":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                employeeList = employeeList.Intersect(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.firstName, queryByCommands[i]) }).Union(Dal.Get<Employee, EmployeeFields>(new List<TypeValue<EmployeeFields>> { new TypeValue<EmployeeFields>(EmployeeFields.lastName, queryByCommands[i]) })).ToList<Employee>()).ToList<Employee>();
                                clubMemberList = clubMemberList.Intersect(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.firstName, queryByCommands[i]) }).Union(Dal.Get<ClubMember, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.lastName, queryByCommands[i]) })).ToList<ClubMember>()).ToList<ClubMember>();
                                customerList = customerList.Intersect(Dal.Get<Customer, ClubMemberFields>(new List<TypeValue<ClubMemberFields>> { new TypeValue<ClubMemberFields>(ClubMemberFields.firstName, queryByCommands[i]) }).Union(Dal.Get<Customer, CustomerFields>(new List<TypeValue<CustomerFields>> { new TypeValue<CustomerFields>(CustomerFields.lastName, queryByCommands[i]) })).ToList<Customer>()).ToList<Customer>();
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.name, queryByCommands[i]) })).ToList<Product>();
                                departmentList = departmentList.Intersect(Dal.Get<Department, DepartmentFields>(new List<TypeValue<DepartmentFields>> { new TypeValue<DepartmentFields>(DepartmentFields.name, queryByCommands[i]) })).ToList<Department>();
                            }
                            break;

                        case "price":
                        case "prices":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.price, queryByCommands[i]) })).ToList<Product>();

                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "from" }))
                                {
                                    i++;
                                    if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, tempInt, -1)).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "to" }))
                                {
                                    i++;
                                    if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, tempInt)).ToList<Product>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "from":
                            if (i + 1 < queryByCommands.Count())
                            {
                                if (queryByCommands[i + 1].Contains('\\'))
                                {
                                    string[] date = queryByCommands[i + 1].Split('\\');
                                    if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                    {
                                        DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                        clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, dt, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<ClubMember>();
                                        transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, dt, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<Transaction>();
                                        productList = new List<Product>();
                                        employeeList = new List<Employee>();
                                        departmentList = new List<Department>();
                                    }
                                    i++;
                                }
                                else if (queryByCommands[i + 1].Contains('.'))
                                {
                                    string[] date = queryByCommands[i + 1].Split('.');
                                    if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                    {
                                        DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                        clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, dt, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<ClubMember>();
                                        transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, dt, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<Transaction>();
                                        productList = new List<Product>();
                                        employeeList = new List<Employee>();
                                        departmentList = new List<Department>();
                                    }
                                    i++;
                                }
                                else if (Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(tempInt, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, -1).Union(Dal.GetRangeByDateTime<Product>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, tempInt, -1)).ToList<Product>()).ToList<Product>();
                                }
                            }
                            break;

                        case "to":
                            if (i + 1 < queryByCommands.Count())
                            {
                                if (queryByCommands[i + 1].Contains('\\'))
                                {
                                    string[] date = queryByCommands[i + 1].Split('\\');
                                    if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                    {
                                        DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                        clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, DateTime.MinValue, dt, -1, -1, -1, -1, -1)).ToList<ClubMember>();
                                        transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, DateTime.MinValue, dt, -1, -1, -1, -1, -1)).ToList<Transaction>();
                                        productList = new List<Product>();
                                        employeeList = new List<Employee>();
                                        departmentList = new List<Department>();
                                    }
                                    i++;
                                }
                                else if (queryByCommands[i + 1].Contains('.'))
                                {
                                    string[] date = queryByCommands[i + 1].Split('.');
                                    if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                    {
                                        DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                        clubMemberList = clubMemberList.Intersect(Dal.GetRangeByDateTime<ClubMember>(-1, -1, DateTime.MinValue, dt, -1, -1, -1, -1, -1)).ToList<ClubMember>();
                                        transactionList = transactionList.Intersect(Dal.GetRangeByDateTime<Transaction>(-1, -1, DateTime.MinValue, dt, -1, -1, -1, -1, -1)).ToList<Transaction>();
                                        productList = new List<Product>();
                                        employeeList = new List<Employee>();
                                        departmentList = new List<Department>();
                                    }
                                    i++;
                                }
                                else if (Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(-1, tempInt, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, -1).Union(Dal.GetRangeByDateTime<Product>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, tempInt)).ToList<Product>()).ToList<Product>();
                                }
                            }
                            break;
                    }
                }
                catch(Exception e){}
            }
            
            return employeeList.Cast<object>().Union(clubMemberList.Cast<object>().Union(transactionList.Cast<object>().Union(productList.Cast<object>().Union(departmentList.Cast<object>())))).ToList();
        }

        private bool checkIfNextStringsAreEqual(string[] queryByCommands, int currentIndex, List<string> commandsToEqualize)
        {
            if (queryByCommands.Count() - currentIndex + 1 < commandsToEqualize.Count())
                return false;

            foreach(string command in commandsToEqualize)
            {
                if (!queryByCommands[currentIndex].Equals(command))
                    return false;
                currentIndex++;
            }
            return true;
        }

        public List<int> getBestSeller()
        {
            //bestSellersProductsQuantity.Clear();
            List<int> bestSellersProducts = new List<int>();
            Transaction t = new Transaction(-1, -1, DateTime.MinValue, Transaction.ReturnTypes.Purchase, null, new Transaction.PaymentMethod(Transaction.paymentMethodTypes.Empty, ""));
            List<Transaction> transactionList = TransactionGet(t, new DateTime(DateTime.Now.Year, DateTime.Now.Month == 1 ? 12 : DateTime.Now.Month - 1, DateTime.Now.Day), DateTime.Now, -1, -1, -1);
            Dictionary<int, int> productsCount = new Dictionary<int,int>();
            foreach(Transaction transaction in transactionList)
            {
                foreach(Transaction.ProductInReceipt receiptRow in transaction.Receipt)
                {
                    if(productsCount.ContainsKey(receiptRow.InventoryID))
                        productsCount[receiptRow.InventoryID] = productsCount[receiptRow.InventoryID] + receiptRow.Quantity;
                    else
                        productsCount.Add(receiptRow.InventoryID, receiptRow.Quantity);
                }
            }
            int maxValue = productsCount.Count == 0 ? 0 : productsCount.Values.Max();
            foreach (KeyValuePair<int, int> pair in productsCount)
                if (pair.Value == maxValue)
                    bestSellersProducts.Add(pair.Key);

            return bestSellersProducts;
        }

        public List<Product> GetQueriesProducts(string[] queryByCommands)
        {
            List<Product> productList = ProductGet(new Product(-1, "", Product.productTypes.Empty, -1, -1, -1), -1, -1);

            //EntitiesClasses entitiesClasses = new EntitiesClasses();
            int tempInt = -1;
            int tempInt2 = -2;
            int tempInt3 = -3;

            for (int i = 0; i < queryByCommands.Count(); i++)
            {
                try
                {
                    switch (queryByCommands[i].ToLower())
                    {
                        case "product":
                        case "products":
                            if (i + 1 < queryByCommands.Count())
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.name, queryByCommands[i + 1]) })).ToList<Product>();
                            break;

                        case "instock":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, true) })).ToList<Product>();
                            break;

                        case "outstock":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, false) })).ToList<Product>();
                            break;

                        case "quantity":
                        case "stock":
                        case "stocks":
                        case "stockcount":
                        case "stockcounts":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "empty" }))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, false) })).ToList<Product>();
                                }
                                else if (checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "not", "empty" }))
                                {
                                    i += 2;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, true) })).ToList<Product>();
                                }
                                else if (i + 2 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "equal" }))
                                {
                                    i += 2;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.stockCount, queryByCommands[i]) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "equal" }))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.stockCount, queryByCommands[i]) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "equals" }))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.stockCount, queryByCommands[i]) })).ToList<Product>();
                                }
                                else if (i + 3 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "bigger", "than" }))
                                {
                                    i += 3;
                                    if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(tempInt, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<Product>();
                                }
                                else if (i + 3 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is", "smaller", "than" }))
                                {
                                    i += 3;
                                    if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(-1, tempInt, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, -1)).ToList<Product>();
                                }
                                i--;
                            }
                            break;

                        case "is":
                            if (i + 2 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "in", "stock" }))
                            {
                                i += 2;
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, true) })).ToList<Product>();
                            }
                            if (i + 3 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "not", "in", "stock" }))
                            {
                                i += 3;
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, false) })).ToList<Product>();
                            }
                            break;

                        case "in":
                            if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "stock" }))
                            {
                                i++;
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inStock, true) })).ToList<Product>();
                            }
                            break;

                        case "inventory":
                        case "inventorys":
                        case "inventoryid":
                        case "inventoryids":
                        case "inventoryid's":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "is" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "equal" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "antique":
                        case "antiques":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Antiques) })).ToList<Product>();
                            break;

                        case "art":
                        case "arts":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Art) })).ToList<Product>();
                            break;

                        case "baby":
                        case "babies":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Baby) })).ToList<Product>();
                            break;

                        case "book":
                        case "books":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Books) })).ToList<Product>();
                            break;

                        case "camera":
                        case "cameras":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Cameras) })).ToList<Product>();
                            break;

                        case "cell":
                        case "cells":
                        case "cellphone":
                        case "cellphones":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Cell) })).ToList<Product>();
                            break;

                        case "clothing":
                        case "clothings":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Clothing) })).ToList<Product>();
                            break;

                        case "computer":
                        case "computers":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Computers) })).ToList<Product>();
                            break;

                        case "movie":
                        case "movies":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Movies) })).ToList<Product>();
                            break;

                        case "motor":
                        case "motors":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Motors) })).ToList<Product>();
                            break;

                        case "home":
                        case "homes":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Home) })).ToList<Product>();
                            break;

                        case "garden":
                        case "gardens":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Garden) })).ToList<Product>();
                            break;

                        case "jewelry":
                        case "jewelries":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Jewelry) })).ToList<Product>();
                            break;

                        case "watch":
                        case "watches":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Watches) })).ToList<Product>();
                            break;

                        case "music":
                        case "musics":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Music) })).ToList<Product>();
                            break;

                        case "pet":
                        case "pets":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Pet) })).ToList<Product>();
                            break;

                        case "sport":
                        case "sports":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Sport) })).ToList<Product>();
                            break;

                        case "stamp":
                        case "stamps":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Stamps) })).ToList<Product>();
                            break;

                        case "toy":
                        case "toys":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Toys) })).ToList<Product>();
                            break;

                        case "else":
                            productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.type, Product.productTypes.Else) })).ToList<Product>();
                            break;

                        case "location":
                        case "locations":
                        case "department":
                        case "departments":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.location, tempInt) })).ToList<Product>();
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "id" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "equal" }) && Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "id":
                        case "ids":
                        case "id's":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.inventoryID, tempInt) })).ToList<Product>();
                                else
                                    i--;
                            }
                            break;

                        case "name":
                        case "names":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.name, queryByCommands[i]) })).ToList<Product>();
                            }
                            break;

                        case "price":
                        case "prices":
                            if (i + 1 < queryByCommands.Count())
                            {
                                i++;
                                if (Int32.TryParse(queryByCommands[i], out tempInt))
                                    productList = productList.Intersect(Dal.Get<Product, ProductFields>(new List<TypeValue<ProductFields>> { new TypeValue<ProductFields>(ProductFields.price, queryByCommands[i]) })).ToList<Product>();

                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "from" }))
                                {
                                    i++;
                                    if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, tempInt, -1)).ToList<Product>();
                                }
                                else if (i + 1 < queryByCommands.Count() && checkIfNextStringsAreEqual(queryByCommands, i, new List<string> { "to" }))
                                {
                                    i++;
                                    if (Int32.TryParse(queryByCommands[i], out tempInt))
                                        productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, tempInt)).ToList<Product>();
                                }
                                else
                                    i--;
                            }
                            break;

                        case "from":
                            if (i + 1 < queryByCommands.Count())
                            {
                                if (queryByCommands[i + 1].Contains('\\'))
                                {
                                    string[] date = queryByCommands[i + 1].Split('\\');
                                    if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                    {
                                        DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                        productList = new List<Product>();
                                    }
                                    i++;
                                }
                                else if (queryByCommands[i + 1].Contains('.'))
                                {
                                    string[] date = queryByCommands[i + 1].Split('.');
                                    if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                    {
                                        DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                        productList = new List<Product>();
                                    }
                                    i++;
                                }
                                else if (Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(tempInt, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, -1).Union(Dal.GetRangeByDateTime<Product>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, tempInt, -1)).ToList<Product>()).ToList<Product>();
                                }
                            }
                            break;

                        case "to":
                            if (i + 1 < queryByCommands.Count())
                            {
                                if (queryByCommands[i + 1].Contains('\\'))
                                {
                                    string[] date = queryByCommands[i + 1].Split('\\');
                                    if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                    {
                                        DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                        productList = new List<Product>();
                                    }
                                    i++;
                                }
                                else if (queryByCommands[i + 1].Contains('.'))
                                {
                                    string[] date = queryByCommands[i + 1].Split('.');
                                    if (date.Count() == 3 && Int32.TryParse(date[0], out tempInt3) && Int32.TryParse(date[1], out tempInt2) && Int32.TryParse(date[2], out tempInt))
                                    {
                                        DateTime dt = new DateTime(tempInt, tempInt2, tempInt3);
                                        productList = new List<Product>();
                                    }
                                    i++;
                                }
                                else if (Int32.TryParse(queryByCommands[i + 1], out tempInt))
                                {
                                    i++;
                                    productList = productList.Intersect(Dal.GetRangeByDateTime<Product>(-1, tempInt, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, -1).Union(Dal.GetRangeByDateTime<Product>(-1, -1, DateTime.MinValue, DateTime.MinValue, -1, -1, -1, -1, tempInt)).ToList<Product>()).ToList<Product>();
                                }
                            }
                            break;
                    }
                }
                catch (Exception e) { }
            }

            return productList;
        }
    }
}
