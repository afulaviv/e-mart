﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;

namespace Buisness_Layer
{
    public interface Buisness_Interface
    {
        #region Employee

        /// <summary>
        /// Add new employee.
        /// </summary>
        /// <param name="employee">The Employee, if he would not belong to any department - insert 0 at deparmentID, Do the same for supervisor if necessary..</param>
        /// <returns>Returns ReturnMessage, if added returns true.</returns>
        ReturnMessage EmployeeAdd(Employee employee);
        /// <summary>
        /// Edit employee
        /// </summary>
        /// <param name="employee">The Employee, if he would not belong to any department - insert 0 at deparmentID, Do the same for supervisor if necessary..</param>
        /// <returns>Returns ReturnMessage, if edited returns true.</returns>
        ReturnMessage EmployeeEdit(Employee employee);


        /// <summary>
        /// 
        /// 
        /// 
        /// Employee by query
        /// </summary>
        /// <param name="employee">Holds the types you are searching for.
        /// For each of those properties you dont want to use for search, use the next value:
        /// 1. strings - use "",
        /// 2. int - use -1,
        /// 3. enums - use empty (given in the enum).</param>
        /// <returns>Returns list of employees which answers given search term.</returns>
        List<Employee> EmployeeGet(Employee employee, int salaryFrom, int salaryTo);
        /// <summary>
        /// Get Employee By TehudatZehut.
        /// </summary>
        /// <param name="TehudatZehut">The Tehudat Zehut.</param>
        /// <returns>Employee, if not exists return null.</returns>
        Employee EmployeeGet(int userID);
        /// <summary>
        /// Removes employee.
        /// </summary>
        /// <param name="employee">employee to remove.</param>
        /// <returns>Returns ReturnMessage, if removed returns true.</returns>
        ReturnMessage EmployeeRemove(int TehudatZehut);

        Employee EmployeeGetById(int id);
        #endregion

        #region ClubMember

        /// <summary>
        /// Add new club member.
        /// </summary>
        /// <param name="clubMember">The new club member, if there is no supervisor, enter 0.
        /// Be aware - it does not important what data you type in club member ID, this method ignores it and put what ID it needs.</param>
        /// <returns>Returns ReturnMessage, true if it success.</returns>
        ReturnMessage ClubMemberAdd(ClubMember clubMember);
        /// <summary>
        /// Edit club member.
        /// </summary>
        /// <param name="clubMember">The club member, if you want to change to no supervisor insert 0.</param>
        /// <returns>Returns ReturnMessage, if edited returns true.</returns>
        ReturnMessage ClubMemberEdit(ClubMember clubMember);
        /// <summary>
        /// Get Club member by query.
        /// </summary>
        /// <param name="clubMember">Holds the types you are searching for.
        /// For each of those properties you dont want to use for search, use the next value:
        /// 1. strings - use "",
        /// 2. int - use -1,
        /// 3. enums - use empty (given in the enum).
        /// Be aware: the history list will not be checked for no matter what lies in it.</param>
        /// <param name="dateTimeBegin">From this date, if dont want to use it, enter DateTime.MinValue.</param>
        /// <param name="dateTimeEnd">To this date, if dont want to use it, enter DateTime.MinValue.</param>
        /// <param name="year">Query by year, if you don't want to use it, enter -1.</param>
        /// <param name="month">Query by month, if you don't want to use it, enter -1.</param>
        /// <param name="day">Query by day, if you don't want to use it, enter -1.</param>
        /// <returns>List of Club members who answer given query.</returns>
        List<ClubMember> ClubMemberGet(ClubMember clubMember, DateTime dateTimeBegin, DateTime dateTimeEnd, int year, int month, int day);
        /// <summary>
        /// Get club member By TehudatZehut.
        /// </summary>
        /// <param name="clubMemberTeudatZehut">The Tehudat Zehut of the club member.</param>
        /// <returns>Club member, if not exists return null.</returns>
        ClubMember ClubMemberGet(int clubMemberID);
        ClubMember ClubMemberGetByUser(User user);
        Customer CustomerGet(int customerID);
        /// <summary>
        /// Removes club member, note: moves all the transactions to the notRegistered club member
        /// </summary>
        /// <param name="clubMemberTeudatZehut">Teudat zehut of club member who need to remove.</param>
        /// <returns>Returns ReturnMessage, if removed returns true.</returns>
        ReturnMessage ClubMemberRemove(int clubMemberTeudatZehut);
        /// <summary>
        /// Get list of Transactions based on teudat zehut of club member.
        /// </summary>
        /// <param name="clubMemberTeudatZehut">Tehudat zehut of the club member.</param>
        /// <returns>List of transactions of the club member, if club member not exist - returns empty list.</returns>
        List<Transaction> ClubMemberGetTransactions(int clubMemberTeudatZehut);

        #endregion

        #region Transaction
        
        /// <summary>
        /// Add new transaction, it will also add the transaction to the member it belongs to.
        /// </summary>
        /// <param name="transaction">The transaction who need to be added, be aware - it does not important what data you type in transaction ID, this method ignores it and put what ID it needs.</param>
        /// <param name="teudatZehutID">The teudat zehut this transaction belong to the club member, insert 0 so it would belong to no one (not registered Customers).</param>
        /// <returns>Returns ReturnMessage, true if it success, if it is false, check the message - it must be because the club member do not exist.</returns>
        ReturnMessage TransactionAdd(Transaction transaction);
        /// <summary>
        /// Edit Transaction.
        /// </summary>
        /// <param name="transaction">The transaction needed to be edited.</param>
        /// <returns>Returns ReturnMessage indicating if a success.</returns>
        ReturnMessage TransactionEdit(Transaction transaction);
        /// <summary>
        /// Get transaction based on input.
        /// </summary>
        /// <param name="transaction">Holds the types you are searching for.
        /// only checks TransactionID, IsAReturn, and Payment.
        /// For each of those properties you dont want to use for search, use the next value:
        /// 1. int - use -1,
        /// 2. enums - use empty (given in the enum).
        /// Be aware: the reciept will not be checked for no matter what lies in it.</param>
        /// <param name="dateTimeBegin">From this date, if dont want to use it, enter DateTime.MinValue.</param>
        /// <param name="dateTimeEnd">To this date, if dont want to use it, enter DateTime.MinValue.</param>
        /// <param name="year">Query by year, if you don't want to use it, enter -1.</param>
        /// <param name="month">Query by month, if you don't want to use it, enter -1.</param>
        /// <param name="day">Query by day, if you don't want to use it, enter -1.</param>
        /// <returns>List of transactions who answer given query.</returns>
        List<Transaction> TransactionGet(Transaction transaction, DateTime dateTimeBegin, DateTime dateTimeEnd, int year, int month, int day);
        /// <summary>
        /// Get club member By TehudatZehut.
        /// </summary>
        /// <param name="clubMemberTeudatZehut">The Tehudat Zehut of the club member.</param>
        /// <returns>Club member.</returns>
        /// 
        /// <summary>
        /// Get transaction By inventoryID.
        /// </summary>
        /// <param name="transactionID">The transaction ID we are working on.</param>
        /// <returns>Transaction, if not exists return null.</returns>
        Transaction TransactionGetByID(int transactionID);
        /// <summary>
        /// Removes transaction, note: remove it from the club member also.
        /// </summary>
        /// <param name="transactionID">The transaction ID.</param>
        /// <param name="teudatZehutID">Teudat zehut of club member who need to remove.</param>
        /// <returns></returns>
        ReturnMessage TransactionRemove(int transactionID, int teudatZehutID);

        #endregion

        #region Product

        /// <summary>
        /// Adds new product.
        /// </summary>
        /// <param name="product">The product</param>
        /// <returns>Returns ReturnMessage indicating if the product has been added.</returns>
        ReturnMessage ProductAdd(Product product);
        /// <summary>
        /// Edit existing product
        /// </summary>
        /// <param name="product">The product</param>
        /// <returns>Returns ReturnMessage indicating if the product has been edited.</returns>
        ReturnMessage ProductEdit(Product product);
        /// <summary>
        /// Get product by query.
        /// </summary>
        /// <param name="product">Holds the types you are searching for.
        /// For each of those properties you dont want to use for search, use the next value:
        /// 1. strings - use "",
        /// 2. int - use -1,
        /// 3. enums - use uncategroized (given in the enum).</param>
        /// <param name="priceFrom">From this price, if you don't want to use it, enter -1.</param>
        /// <param name="priceTo">To this price, if you don't want to use it, enter -1.</param>
        /// <returns>Returns list of products who answer your query.</returns>
        List<Product> ProductGet(Product product, int priceFrom, int priceTo);
        /// <summary>
        /// Get product by inventory ID.
        /// </summary>
        /// <param name="inventoryID">The inventory ID of the product.</param>
        /// <returns>Returns the product.</returns>
        Product ProductGetByID(int inventoryID);
        /// <summary>
        /// Removes product.
        /// </summary>
        /// <param name="productID">The inventory ID of the product who need to be removed.</param>
        /// <returns>Returns ReturnMessage, if removed returns true.</returns>
        ReturnMessage ProductRemove(int productID);
        
        #endregion

        #region Customer
        ReturnMessage CustomerAdd(Customer customer);
        ReturnMessage CustomerEdit(Customer customer);
        List<Customer> CustomerGet(Customer customer);
        Customer CustomerGetByUser(User user);
        ReturnMessage CustomerRemove(int customerMemberID);
        List<Transaction> CustomerGetTransactions(int customerMemberID);
        //List<Customer> CustomerGetAll();
        #endregion

        #region Department

        /// <summary>
        /// Adds new department.
        /// </summary>
        /// <param name="department">The new department, be aware - it does not important what data you type in ID, this method ignores it and put what ID it needs.</param>
        /// <returns>Returns ReturnMessage, true if it success.</returns>
        ReturnMessage DepartmentAdd(Department department);
        /// <summary>
        /// Edit department.
        /// </summary>
        /// <param name="department">The department.</param>
        /// <returns>Returns ReturnMessage, if edited returns true.</returns>
        ReturnMessage DepartmentEdit(Department department);
        /// <summary>
        /// Get department by query.
        /// </summary>
        /// <param name="department">>Holds the types you are searching for.
        /// For each of those properties you dont want to use for search, use the next value:
        /// 1. strings - use "",
        /// 2. int - use -1.</param>
        /// <returns>Returns list of deparments who answer your query.</returns>
        List<Department> DepartmentGet(Department department);
        /// <summary>
        /// Removes department.
        /// </summary>
        /// <param name="id">The id of the department.</param>
        /// <returns>Returns the department.</returns>
        Department DepartmentGetByID(int id);
        /// <summary>
        /// Removes deparment, also make sure he get rid of all the dependecies.
        /// </summary>
        /// <param name="departmentID">The department ID of the department who needs to be removed.</param>
        /// <returns>Returns ReturnMessage, if removed returns true.</returns>
        ReturnMessage DepartmentRemove(int departmentID);

        #endregion
        
        #region User

        /// <summary>
        /// Returns if any user exists in the Data structure.
        /// </summary>
        /// <returns>Returns ReturnMessage, true if any user exists.</returns>
        ReturnMessage UserAny();
        User UserCheckCredentials(User user);

        #endregion

        List<object> GetQueries(string query);

        List<Product> GetQueriesProducts(string[] queryByCommands);

        List<int> getBestSeller();

        //void FirstTimeInit();
    }
}
