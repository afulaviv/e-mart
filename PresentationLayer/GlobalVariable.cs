﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;

namespace PresentationLayer
{
    public class GlobalVariable
    {
        public static ClubMember clubMember;
        public static Customer customer;
        public static Employee employee;
    }
}