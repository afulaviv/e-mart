﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using System.Collections.ObjectModel;
using Backend;
using Buisness_Layer;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for CartScreen.xaml
    /// </summary>
    public partial class CartScreen : Window, ScreenInterface
    {
        private List<Product> productList;
        private Buisness_Interface bl;
        private User user;


        public CartScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
            this.productList = new List<Product>();
            List<Product> list = bl.GetQueriesProducts(new String[] { "" });
            ProductCollection collectP = new ProductCollection(bl);
            listView.ItemsSource = collectP;
            //listView.Columns.RemoveAt(0);   
        }

        public void Run()
        {
            this.Show();
        }

        private void image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (listView.SelectedItem != null)
            {
                DataObject data = new DataObject("Item added successfully");
                var effects = DragDrop.DoDragDrop(sender as DependencyObject, data, DragDropEffects.Move);
            }
        }
        private void button1_Drop(object sender, DragEventArgs e)
        {
            var data = e.Data.GetData(typeof(string)).ToString();
            Product currentObject = (Product)listView.SelectedItem;
            this.productList.Add(currentObject);
            MessageBox.Show(data);
            
        }

        private void Button_Click_Discard(object sender, RoutedEventArgs e)
        {
            this.productList = new List<Product>();

            var nextScreen = new MainScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        private void Button_Click_Buy(object sender, RoutedEventArgs e)
        {
            List<Transaction.ProductInReceipt> reciept = new List<Transaction.ProductInReceipt>();

            foreach (Product p in productList)
            {
                //for (int j = 0; j < GlobalVariable.customer.Cart.Count; j++)
                //{
                    //if (GlobalVariable.customer.Cart[j].ProductID == p.InventoryID)
                    //    GlobalVariable.customer.Cart[j].Quantity += 1;
                    //else
                    GlobalVariable.customer.Cart.Add(new Customer.ProductIdQuantity(p.InventoryID, 1));
                //}
            }

            //for (int j = 0; j < GlobalVariable.customer.Cart.Count; j++)
            //{

            //}

            for (int i = 0; i < GlobalVariable.customer.Cart.Count; i++)
            {
                reciept.Add(new Transaction.ProductInReceipt(GlobalVariable.customer.Cart[i].ProductID, bl.ProductGetByID(GlobalVariable.customer.Cart[i].ProductID).Name, bl.ProductGetByID(GlobalVariable.customer.Cart[i].ProductID).StockCount, bl.ProductGetByID(GlobalVariable.customer.Cart[i].ProductID).Price));
            }


            //foreach (Product i in productList)
            //{
            //    if (!GlobalVariable.customer.Cart.ContainsKey(i.InventoryID))
            //    {
            //        int count = 0;
            //        foreach (Product j in productList)
            //        {
            //            if (j.Equals(i)) count++;
            //        }
            //        GlobalVariable.customer.Cart.Add(i.InventoryID, count);
            //        reciept.Add(new Transaction.ProductInReceipt(i.InventoryID, i.Name, count, i.Price));
            //    }
            //}

            Transaction transaction = new Transaction(DateTime.Now, GlobalVariable.customer.Id, Transaction.ReturnTypes.Purchase, reciept, new Transaction.PaymentMethod(Transaction.paymentMethodTypes.visa, ""));

            var nextScreen = new BuyScreen(transaction, bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

    }
    public class ProductCollection : ObservableCollection<Product>
    {
        public string Name;
        public bool InStock;
        public int Price;
        private Buisness_Interface bl;

        public ProductCollection(Buisness_Interface bl)
            : base()
        {
            this.bl = bl;
            var list = bl.GetQueriesProducts(new string[] { "" });
            foreach (var i in list)
            {
                Add(new Product(i));
            }
        }
        

    }

    
}
