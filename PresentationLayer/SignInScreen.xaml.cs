﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections;
using Backend;
using Buisness_Layer;
using System.Text.RegularExpressions;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for SignInScreen.xaml
    /// </summary>
    public partial class SignInScreen : Window, ScreenInterface
    {
        private Buisness_Interface bl;
        private bool isYesChecked  = false;

        public SignInScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;

            if (!bl.UserAny().Success)
            {
                GlobalVariable.employee = new Employee(-1, "", "", User.PermissionType.Admin, -1, "", "", -1, -1, Employee.GenderTypes.Empty, -1);
            }

            if (GlobalVariable.employee != null && GlobalVariable.employee.Type == User.PermissionType.Admin)
            {
                employeeGrid.Visibility = Visibility.Visible;
                eComboGender.ItemsSource = Enum.GetValues(typeof(Employee.GenderTypes));
                eComboGender.SelectedIndex = 0;
                typeCombo.ItemsSource = Enum.GetValues(typeof(User.PermissionType));
                typeCombo.SelectedItem = User.PermissionType.Admin;
                typeCombo.Visibility = Visibility.Visible;
                perType.Visibility = Visibility.Visible;
            }

            if (GlobalVariable.employee == null) 
            {
                CustomerGrid.Visibility = Visibility.Visible;
                typeCombo.Items.Add(User.PermissionType.Customer);
                typeCombo.SelectedItem = User.PermissionType.Customer;
                GlobalVariable.customer = new Customer(-1, "", "", User.PermissionType.Customer, "", "", null, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), null);
                cmComboGender.ItemsSource = Enum.GetValues(typeof(ClubMember.GenderTypes));
                cmComboGender.SelectedItem = ClubMember.GenderTypes.Empty;
                cPaymentType.ItemsSource = Enum.GetValues(typeof(Customer.paymentMethodTypes));
                cPaymentType.SelectedIndex = 0;
            }
        }

        public void Run()
        {
            this.Show();
        }

        private void SignIn_Click(object sender, RoutedEventArgs e)
        {
            string username = user.Text;
            string password = pass.Password;
            bool capitalLetter = false;
            bool lowLetter = false;
            bool specialChar = false;
            bool numChar = false;

            for (int i = 0; i < password.Length; i++)
            {
                if (char.IsUpper(password[i]))
                    capitalLetter = true;
                if (char.IsDigit(password[i]))
                    numChar = true;
                if (char.IsSymbol(password[i]))
                    specialChar = true;
                if (char.IsLower(password[i]))
                    lowLetter = true;
            }
            if (!capitalLetter || !lowLetter || !numChar || !specialChar)
                invalidPassword.Text = "The password must contain capital letter, low letter, number and special char";
            else
            {
                MessageBoxResult areYouSure = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (areYouSure == MessageBoxResult.Yes)
                {
                    if (GlobalVariable.customer != null)
                    {
                        ReturnMessage cusMsg = null;
                        ReturnMessage cmMsg = null;
                        if (checkIfLegalFieldsCustomer())
                        {
                            if (!isYesChecked)
                                cusMsg = bl.CustomerAdd(new Customer(-1, username, password, (User.PermissionType)Enum.Parse(typeof(User.PermissionType), typeCombo.Text), cFn.Text, cLn.Text, null, new Customer.PaymentMethod((Customer.paymentMethodTypes)Enum.Parse(typeof(Customer.paymentMethodTypes), cPaymentType.Text), creditNumber1.Text + creditNumber2.Text + creditNumber3.Text + creditNumber4.Text), null));
                            else
                                cmMsg = bl.ClubMemberAdd(new ClubMember(-1, username, password, (User.PermissionType)Enum.Parse(typeof(User.PermissionType), typeCombo.Text), Convert.ToInt32(cmID.Text), cFn.Text, cLn.Text, null, (ClubMember.GenderTypes)Enum.Parse(typeof(ClubMember.GenderTypes), cmComboGender.Text), Convert.ToDateTime(cmDOB.Text), new Customer.PaymentMethod((Customer.paymentMethodTypes)Enum.Parse(typeof(Customer.paymentMethodTypes), cPaymentType.Text), creditNumber1.Text + creditNumber2.Text + creditNumber3.Text + creditNumber4.Text), null));
                        }
                        ReturnMessage tmp = null;
                        if (cusMsg != null)
                        {
                            tmp = cusMsg;
                        }
                        if (cmMsg != null)
                        {
                            tmp = cmMsg;
                        }
                        if (tmp.Success.Equals(false))
                        {
                            invalidUser.Content = tmp.Message;
                            invalidUser.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            var nextScreen = new LogonScreen(bl);
                            this.Hide();
                            nextScreen.Closed += (s, args) => this.Close();
                            nextScreen.Show();
                        }
                    }
                    else
                    {
                        if (GlobalVariable.employee != null)
                        {
                            if (checkIfLegalFieldsEmployee())
                            {
                                ReturnMessage empMsg = bl.EmployeeAdd(new Employee(-1, username, password, (User.PermissionType)Enum.Parse(typeof(User.PermissionType), typeCombo.Text), Convert.ToInt32(eId.Text), eFn.Text, eLn.Text, Convert.ToInt32(eDepId.Text), Convert.ToInt32(eSalary.Text), (Employee.GenderTypes)Enum.Parse(typeof(Employee.GenderTypes), eComboGender.Text), Convert.ToInt32(eSupervisor.Text)));

                                if (empMsg.Success.Equals(false))
                                {
                                    invalidUser.Content = empMsg.Message;
                                    invalidUser.Visibility = Visibility.Visible;
                                }
                                else
                                {
                                    var nextScreen = new LogonScreen(bl);
                                    this.Hide();
                                    nextScreen.Closed += (s, args) => this.Close();
                                    nextScreen.Show();
                                }
                            }
                        }
                    }
                }
            }
        }
        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Application.Current.Shutdown();
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new LogonScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        private void onlyNumbers(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void cmYes_Checked(object sender, RoutedEventArgs e)
        {
            clubMemberGrid.Visibility = Visibility.Visible;
            isYesChecked = true;

        }

        private void cmNo_Checked(object sender, RoutedEventArgs e)
        {
            clubMemberGrid.Visibility = Visibility.Collapsed;
            isYesChecked = false;
        }

        private bool checkIfLegalFieldsCustomer()
        {
            bool ans = true;
            if (cFn.Text.Equals(""))
            {
                ans = false;
                invalidCustomerFirstName.Visibility = Visibility.Visible;
            }
                
            if (cLn.Text.Equals(""))
            {
                ans = false;
                invalidCustomerLastName.Visibility = Visibility.Visible;
            }
            return ans;
        }

        private void creditNumber1_KeyDown(object sender, KeyEventArgs e)
        {
            if (creditNumber1.Text.Length == 3)
            {
                creditNumber3.Visibility = Visibility.Collapsed;
                //System.Windows.Forms.SendKeys.Send("{TAB}");
            }
        }

        private bool checkIfLegalFieldsEmployee()
        {
            bool ans = true;
            if (eId.Text.Equals(""))
            {
                invalidEId.Visibility = Visibility.Visible;
                ans = false;
            }
            if (eFn.Text.Equals(""))
            {
                invalidEFn.Visibility = Visibility.Visible;
                ans = false;
            }
            if  (eLn.Text.Equals(""))
            {
                invalidELn.Visibility = Visibility.Visible;
                ans = false;
            }
            if (eDepId.Text.Equals(""))
            {
                eDepId.Text = "-1";
            }
            if (eSalary.Text.Equals(""))
            {
                invalidESalary.Visibility = Visibility.Visible;
                ans = false;
            }
            if (eSupervisor.Text.Equals(""))
            {
                eSupervisor.Text = "-1";
            }
            return ans;
        }

    }
}