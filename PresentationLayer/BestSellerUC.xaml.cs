﻿using Backend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for BestSellerUCSpecific.xaml
    /// Best seller User control to attach to main window on load.
    /// </summary>
    public partial class BestSellerUC : UserControl
    {
        public BestSellerUC(Product product)
        {
            InitializeComponent();
            this.DataContext = product;
        }
    }
}
