﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Backend;
using System.Collections.ObjectModel;
using Buisness_Layer;
using System.Text.RegularExpressions;
using System.Reflection;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for TransactionScreen.xaml
    /// </summary>
    public partial class TransactionScreen : Window, ScreenInterface
    {
        private ObservableCollection<Reciept> recieptList = new ObservableCollection<Reciept>();
        private Buisness_Interface bl;
        private ReturnMessage message;
        private List<Product> tmpProductList;

        public TransactionScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            returnType.ItemsSource = Enum.GetValues(typeof(Transaction.ReturnTypes));
            returnType.SelectedItem = Transaction.ReturnTypes.Empty;
            payment.ItemsSource = Enum.GetValues(typeof(Transaction.paymentMethodTypes));
            payment.SelectedItem = Transaction.paymentMethodTypes.Empty;
            this.bl = bl;

        }

        public void Run()
        {
            this.Show();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new MainScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        private void checkProducts_Click(object sender, RoutedEventArgs e)
        {
            int id = -1;
            if (!pId.Text.Equals(""))
                id = Convert.ToInt32(pId.Text);
            int price = -1;
            if (!pPrice.Text.Equals(""))
                price = Convert.ToInt32(pPrice.Text);
            string name = "";
            if (!pName.Text.Equals(""))
                name = pName.Text;
            tmpProductList = bl.ProductGet(new Product(id,name,Product.productTypes.Empty,-1,-1,price,null),-1,-1);
            productSearch.ItemsSource = tmpProductList;
        }

        private void onlyNumbers(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void productSearch_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            quantity.Visibility = Visibility.Visible;
            quantityBox.Visibility = Visibility.Visible;
        }

        private void quantityBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Product p = (Product)productSearch.SelectedItem;
                if (Convert.ToInt32(quantityBox.Text) <= p.StockCount)
                {
                    recieptList.Add(new Reciept(p.InventoryID,p.Name,p.Price,Convert.ToInt32(quantityBox.Text),p.StockCount));
                    inReciept.ItemsSource = recieptList;
                    p.StockCount = p.StockCount - Convert.ToInt32(quantityBox.Text);
                    foreach (Product product in tmpProductList)
                        if (product.Equals(p))
                        {
                            tmpProductList.Remove(product);
                            tmpProductList.Add(p);
                            break;
                        } 
                    productSearch.ItemsSource = tmpProductList;
                    productSearch.Items.Refresh();
                }
            }
        }

        private class Reciept
        {
            public int inventoryID { get; set; }
            public string name { get; set; }
            public int price { get; set; }
            public int quantity { get; set; }

            public Reciept(int inventoryID, string name, int price, int quantity, int stockCount)
            {
                this.inventoryID = inventoryID;
                this.name = name;
                this.quantity = quantity;
                this.price = price * quantity;
            }
        }

        private void addTransaction_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Customer customer = bl.CustomerGetByUser(new User(-1, customerUser.Text, ""));
                ClubMember clubmember = bl.ClubMemberGetByUser(new User(-1, customerUser.Text, ""));

                List<Transaction.ProductInReceipt> productInReceipt = new List<Transaction.ProductInReceipt>();
                foreach (Reciept r in recieptList)
                {
                    Transaction.ProductInReceipt rec = new Transaction.ProductInReceipt(r.inventoryID, r.name, r.price, r.quantity);
                    productInReceipt.Add(rec);
                }
                MessageBoxResult areYouSure = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (areYouSure == MessageBoxResult.Yes)
                {
                    int id = -1;
                    if (customer != null) id = customer.Id;
                    if (clubmember != null) id = clubmember.Id;
                    message = bl.TransactionAdd(new Transaction(date.DisplayDate, id, (Transaction.ReturnTypes)Enum.Parse(typeof(Transaction.ReturnTypes), returnType.Text), productInReceipt, new Transaction.PaymentMethod((Transaction.paymentMethodTypes)Enum.Parse(typeof(Transaction.paymentMethodTypes), payment.Text), "")));
                    if (message.Success == true)
                    {
                        var nextScreen = new MainScreen(bl);
                        foreach (Product p in productSearch.ItemsSource)
                        {
                            bl.ProductEdit(p);
                        }
                        this.Hide();
                        nextScreen.Closed += (s, args) => this.Close();
                        nextScreen.Show();
                    }
                    else
                        invalidMessage.Content = message.Message;
                }
            }
            catch (Exception ex)
            {
                MessageBoxResult msg = MessageBox.Show("Some fields have invalid arguments", "", MessageBoxButton.OK);
            }
        }

    }

}
