﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using Buisness_Layer;
using Backend;

namespace PresentationLayer
{
    public class ProductCollection : ObservableCollection<Product>
    {
        public string Name;
        public bool InStock;
        public int Price;
        private Buisness_Interface bl = new Buisness();

        public ProductCollection()
            : base()
        {
            var list = bl.GetQueriesProducts(new string[] { "" });
            foreach (var i in list)
            {
                Add(new Product(i));
            }
        }

    }
}


