﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Backend;
using Buisness_Layer;
using System.Text.RegularExpressions;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for DepartmentScreen.xaml
    /// </summary>
    public partial class DepartmentScreen : Window, ScreenInterface
    {
        private ScreenInterface nextScreen;
        private ReturnMessage message;
        private Buisness_Interface bl;

        public DepartmentScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
        }

        public void Run()
        {
            this.Show();
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            nextScreen = new MainScreen(bl);
            nextScreen.Run();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            MessageBoxResult areYouSure = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (areYouSure == MessageBoxResult.Yes)
            {
                message = bl.DepartmentAdd(new Department(name.Text));
                if (message.Success != true)
                {
                    nextScreen = new MainScreen(bl);
                    this.Hide();
                    nextScreen.Run();
                }
                else
                    invalidMessage.Content = message.Message;
            }
        }

        private void onlyNumbers(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ReturnMessage msg = bl.DepartmentEdit(new Department(Convert.ToInt32(id.Text), name.Text));
            if (msg.Success.Equals(true))
            {
                nextScreen = new MainScreen(bl);
                this.Hide();
                nextScreen.Run();
            }
            else
            {
                invalidMessage.Content = msg.Message;
            }
               
        }

    }
}
