﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Backend;
using Buisness_Layer;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for BuyScreen.xaml
    /// A class representing the buy screen which comes after cart screen on button click
    /// </summary>
    public partial class BuyScreen : Window
    {
        private Buisness_Interface bl;
        private User user;
        private Transaction transaction;
        public BuyScreen(Transaction transaction, Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
            this.DataContext = GlobalVariable.customer;
            this.transaction = transaction;

            int sum = 0;
            foreach(Transaction.ProductInReceipt productInReceipt in transaction.Receipt)
                sum += productInReceipt.Price * productInReceipt.Quantity;

            this.totalPrice.Content = sum;

            comboType.ItemsSource = Enum.GetValues(typeof(Customer.paymentMethodTypes));
            comboType.SelectedIndex = 0;
        }


        private void Buy_Click(object sender, RoutedEventArgs e)
        {
            if (comboType.SelectedItem.Equals(Customer.paymentMethodTypes.visa) && !detailsTB.Text.Equals(""))
            {
                transaction.Payment.PaymentMethodType = Transaction.paymentMethodTypes.visa;
                transaction.Payment.AdditionInfo = detailsTB.Text;

                if (comboSave.SelectedItem != null && comboSave.SelectedItem.Equals("Yes"))
                {
                    if (GlobalVariable.clubMember != null)
                    {
                        GlobalVariable.clubMember.Payment.PaymentMethodType = Customer.paymentMethodTypes.visa;
                        GlobalVariable.clubMember.Payment.AdditionInfo = detailsTB.Text;
                        bl.ClubMemberEdit(GlobalVariable.clubMember);

                        ReturnMessage ret = bl.TransactionAdd(transaction);
                        if (ret.Success)
                        {
                            MessageBox.Show("Transaction has been recieved, thank you!");
                            var nextScreen = new CartScreen(bl);
                            this.Hide();
                            nextScreen.Closed += (s, args) => this.Close();
                            nextScreen.Show();
                        }
                        else
                            MessageBox.Show(ret.Message);
                    }
                    else if (GlobalVariable.customer != null)
                    {
                        GlobalVariable.customer.Payment.PaymentMethodType = Customer.paymentMethodTypes.visa;
                        GlobalVariable.customer.Payment.AdditionInfo = detailsTB.Text;
                        bl.CustomerEdit(GlobalVariable.customer);

                        ReturnMessage ret = bl.TransactionAdd(transaction);
                        if (ret.Success)
                        {
                            MessageBox.Show("Transaction has been recieved, thank you!");
                            var nextScreen = new CartScreen(bl);
                            this.Hide();
                            nextScreen.Closed += (s, args) => this.Close();
                            nextScreen.Show();
                        }
                        else
                            MessageBox.Show(ret.Message);
                    }
                }
                else
                {
                    if (GlobalVariable.clubMember != null)
                    {
                        ReturnMessage ret = bl.TransactionAdd(transaction);
                        if (ret.Success)
                        {
                            MessageBox.Show("Transaction has been recieved, thank you!");
                            var nextScreen5 = new CartScreen(bl);
                            this.Hide();
                            nextScreen5.Closed += (s, args) => this.Close();
                            nextScreen5.Show();
                        }
                        else
                            MessageBox.Show(ret.Message);
                    }
                    else if (GlobalVariable.customer != null)
                    {
                        ReturnMessage ret = bl.TransactionAdd(transaction);
                        if (ret.Success)
                        {
                            MessageBox.Show("Transaction has been recieved, thank you!");
                            var nextScreen = new CartScreen(bl);
                            this.Hide();
                            nextScreen.Closed += (s, args) => this.Close();
                            nextScreen.Show();
                        }
                        else
                            MessageBox.Show(ret.Message);
                    }
                }
            }
            else if(comboType.Text.ToLower().Equals("mastercard") && !detailsTB.Text.Equals(""))
            {
                transaction.Payment.PaymentMethodType = Transaction.paymentMethodTypes.masterCard;
                transaction.Payment.AdditionInfo = detailsTB.Text;

                if (comboSave.SelectedItem != null && comboSave.SelectedItem.Equals("Yes"))
                {
                    if (GlobalVariable.clubMember != null)
                    {
                        GlobalVariable.clubMember.Payment.PaymentMethodType = Customer.paymentMethodTypes.masterCard;
                        GlobalVariable.clubMember.Payment.AdditionInfo = detailsTB.Text;
                        bl.ClubMemberEdit(GlobalVariable.clubMember);

                        ReturnMessage ret = bl.TransactionAdd(transaction);
                        if (ret.Success)
                        {
                            MessageBox.Show("Transaction has been recieved, thank you!");
                            var nextScreen = new CartScreen(bl);
                            this.Hide();
                            nextScreen.Closed += (s, args) => this.Close();
                            nextScreen.Show();
                        }
                        else
                            MessageBox.Show(ret.Message);
                    }
                    else if (GlobalVariable.customer != null)
                    {
                        GlobalVariable.customer.Payment.PaymentMethodType = Customer.paymentMethodTypes.masterCard;
                        GlobalVariable.customer.Payment.AdditionInfo = detailsTB.Text;
                        bl.CustomerEdit(GlobalVariable.customer);

                        ReturnMessage ret = bl.TransactionAdd(transaction);
                        if (ret.Success)
                        {
                            MessageBox.Show("Transaction has been recieved, thank you!");
                            var nextScreen = new CartScreen(bl);
                            this.Hide();
                            nextScreen.Closed += (s, args) => this.Close();
                            nextScreen.Show();
                        }
                        else
                            MessageBox.Show(ret.Message);
                    }
                }
                else
                {
                    if (GlobalVariable.clubMember != null)
                    {
                        ReturnMessage ret = bl.TransactionAdd(transaction);
                        if (ret.Success)
                        {
                            MessageBox.Show("Transaction has been recieved, thank you!");
                            var nextScreen = new CartScreen(bl);
                            this.Hide();
                            nextScreen.Closed += (s, args) => this.Close();
                            nextScreen.Show();
                        }
                        else
                            MessageBox.Show(ret.Message);
                    }
                    else if (GlobalVariable.customer != null)
                    {
                        ReturnMessage ret = bl.TransactionAdd(transaction);
                        if (ret.Success)
                        {
                            MessageBox.Show("Transaction has been recieved, thank you!");
                            var nextScreen = new CartScreen(bl);
                            this.Hide();
                            nextScreen.Closed += (s, args) => this.Close();
                            nextScreen.Show();
                        }
                        else
                            MessageBox.Show(ret.Message);
                    }
                }
            }
        }

        private void comboSave_Initialized(object sender, EventArgs e)
        {
            comboSave.Items.Add("Yes");
            comboSave.Items.Add("No");
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new CartScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

    }
}
