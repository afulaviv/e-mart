﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.ComponentModel;
using Backend;
using Buisness_Layer;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for SearchScreen.xaml
    /// Search screen to check out products.
    /// </summary>
    public partial class SearchScreen : Window, ScreenInterface
    {
        private List<Product> queryReply;
        private List<CheckBox> productTypesCheckBox;
        private User user;
        private Buisness_Interface bl;

        public SearchScreen(List<Product> search, Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
            //DataGridTemplateColumn col1 = new DataGridTemplateColumn();
            //col1.Header = "MyHeader";
            //FrameworkElementFactory factory1 = new FrameworkElementFactory(typeof(Image));
            //Binding b1 = new Binding(search[0].Image);
            //b1.Mode = BindingMode.TwoWay;
            //factory1.SetValue(Image.SourceProperty, b1);
            //DataTemplate cellTemplate1 = new DataTemplate();
            //cellTemplate1.VisualTree = factory1;
            //col1.CellTemplate = cellTemplate1;
            //searchTable.Columns.Add(col1);

            //Image finalImage = new Image();
            //finalImage.Width = 80;

            //BitmapImage logo = new BitmapImage();
            //logo.BeginInit();
            //logo.UriSource = new Uri(search[0].ImageURL);
            //logo.EndInit();
            //asdf.Source = logo;


            //asdf.Source = new System.Windows.Controls.Image();

            queryReply = search;
            searchTable.ItemsSource = search;



            //ObservableCollection<User> users = new ObservableCollection<User>();
            //users.Add(new User { FirstName = "firstname-1", LastName = "lastname-1" });
            //users.Add(new User { FirstName = "firstname-2", LastName = "lastname-2" });
            //users.Add(new User { FirstName = "firstname-3", LastName = "lastname-3" });
            //users.Add(new User { FirstName = "firstname-4", LastName = "lastname-4" });
            //DataGrid.ItemsSource = users;
        }

        public void Run()
        {
            this.Show();
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void filtersDockPane_Initialized(object sender, EventArgs e)
        {
            productTypesCheckBox = new List<CheckBox>();

            foreach (Backend.Product.productTypes type in Enum.GetValues(typeof(Backend.Product.productTypes)))
            {
                CheckBox checkBox = new CheckBox();
                checkBox.Content = type.ToString();
                checkBox.Checked += new RoutedEventHandler(handleCheckBoxCheck);
                checkBox.Unchecked += new RoutedEventHandler(handleCheckBoxCheck);
                checkBox.Margin = new Thickness(3);
                productTypesCheckBox.Add(checkBox);
            }

            StackPanel stackPanel = new StackPanel();
            stackPanel.Orientation = Orientation.Vertical;
            foreach (CheckBox checkBox in productTypesCheckBox)
                stackPanel.Children.Add(checkBox);

            filtersDockPane.Children.Add(stackPanel);
        }

        private void handleCheckBoxCheck(object sender, RoutedEventArgs e)
        {
            List<Object> filteredObjectList = new List<Object>();
            bool replaceOriginalLists = false;

            foreach (CheckBox checkBox in productTypesCheckBox)
                if(checkBox.IsChecked.Equals(true))
                    foreach (Object row in queryReply)
                        if (row.GetType().Equals(typeof(Backend.Product)))
                        {
                            replaceOriginalLists = true;
                            if(((Backend.Product) row).Type.ToString().Equals(checkBox.Content))
                                filteredObjectList.Add(row);
                        }

            if (replaceOriginalLists)
                searchTable.ItemsSource = filteredObjectList;
            else
                searchTable.ItemsSource = queryReply;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new MainScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }
    }
}