﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Buisness_Layer;
using System.Text.RegularExpressions;
using Backend;
using System.IO;
using System.Drawing;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for ProductScreen.xaml
    /// </summary>
    public partial class ProductScreen : Window, ScreenInterface
    {
        private Buisness_Interface bl;
        private ReturnMessage message;

        public ProductScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
            comboType.ItemsSource = Enum.GetValues(typeof(Product.productTypes));
            comboType.SelectedIndex = 0;
        }

        public void Run()
        {
            this.Show();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                MessageBoxResult areYouSure = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
                if (areYouSure == MessageBoxResult.Yes)
                {
                    int imageIndex = 0;
                    while (File.Exists(System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName + "\\media\\" + filename + imageIndex + ".png"))
                        imageIndex++;
                    if (location.Text.Equals(""))
                        location.Text = "-1";
                    message = bl.ProductAdd(new Product(Convert.ToInt32(inventoryID.Text), productName.Text, (Product.productTypes)Enum.Parse(typeof(Product.productTypes), comboType.Text), Convert.ToInt32(location.Text), Convert.ToInt32(stockCount.Text), Convert.ToInt32(price.Text), System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName + "\\media\\" + filename + imageIndex + ".png"));
                    if (message.Success == true)
                    {

                        SaveImageToJPEG(System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName + "\\media\\" + filename + imageIndex + ".png");

                        var nextScreen = new MainScreen(bl);
                        this.Hide();
                        nextScreen.Closed += (s, args) => this.Close();
                        nextScreen.Show();
                    }
                    else
                        invalidMessage.Content = message.Message;
                }
            }
            catch (Exception ex)
            {
                MessageBoxResult msg = MessageBox.Show("Some fields have invalid arguments", "", MessageBoxButton.OK);
            }
        }

        private void onlyNumbers(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void SaveImageToJPEG(string Location)
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = imageUri;
            bi.DecodePixelHeight = 144;
            bi.DecodePixelWidth = 137;
            bi.EndInit();

            System.Windows.Media.Imaging.PngBitmapEncoder pngBitmapEncoder = new System.Windows.Media.Imaging.PngBitmapEncoder();
            System.IO.FileStream stream = new System.IO.FileStream(Location, FileMode.Create);

            pngBitmapEncoder.Interlace = PngInterlaceOption.On;
            pngBitmapEncoder.Frames.Add(System.Windows.Media.Imaging.BitmapFrame.Create(bi));
            pngBitmapEncoder.Save(stream);
            stream.Flush();
            stream.Close();
        }

        private string filename;
        private Uri imageUri;

        private void ImageAdd_Click(object sender, RoutedEventArgs e)
        {
            // Create OpenFileDialog 
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();



            // Set filter for file extension and default file extension 
            dlg.DefaultExt = ".png";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg|PNG Files (*.png)|*.png|JPEG Files (*.jpeg)|*.jpg|GIF Files (*.gif)|*.gif";


            // Display OpenFileDialog by calling ShowDialog method 
            Nullable<bool> result = dlg.ShowDialog();


            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                imageUri = new Uri(@dlg.FileName);
                img.Source = new BitmapImage(new Uri(@dlg.FileName));
                filename = @dlg.SafeFileName;
            }
            else
            {
                filename = "";
                imageUri = null;
            }
                
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        private void Back_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new MainScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Product p = bl.ProductGetByID(Convert.ToInt32(inventoryID.Text));
                p.Name = productName.Text;
                p.Type = (Product.productTypes)comboType.SelectedItem;
                p.Location = Convert.ToInt32(location.Text);
                p.StockCount = Convert.ToInt32(stockCount.Text);
                p.Price = Convert.ToInt32(price.Text);
                int imageIndex = 0;
                while (File.Exists(System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName + "\\media\\" + filename + imageIndex + ".png"))
                    imageIndex++;
                p.ImageURL = System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName + "\\media\\" + filename + imageIndex + ".png";
                ReturnMessage msg = bl.ProductEdit(p);
                if (msg.Success == true)
                {

                    SaveImageToJPEG(System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName + "\\media\\" + filename + imageIndex + ".png");

                    var nextScreen = new MainScreen(bl);
                    this.Hide();
                    nextScreen.Closed += (s, args) => this.Close();
                    nextScreen.Show();
                }
                else
                    invalidMessage.Content = message.Message;
            }
            catch (Exception ex)
            {
                MessageBoxResult msg = MessageBox.Show("Some fields have invalid arguments", "", MessageBoxButton.OK);
            }
        }
    }
}
