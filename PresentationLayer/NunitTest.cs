﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;
using NUnit.Framework;
using Data_Access_Layer;

namespace Buisness_Layer
{
    [TestFixture]

    class NunitTest
    {
        private Buisness_Interface b;

        [SetUp]
        public void NunitTestMethod()
        {
            b = new Buisness(new Data_Access());
        }

        [Test]
        public void CheckIfClubMemberAdded()
        {
            b.ClubMemberAdd(new ClubMember(-1, "a1", "AS", User.PermissionType.Worker, -100, "1", "2", ClubMember.GenderTypes.male, DateTime.Now, new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "")));
            // checks if the user is added successfully.
            Assert.NotNull(b.ClubMemberGet(b.ClubMemberGetByUser(new User(-1, "a1", "AS")).Id));
            b.ClubMemberRemove(b.ClubMemberGet(new ClubMember(-1, "a1", "AS", User.PermissionType.Worker, -100, "1", "2", ClubMember.GenderTypes.male, DateTime.Now, new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "")), DateTime.MinValue, DateTime.MinValue, -1, -1, -1)[0].Id);
        }

        [Test]
        public void ClubMemberGetByID()
        {
            b.ClubMemberAdd(new ClubMember(-1, "a2", "AS", User.PermissionType.Worker, -100, "1", "2", ClubMember.GenderTypes.male, DateTime.Now, new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "")));
            // checks if the wanted club member we wanted is the one who we retrieved.
            Assert.AreNotEqual(-1, b.ClubMemberGet(b.ClubMemberGetByUser(new User(-1, "a2", "AS")).Id));
            b.ClubMemberRemove(b.ClubMemberGet(new ClubMember(-1, "a2", "AS", User.PermissionType.Worker, -100, "1", "2", ClubMember.GenderTypes.male, DateTime.Now, new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "")), DateTime.MinValue, DateTime.MinValue, -1, -1, -1)[0].Id);
        }

        [Test]
        public void ClubMemberIsRemoved()
        {
            b.UserAny();
            b.ClubMemberAdd(new ClubMember(-1, "a3", "AS", User.PermissionType.Worker, -100, "1", "2", new List<int>(), ClubMember.GenderTypes.male, DateTime.Now, new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, ""), new List<Customer.ProductIdQuantity>()));
            b.ClubMemberRemove(b.ClubMemberGetByUser(new User(-1, "a3", "AS")).Id);
            // checks if the club member is removed.
            Assert.Null(b.ClubMemberGetByUser(new User(-1, "a3", "AS")));
        }

        [Test]
        public void CheckIfCustomerClassCanBeAdded()
        {
            b.CustomerAdd(new Customer(-1, "a1", "111", User.PermissionType.Customer, "Nunit", "Test1", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()));
            // checks if the customer is added successfully.
            Assert.NotNull(b.CustomerGet(new Customer(-1, "a1", "111", User.PermissionType.Customer, "Nunit", "Test1", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), new List<Customer.ProductIdQuantity>())));
            b.CustomerRemove(b.CustomerGet(new Customer(-1, "a1", "111", User.PermissionType.Customer, "Nunit", "Test1", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()))[0].Id);
        }

        [Test]
        public void CheckIfBestSellingItemIsWorking()
        {
            b.CustomerAdd(new Customer(-1, "a2", "111", User.PermissionType.Customer, "Nunit", "Test2", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()));
            b.ProductAdd(new Product(101, "Nunit Product", Product.productTypes.Antiques, -1, 100, 1000));

            List<Customer> customerList = b.CustomerGet(new Customer(-1, "a1", "111", User.PermissionType.Customer, "Nunit", "Test2", null, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), null));

            b.TransactionAdd(new Transaction(DateTime.Now, customerList[0].Id, Transaction.ReturnTypes.Purchase, new List<Transaction.ProductInReceipt>{ new Transaction.ProductInReceipt(b.ProductGet(new Product(-1, "Nunit Product", Product.productTypes.Antiques, -1, -1, -1),-1,-1)[0].InventoryID, "Nunit Product", 100, 1000)}, new Transaction.PaymentMethod(Transaction.paymentMethodTypes.visa, "1234")));
            
            // checks if the product is now the best seller
            List<int> productList = b.getBestSeller();
            Assert.AreEqual("Nunit Product", b.ProductGetByID(productList[0]).Name);
            b.CustomerRemove(b.CustomerGet(new Customer(-1, "a2", "111", User.PermissionType.Customer, "Nunit", "Test2", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()))[0].Id);
            b.ProductRemove(b.ProductGet(new Product(-1, "Nunit Product", Product.productTypes.Antiques, -1, 100, 1000), -1, -1)[0].InventoryID);
        }

        [Test]
        public void CheckIfWeCanBuyMoreThanThereIsFromSomeProduct()
        {
            b.CustomerAdd(new Customer(-1, "a3", "111", User.PermissionType.Customer, "Nunit", "Test3", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()));
            b.ProductAdd(new Product(102, "Nunit Product2", Product.productTypes.Antiques, -1, 100, 1000));

            // checks if we can buy more than there is in the stock
            List<int> productList = b.getBestSeller();
            //int a = b.CustomerGetByUser(new User(-1, "a3", "111", User.PermissionType.Worker)).Id;
            Assert.AreEqual(false, b.TransactionAdd(new Transaction(DateTime.Now, b.CustomerGetByUser(new User(-1, "a3", "111", User.PermissionType.Worker)).Id, Transaction.ReturnTypes.Purchase, new List<Transaction.ProductInReceipt> { new Transaction.ProductInReceipt(b.ProductGet(new Product(-1, "Nunit Product2", Product.productTypes.Empty, -1, -1, -1, ""), -1, -1)[0].InventoryID, "Nunit Product", 10000, 1000) }, new Transaction.PaymentMethod(Transaction.paymentMethodTypes.visa, "1234"))).Success);
            b.CustomerRemove(b.CustomerGet(new Customer(-1, "a3", "111", User.PermissionType.Customer, "Nunit", "Test3", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()))[0].Id);
            b.ProductRemove(b.ProductGet(new Product(-1, "Nunit Product2", Product.productTypes.Antiques, -1, 100, 1000), -1, -1)[0].InventoryID);
        }

        [Test]
        public void ChecksIfCostumerCanSaveHisCart()
        {
            b.CustomerAdd(new Customer(-1, "a4", "111", User.PermissionType.Customer, "Nunit", "Test5", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()));
            b.ProductAdd(new Product(104, "Nunit Product4", Product.productTypes.Antiques, -1, 100, 1000));

            List<Product> product = b.ProductGet(new Product(-1, "Nunit Product4", Product.productTypes.Empty, -1, -1, -1), -1, -1);

            Customer customer = b.CustomerGet(new Customer(-1, "a4", "111", User.PermissionType.Customer, "Nunit", "Test5", null, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), new List<Customer.ProductIdQuantity>()))[0];

            customer.Cart.Add(new Customer.ProductIdQuantity(product[0].InventoryID, 1));
            b.CustomerEdit(customer);

            customer = b.CustomerGet(customer)[0];

            // checks if there is anything in the cart
            Assert.AreEqual(1, customer.Cart.Count);
            b.CustomerRemove(b.CustomerGet(new Customer(-1, "a4", "111", User.PermissionType.Customer, "Nunit", "Test5", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()))[0].Id);
            b.ProductRemove(b.ProductGet(new Product(104, "Nunit Product4", Product.productTypes.Antiques, -1, 100, 1000), -1, -1)[0].InventoryID);
        }

        [Test]
        public void ChecksIfCostumerCanBeEdited()
        {
            b.CustomerAdd(new Customer(-1, "a5", "111", User.PermissionType.Customer, "Nunit", "Test6", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()));
            List<Customer> customer = b.CustomerGet(new Customer(-1, "a5", "111", User.PermissionType.Customer, "Nunit", "Test6", null, new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()));
            customer[0].Payment = new Customer.PaymentMethod(Customer.paymentMethodTypes.visa, "4321");
            b.CustomerEdit(customer[0]);
            Customer.PaymentMethod p = b.CustomerGet(customer[0].Id).Payment;
            // to return to original

            // checks if there is anything in the cart
            Assert.AreEqual(new Customer.PaymentMethod(Customer.paymentMethodTypes.visa, "4321"), p);
            b.CustomerRemove(b.CustomerGet(new Customer(-1, "a5", "111", User.PermissionType.Customer, "Nunit", "Test6", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()))[0].Id);
        }

        [Test]
        public void CheckIfFiltersAreWorking()
        {
            b.ProductAdd(new Product(106, "Nunit Product7", Product.productTypes.Garden, -1, 100, 1000));
            b.ProductAdd(new Product(107, "Nunit Product8", Product.productTypes.Jewelry, -1, 100, 1000));

            List<Product> productList = b.GetQueriesProducts(new String[1] { "garden" });

            // checks if there is anything in the cart
            Assert.AreEqual(0, productList.Count);
            b.ProductRemove(b.ProductGet(new Product(106, "Nunit Product7", Product.productTypes.Garden, -1, 100, 1000), -1, -1)[0].InventoryID);
            b.ProductRemove(b.ProductGet(new Product(107, "Nunit Product8", Product.productTypes.Jewelry, -1, 100, 1000), -1, -1)[0].InventoryID);
        }

        [Test]
        public void CheckIfCustomerAndClubMemberCanHaveTheSameInsertedID()
        {
            b.CustomerAdd(new Customer(-1, "a5", "111", User.PermissionType.Customer, "Nunit", "Test9", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()));

            // checks if there is anything in the cart
            Assert.AreEqual(true, b.ClubMemberAdd(new ClubMember(-1, "a3", "111", User.PermissionType.Customer, 108, "Nunit", "Test9", new List<int>(), ClubMember.GenderTypes.male, DateTime.Now, new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>())).Success);
            b.CustomerRemove(b.CustomerGet(new Customer(-1, "a5", "111", User.PermissionType.Customer, "Nunit", "Test9", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>()))[0].Id);
        }
        /// <summary>
        /// //////////
        /// </summary>
        [Test]
        public void checkIfClubMemberCanBeAddedDoubleTime()
        {
            b.ClubMemberAdd(new ClubMember(-1, "s1", "1111", User.PermissionType.Worker, 123, "1", "!", new List<int>(), ClubMember.GenderTypes.male, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), new List<Customer.ProductIdQuantity>()));
            
            // checks if there is anything in the cart
            Assert.AreEqual(false, b.ClubMemberAdd(new ClubMember(-1, "s1", "1111", User.PermissionType.Worker, 123, "1", "!", new List<int>(), ClubMember.GenderTypes.male, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), new List<Customer.ProductIdQuantity>())).Success);
            b.ClubMemberRemove(b.ClubMemberGet(new ClubMember(-1, "s1", "1111", User.PermissionType.Worker, 123, "1", "!", new List<int>(), ClubMember.GenderTypes.male, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), new List<Customer.ProductIdQuantity>()), DateTime.MinValue, DateTime.MinValue,-1,-1,-1)[0].Id);
        }

        [Test]
        public void checkIfPasswordCanBeRetrieved()
        {
            b.ClubMemberAdd(new ClubMember(-1, "s1", "1111", User.PermissionType.Worker, 123, "1", "!", new List<int>(), ClubMember.GenderTypes.male, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""), new List<Customer.ProductIdQuantity>()));

            // checks if there is anything in the cart
            Assert.AreEqual(true, b.ClubMemberAdd(new ClubMember(-1, "a3", "111", User.PermissionType.Customer, 108, "Nunit", "Test9", new List<int>(), ClubMember.GenderTypes.male, DateTime.Now, new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>())).Success);
        }

        [Test]
        public void checkIfThereCanBeMultipleUserName()
        {
            b.ClubMemberAdd(new ClubMember(-1, "s1", "1111", User.PermissionType.Worker, 123, "a", "asdf", ClubMember.GenderTypes.male, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, "")));

            // checks if there is anything in the cart
            Assert.AreEqual(false, b.ClubMemberAdd(new ClubMember(-1, "s1", "1111", User.PermissionType.Worker, 123, "a", "asdf", ClubMember.GenderTypes.male, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""))).Success);
        }

        [Test]
        public void checkIfThereCanBeTransactionWithFalseMemberID()
        {
            Assert.AreEqual(false, b.TransactionAdd(new Transaction(DateTime.MinValue, -1, Transaction.ReturnTypes.Returned, new List<Transaction.ProductInReceipt>(), new Transaction.PaymentMethod(Transaction.paymentMethodTypes.masterCard, ""))).Success);
        }

        [Test]
        public void checkIfThereCanBeEmployeeWithEmployeeSameNameAsClubMember()
        {
            b.EmployeeAdd(new Employee(-1, "ss1", "", User.PermissionType.Worker, -1, "", "", -1, -1, Employee.GenderTypes.Empty, -1));
            
            // checks if there is anything in the cart
            Assert.AreEqual(false, b.ClubMemberAdd(new ClubMember(-1, "ss1", "1111", User.PermissionType.Worker, 123, "a", "asdf", ClubMember.GenderTypes.male, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, ""))).Success);
        }

        [Test]
        public void checkIfThereCanBeEmployeeWithCustomerSameNameAsClubMember()
        {
            b.ClubMemberAdd(new ClubMember(-1, "dsa", "1111", User.PermissionType.Worker, 123, "a", "asdf", ClubMember.GenderTypes.male, DateTime.MinValue, new Customer.PaymentMethod(Customer.paymentMethodTypes.Empty, "")));

            // checks if there is anything in the cart
            Assert.AreEqual(false, b.CustomerAdd(new Customer(-1, "dsa", "111", User.PermissionType.Customer, "Nunit", "Test9", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>())).Success);
        }

        [Test]
        public void checkIfThereCanBeEmployeeWithCustomerSameNameAsEmployee()
        {
            b.EmployeeAdd(new Employee(-1, "msc", "", User.PermissionType.Worker, -1, "", "", -1, -1, Employee.GenderTypes.Empty, -1));

            // checks if there is anything in the cart
            Assert.AreEqual(false, b.CustomerAdd(new Customer(-1, "msc", "111", User.PermissionType.Customer, "Nunit", "Test9", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.masterCard, "1234"), new List<Customer.ProductIdQuantity>())).Success);
        }
    }
}
