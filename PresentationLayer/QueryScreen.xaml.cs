﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Buisness_Layer;
using Backend;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for QueryScreen.xaml
    /// </summary>
    public partial class QueryScreen : Window, ScreenInterface
    {
        private Buisness_Interface bl;

        public QueryScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            queryData.Height = 350; //400
            queryData.Width = 550; //600
            queryData.Visibility = Visibility.Collapsed;
            this.bl = bl;
        }



        public void Run()
        {
            this.Show();
        }

        private void searchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                queryData.ItemsSource =  bl.GetQueries(searchBox.Text);
                queryData.Visibility = Visibility.Visible;
                headlineLabel.Visibility = Visibility.Collapsed;
                searchBox.Visibility = Visibility.Collapsed;
                this.Height = 400;
                this.Width = 600;
                if (queryData.ItemsSource.GetType().Equals(typeof(List<Employee>)))
                {
                    queryData.Columns.RemoveAt(4);
                }
            }
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        //private void queryData_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        //{
        //    test.Content = queryData.ItemsSource.GetType().ToString();
        //    if (queryData.ItemsSource.GetType().Equals(typeof(List<Product>)))
        //    {
        //        Product p = (Product)queryData.SelectedItem;
        //        test.Content = p.Name;
        //    }
        //}

        
    }
}
