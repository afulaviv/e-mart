﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;
using System.Windows.Shapes;
using Backend;
using Buisness_Layer;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for EmployeeScreen.xaml
    /// </summary>
    public partial class EmployeeScreen : Window, ScreenInterface
    {
        private ReturnMessage message;
        private Buisness_Interface bl;

        public EmployeeScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
            this.Height = 402;
            this.Width = 389;
            empGrid.Visibility = Visibility.Collapsed;
            addEmp.Visibility = Visibility.Visible;
            removeEmp.Visibility = Visibility.Visible;
            RMB.Visibility = Visibility.Collapsed;
            RMB2.Visibility = Visibility.Collapsed;
        }

        public void Run()
        {
            this.Show();
        }

        private void onlyNumbers(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            empGrid.Visibility = Visibility.Collapsed;
            addEmp.Visibility = Visibility.Visible;
            removeEmp.Visibility = Visibility.Visible;
            RMB.Visibility = Visibility.Collapsed;
            RMB2.Visibility = Visibility.Collapsed;
        }

        private void edit_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult areYouSure = System.Windows.MessageBox.Show("Are you sure?", "Delete Confirmation", System.Windows.MessageBoxButton.YesNo);
            if (areYouSure == MessageBoxResult.Yes)
            {
                Employee employee = bl.EmployeeGetById(Convert.ToInt32(id.Text));
                employee.FirstName = fn.Text;
                employee.LastName = ln.Text;
                employee.DepartmentID = Convert.ToInt32(depId.Text);
                employee.Salary = Convert.ToInt32(salary.Text);
                employee.Gender = (Employee.GenderTypes)comboGender.SelectedItem;
                employee.SuperVisorID = Convert.ToInt32(supervisor.Text);

                message = bl.EmployeeEdit(employee);
                //message = bl.EmployeeAdd(new Employee(-1, Convert.ToInt32(id.Text), fn.Text, ln.Text, Convert.ToInt32(depId.Text), Convert.ToInt32(salary.Text), (Employee.GenderTypes)Enum.Parse(typeof(Employee.GenderTypes), comboGender.SelectedItem.ToString()), Convert.ToInt32(supervisor.Text)));
                if (message.Success == true)
                {
                    var nextScreen = new MainScreen(bl);
                    this.Hide();
                    nextScreen.Closed += (s, args) => this.Close();
                    nextScreen.Show();
                }
                else
                    invalidMessage.Content = message.Message;
            }
        }

        private void comboGender_Initialized(object sender, EventArgs e)
        {
            foreach (Employee.GenderTypes type in Enum.GetValues(typeof(Employee.GenderTypes)))
                comboGender.Items.Add(type);
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }
        private void addEmp_Click(object sender, RoutedEventArgs e)
        {
            empGrid.Visibility = Visibility.Visible;
            addEmp.Visibility = Visibility.Collapsed;
            removeEmp.Visibility = Visibility.Collapsed;
            RMB.Visibility = Visibility.Collapsed;
            RMB2.Visibility = Visibility.Collapsed;
        }

        private void removeMenu_Click(object sender, RoutedEventArgs e)
        {
            rmvGrid.Visibility = Visibility.Visible;
            addEmp.Visibility = Visibility.Collapsed;
            removeEmp.Visibility = Visibility.Collapsed;
            RMB.Visibility = Visibility.Visible;
            RMB2.Visibility = Visibility.Visible;
            
             }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var nextScreen = new MainScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }
    }
}
