﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Buisness_Layer;
using System.Collections;
using Backend;
using System.Text.RegularExpressions;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for LogonScreen.xaml
    /// </summary>
    public partial class LogonScreen : Window, ScreenInterface
    {
        private Buisness_Interface bl;
        private ScreenInterface nextScreen;

        public LogonScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
            
        }
        public void Run()
        {
            this.Show();
        }

        private void login_Click(object sender, RoutedEventArgs e)
        {
            GlobalVariable.clubMember = null;
            GlobalVariable.employee = null;
            GlobalVariable.customer = null;


            string userName = username.Text;
            string pass = passwordBox.Password;

            User user = bl.UserCheckCredentials(new User(-1, userName, pass));
            if (user != null)
            {
                if (user.Type == User.PermissionType.Customer)
                {
                    GlobalVariable.customer = bl.CustomerGet(user.Id);
                    GlobalVariable.clubMember = bl.ClubMemberGet(user.Id);
                }
                else
                {
                    GlobalVariable.employee = bl.EmployeeGet(user.Id);
                }
                nextScreen = new MainScreen(bl);
                this.Hide();
                nextScreen.Run();
            }
            else
            {
                userInvalid.Text = "User is not exist";
            }
        }

        private void sign_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var nextScreen = new SignInScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        private void onlyNumbers(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
