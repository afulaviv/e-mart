﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Buisness_Layer;
using Backend;
namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for Maps.xaml
    /// </summary>
    public partial class Maps : Window, ScreenInterface
    {
        //  private string htmlPage = @"C:\Users\Leon\Documents\Visual Studio 2012\Projects\CLONEEEEE\PresentationLayer\Web\DynamicMap.html";
        #region Web Page Code
        private string htmlPage = @"
<html>
  <body>
<script src='http://maps.google.com/maps/api/js?sensor=false'></script>
	<div style='overflow:hidden;height:560px;width:750px;'>
		<div id='gmap_canvas' style='height:560px;width:750px;'></div>
		<style>#gmap_canvas img{max-width:none!important;background:none!important}</style>
		<a class='google-map-code' href='http://premium-wordpress-themes.me' id='get-map-data'>volcano</a>
	</div>
	
	<script type='text/javascript'>

function initialize() {
  var mapOptions = {
    zoom: 2,
    center: new google.maps.LatLng(30.0,0,0),
	mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById('gmap_canvas'),
                                mapOptions);

  setMarkers(map, places);
}

/**
 * Data for the markers consisting of a name, a LatLng and a zIndex for
 * the order in which these markers should display on top of each
 * other.
 */
var places = [
  ['E-Mart Beersheba - Grand Canyon',31.250512, 34.771416 , 4],
  ['E-Mart TLV - Azrielli', 32.074928, 34.792770 , 5],
  ['E-Mart TLV - Rotshield', 32.070734, 34.778637 , 3],
  ['E-Mart Berlin',52.521919, 13.405417, 2],
  ['E-Mart Haifa - Cinemall', 32.792958, 35.037290 , 1],
  ['E-Mart Haifa - Grand Canyon', 32.789498, 35.006687  , 0],
  ['E-Mart Rehovot - Canyon', 31.893251, 34.805963  , 9],
  ['E-Mart Silicon Valley',37.385892, -122.037150  , 8],
  ['E-Mart MIT',42.360686, -71.093926 , 7],
  ['E-Mart Beersheba - Big',31.242618, 34.811871 ,6]
  
];

function setMarkers(map, locations) {
  // Add markers to the map

  for (var i = 0; i < locations.length; i++) {
    var place = locations[i];
	
    var myLatLng = new google.maps.LatLng(place[1], place[2]);
    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        title: place[0],
        zIndex: place[3]
    	});
     }
}

google.maps.event.addDomListener(window, 'load', initialize);

    </script>
  </body>
</html>
        ";
        #endregion Web Page Code
        private Buisness_Interface bl;

        public Maps(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
            showMap();
        }
        public void Run()
        {
            this.Show();
        }
        public void HideScriptErrors(WebBrowser wb, bool hide)
        {
            var fiComWebBrowser = typeof(WebBrowser).GetField("_axIWebBrowser2", BindingFlags.Instance | BindingFlags.NonPublic);
            if (fiComWebBrowser == null) return;
            var objComWebBrowser = fiComWebBrowser.GetValue(wb);
            if (objComWebBrowser == null)
            {
                wb.Loaded += (o, s) => HideScriptErrors(wb, hide); //In case we are to early
                return;
            }
            objComWebBrowser.GetType().InvokeMember("Silent", BindingFlags.SetProperty, null, objComWebBrowser, new object[] { hide });
        }
        private void showMap()
        {
            InternetChecker checkConnection = new InternetChecker();
            if (!checkConnection.IsConnectedToInternet())
                MessageBox.Show("Please check your internet connection.");
            else
            {
                HideScriptErrors(web, true);
                web.NavigateToString(htmlPage);
            }
        }

        private void Backbtn_Click_1(object sender, RoutedEventArgs e)
        {
            var nextScreen = new MainScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        private void Snifim_Initialized(object sender, EventArgs e)
        {
            List<String> list = new List<string>() {
            "E-Mart Beersheba - Big",
            "E-Mart Beersheba - Grand Canyon",
            "E-Mart Berlin",
            "E-Mart Haifa - Cinemall",
            "E-Mart Haifa - Grand Canyon",
            "E-Mart MIT",
            "E-Mart Rehovot - Canyon",
            "E-Mart Silicon Valley",
            "E-Mart TLV - Azrielli",
            "E-Mart TLV - Rotshield"
             };
            foreach (String i in list)
                Snifim.Items.Add(i);
        }

        private void Check_Weather_Click(object sender, RoutedEventArgs e)
        {
            /*
             ['E-Mart Beersheba - Grand Canyon',31.250512, 34.771416 , 4],
  ['E-Mart TLV - Azrielli', 32.074928, 34.792770 , 5],
  ['E-Mart TLV - Rotshield', 32.070734, 34.778637 , 3],
  ['E-Mart Berlin',52.521919, 13.405417, 2],
  ['E-Mart Haifa - Cinemall', 32.792958, 35.037290 , 1],
  ['E-Mart Haifa - Grand Canyon', 32.789498, 35.006687  , 0],
  ['E-Mart Rehovot - Canyon', 31.893251, 34.805963  , 9],
  ['E-Mart Silicon Valley',37.385892, -122.037150  , 8],
  ['E-Mart MIT',42.360686, -71.093926 , 7],
  ['E-Mart Beersheba - Big',31.242618, 34.811871 ,6]
            */
            string currentComboSt;
            try
            {
                currentComboSt = Snifim.SelectedItem.ToString();
            }
            catch (Exception ee) { currentComboSt = ""; }
            if (currentComboSt.Equals(""))
            {
                System.Windows.MessageBox.Show("Choose one of the options below and try again.");
            }
            else if (currentComboSt.Equals("E-Mart Beersheba - Big"))
            {
                string st = null;
                st = getTemperature(31.242618, 34.811871);
                string desc = getDescription(31.242618, 34.811871);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");

            }
            else if (currentComboSt.Equals("E-Mart Beersheba - Grand Canyon"))
            {
                string st = null;
                st = getTemperature(31.250512, 34.771416);
                string desc = getDescription(31.250512, 34.771416);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");
            }
            else if (currentComboSt.Equals("E-Mart Berlin"))
            {
                string st = null;
                st = getTemperature(52.521919, 13.405417);
                string desc = getDescription(52.521919, 13.405417);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");

            }
            else if (currentComboSt.Equals("E-Mart Haifa - Cinemall"))
            {
                string st = null;
                st = getTemperature(32.792958, 35.037290);
                string desc = getDescription(32.792958, 35.037290);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");

            }
            else if (currentComboSt.Equals("E-Mart Haifa - Grand Canyon"))
            {
                string st = null;
                st = getTemperature(32.789498, 35.006687);
                string desc = getDescription(32.789498, 35.006687);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");
            }
            else if (currentComboSt.Equals("E-Mart MIT"))
            {
                string st = null;
                st = getTemperature(42.360686, -71.093926);
                string desc = getDescription(42.360686, -71.093926);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");

            }
            else if (currentComboSt.Equals("E-Mart Rehovot - Canyon"))
            {
                string st = null;
                st = getTemperature(31.893251, 34.805963);
                string desc = getDescription(31.893251, 34.805963);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");
            }
            else if (currentComboSt.Equals("E-Mart Silicon Valley"))
            {
                string st = null;
                st = getTemperature(37.385892, -122.037150);
                string desc = getDescription(37.385892, -122.037150);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");
            }
            else if (currentComboSt.Equals("E-Mart TLV - Azrielli"))
            {
                string st = null;
                st = getTemperature(32.074928, 34.792770);
                string desc = getDescription(32.074928, 34.792770);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");
            }
            else if (currentComboSt.Equals("E-Mart TLV - Rotshield"))
            {
                string st = null;
                st = getTemperature(32.070734, 34.778637);
                string desc = getDescription(32.070734, 34.778637);
                if (st != null)
                {
                    if (desc == null) desc = "";
                    System.Windows.Forms.DialogResult result = MsgBox.Show("The temperature here is " + st + " Celsius.", desc, MsgBox.Buttons.OK, MsgBox.Icon.Info, MsgBox.AnimateStyle.FadeIn);
                }
                else MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");
            }
            else
            {
                MessageBox.Show("A problem has occured.\nPlease Check your internet connection.");
            }
        }

        public string getTemperature(double lat, double longt)
        {
            try
            {
                string url = @"http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + longt + "&units=metric";
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                httpWebRequest.Accept = "text/json";
                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                string result = "";
                using (System.IO.Stream stream = response.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                response.Close();
                result = result.Substring(result.IndexOf("\"temp\":") + 7, 8);
                while (result.IndexOf(",") != -1)
                    result = result.Substring(0, result.Length - 1);
                return result;
            }
            catch (Exception ef)
            {
                return null;
            }

        }
        public string getDescription(double lat, double longt)
        {
            try
            {
                string url = @"http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + longt + "&units=metric";
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                httpWebRequest.Method = WebRequestMethods.Http.Get;
                httpWebRequest.Accept = "text/json";
                httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)httpWebRequest.GetResponse();
                string result = "";
                using (System.IO.Stream stream = response.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                response.Close();
                result = result.Substring(result.IndexOf("\"description\":") + 15, 28);
                while (result.IndexOf("\",") != -1)
                    result = result.Substring(0, result.Length - 1);
                result = result.Substring(0, result.Length - 1);

                return char.ToUpper(result[0]) + result.Substring(1);
            }
            catch (Exception ef)
            {
                return null;
            }

        }
        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                showMap();
            }
            catch (Exception ef) { }
        }
    }

    public class InternetChecker
    {
        [System.Runtime.InteropServices.DllImport("wininet.dll")]
        private extern static bool InternetGetConnectedState(out int Description, int ReservedValue);

        public bool IsConnectedToInternet()
        {
            try
            {
                int Desc;
                return InternetGetConnectedState(out Desc, 0);
            }
            catch (Exception ef) { return true; }
        }
    }



}
