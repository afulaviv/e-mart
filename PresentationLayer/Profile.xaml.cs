﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Backend;
using Buisness_Layer;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for Profile.xaml
    /// </summary>
    public partial class Profile : Window, ScreenInterface
    {
        private ScreenInterface nextScreen;
        private Buisness_Interface bl;

        public Profile(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;
            if (GlobalVariable.employee != null)
            {
                employeeGrid.Visibility = Visibility.Visible;
                eHeadline.Content = "Welcome " + GlobalVariable.employee.FirstName + " " + GlobalVariable.employee.LastName;
                eId.Content = GlobalVariable.employee.TehudatZehut;
                eDepId.Content = GlobalVariable.employee.DepartmentID;
                eSal.Content = GlobalVariable.employee.Salary;
                eGen.Content = GlobalVariable.employee.Gender;
                eSupId.Content = GlobalVariable.employee.SuperVisorID;
                inChargeOf.ItemsSource = bl.EmployeeGet(new Employee(-1, "", "", User.PermissionType.Manager, -1, "", "", -1, -1, Employee.GenderTypes.Empty, GlobalVariable.employee.TehudatZehut), -1, -1);
            }

            if (GlobalVariable.customer != null)
            {
                customerGrid.Visibility = Visibility.Visible;
                cHeadline.Content = "Welcome " + GlobalVariable.customer.FirstName + " " + GlobalVariable.customer.LastName;

                List<Transaction> transaction = new List<Transaction>();
                for (int i = 0; i < GlobalVariable.customer.TransactionHistory.Count; i++)
                {
                    transaction.Add(bl.TransactionGetByID(GlobalVariable.customer.TransactionHistory[i]));
                }
                cTransactionHistory.ItemsSource = transaction;

                cPaymentType.Content = GlobalVariable.customer.Payment.PaymentMethodType.ToString();

            }
            if (GlobalVariable.clubMember != null)
            {
                clubmemberGrid.Visibility = Visibility.Visible;
                cmHeadline.Content = "Welcome " + GlobalVariable.clubMember.FirstName + " " + GlobalVariable.clubMember.LastName;
                
                List<Transaction> transaction = new List<Transaction>();
                for (int i = 0; i < GlobalVariable.clubMember.TransactionHistory.Count; i++)
                {
                    transaction.Add(bl.TransactionGetByID(GlobalVariable.clubMember.TransactionHistory[i]));
                }
                cmTransactionHistory.ItemsSource = transaction;

                cmPaymentType.Content = GlobalVariable.clubMember.Payment.ToString();
                cmId.Content = GlobalVariable.clubMember.TehudatZehut;
                cmGender.Content = GlobalVariable.clubMember.Gender;
                cmDOB.Content = GlobalVariable.clubMember.DateOfBirth.ToString();
            }
        }
        public void Run()
        {
            this.Show();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Hide();
            nextScreen = new MainScreen(bl);
            nextScreen.Run();
        }
    }
}
