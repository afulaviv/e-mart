﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using Buisness_Layer;
using System.Collections;
using Backend;
using System.ComponentModel;

namespace PresentationLayer
{
    /// <summary>
    /// Interaction logic for MainScreen.xaml
    /// The main screen of E mart
    /// </summary>
    public partial class MainScreen : Window, ScreenInterface
    {
        private Buisness_Interface bl;
        private Button[] b;

        public MainScreen(Buisness_Interface bl)
        {
            InitializeComponent();
            this.bl = bl;

            //Aviv tetapel ---> null Exception

            List<int> productsIDList = bl.getBestSeller();
            if (productsIDList.Count >= 4)
                productsIDList.RemoveRange(4, productsIDList.Count);
            foreach (int id in productsIDList)
                bestSellersStackPanel.Children.Add(new BestSellerUC(bl.ProductGetByID(id)));
            List<User.functionsType> functions = null;
            if (GlobalVariable.customer != null)
                functions = GlobalVariable.customer.getFunctions();
            if (GlobalVariable.employee != null)
                functions = GlobalVariable.employee.getFunctions();
            if (GlobalVariable.clubMember != null)
                functions = GlobalVariable.clubMember.getFunctions();
            addButtonsByPermission(functions);
        }

        private void addButtonsByPermission(List<User.functionsType> functions)
        {
            b = new Button[functions.Count];
            for (int i = 0; i < functions.Count; i++)
            {
                b[i] = new Button();
                b[i].Content = functions[i];
                b[i].Click += new RoutedEventHandler(nextScreen_Click);
                statusBarPanel.Children.Add(b[i]);
            }
        }

        private void nextScreen_Click(object sender, RoutedEventArgs e)
        {
            switch (Convert.ToString(((Button)sender).Content))
            {
                case ("Add_or_Edit_Product"):
                    var nextScreen = new ProductScreen(bl);
                    this.Hide();
                    nextScreen.Closed += (s, args) => this.Close();
                    nextScreen.Show();
                    break;
                case ("Remove_or_Edit_Employee"):
                    var nextScreen1 = new EmployeeScreen(bl);
                    this.Hide();
                    nextScreen1.Closed += (s, args) => this.Close();
                    nextScreen1.Show();
                    break;
                case ("Add_or_Edit_Department"):
                    var nextScreen2 = new DepartmentScreen(bl);
                    this.Hide();
                    nextScreen2.Closed += (s, args) => this.Close();
                    nextScreen2.Show();
                    break;
                case ("Add_Transaction"):
                    var nextScreen3 = new TransactionScreen(bl);
                    this.Hide();
                    nextScreen3.Closed += (s, args) => this.Close();
                    nextScreen3.Show();
                    break;
                case ("Edit_User"):
                    var nextScreen9 = new Profile(bl);
                    this.Hide();
                    nextScreen9.Closed += (s, args) => this.Close();
                    nextScreen9.Show();
                    break;
                case ("See_Profile"):
                    var nextScreen4 = new Profile(bl);
                    this.Hide();
                    nextScreen4.Closed += (s, args) => this.Close();
                    nextScreen4.Show();
                    break;
                case ("Cart"):
                    if (GlobalVariable.customer != null)
                    {
                        var nextScreen5 = new CartScreen(bl);
                        this.Hide();
                        nextScreen5.Closed += (s, args) => this.Close();
                        nextScreen5.Show();
                    }
                    break;
                case ("Query"):
                        var nextScreen6 = new QueryScreen(bl);
                        this.Hide();
                        nextScreen6.Closed += (s, args) => this.Close();
                        nextScreen6.Show();
                    break;
                case ("Map_of_Stores"):
                    var nextScreen7 = new Maps(bl);
                    this.Hide();
                    nextScreen7.Closed += (s, args) => this.Close();
                    nextScreen7.Show();
                    break;
                case ("Add_User"):
                    var nextScreen8 = new SignInScreen(bl);
                    this.Hide();
                    nextScreen8.Closed += (s, args) => this.Close();
                    nextScreen8.Show();
                    break;
                case ("Edit_Customer"):
                    var nextScreen10 = new SignInScreen(bl);
                    this.Hide();
                    nextScreen10.Closed += (s, args) => this.Close();
                    nextScreen10.Show();
                    break;
                case ("Edit_Club_Member"):
                    var nextScreen11 = new SignInScreen(bl);
                    this.Hide();
                    nextScreen11.Closed += (s, args) => this.Close();
                    nextScreen11.Show();
                    break;
            }
        }

        public void Run()
        {
            this.Show();
            string welcomeUser = "";
            if (GlobalVariable.customer != null) { welcomeUser = GlobalVariable.customer.FirstName; }
            if (GlobalVariable.employee != null) { welcomeUser = GlobalVariable.employee.FirstName; }
            if (GlobalVariable.clubMember != null) { welcomeUser = GlobalVariable.clubMember.FirstName; }
            welcome.Content = "Welcome " + welcomeUser;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.DoWork += worker_DoWork;
            worker.ProgressChanged += worker_ProgressChanged;

            worker.RunWorkerAsync();
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            for (int i = 0; i < 101; i++)
            {
                (sender as BackgroundWorker).ReportProgress(i);
                Thread.Sleep(20);
            }
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            switch (Convert.ToString(prgBar.Value))
            {
                case ("50"): searchGrid.Visibility = Visibility.Visible;
                    break;
                case ("80"): buttonGrid.Visibility = Visibility.Visible;
                    break;
                case ("99"): prgBar.Visibility = Visibility.Collapsed; prgText.Visibility = Visibility.Collapsed; this.Height = this.Height - 18;
                    break;
            }
            prgBar.Value = e.ProgressPercentage;
        }

        private void TextBox_MouseEnter(object sender, MouseEventArgs e)
        {
            if (search.Text == "Search something...")
                search.Text = "";
        }

        private void search_MouseLeave(object sender, MouseEventArgs e)
        {
            if (search.Text == "")
                search.Text = "Search something...";
        }

        private void search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                List<Product> productsListQuery = bl.GetQueriesProducts(search.Text.Split(' '));
                var nextScreen = new SearchScreen(productsListQuery, bl);
                this.Hide();
                nextScreen.Closed += (s, args) => this.Close();
                nextScreen.Show();
            }
        }


        private void statusBar_MouseEnter(object sender, MouseEventArgs e)
        {
            statusBarPanel.Visibility = Visibility.Visible;
        }

        private void statusBar_MouseLeave(object sender, MouseEventArgs e)
        {
            statusBarPanel.Visibility = Visibility.Collapsed;
        }

        private void productButton_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new ProductScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);

            Application.Current.Shutdown();
        }

        private void queriesButton_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new QueryScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        private void addUser_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new SignInScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        private void addEmployee_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new EmployeeScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        private void transaction_Click(object sender, RoutedEventArgs e)
        {
            var nextScreen = new TransactionScreen(bl);
            this.Hide();
            nextScreen.Closed += (s, args) => this.Close();
            nextScreen.Show();
        }

        //private void cart_Click_1(object sender, RoutedEventArgs e)
        //{
        //    // Gili tetapel
        //    // ||
        //    // \/  Ma leon asa po bichlal?!?!?!?!
        //    var nextScreen = new CartScreen(new Customer(-1, "", "", User.PermissionType.Worker, "", "", new List<int>(), new Customer.PaymentMethod(Customer.paymentMethodTypes.visa, "1234"), new Hashtable()) ,bl);
        //    this.Hide();
        //    nextScreen.Closed += (s, args) => this.Close();
        //    nextScreen.Show();
        //}

        private void Button_ChangePass(object sender, RoutedEventArgs e)
        {

        }

        private void Button_DiscardPass(object sender, RoutedEventArgs e)
        {

        }

        private void prgBar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }
    }
}
