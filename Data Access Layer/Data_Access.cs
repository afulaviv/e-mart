﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization.Formatters.Binary;
using System.Timers;
using Backend;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Security.Cryptography;

namespace Data_Access_Layer
{
    /// <summary>
    /// This class handles stored data on the the memory.
    /// </summary>
    public class Data_Access : Data_Access_Interface
    {
        private SqlConnection myConnection = new SqlConnection("server=AVIV-PC;Trusted_Connection=yes;" +
                                                       "database=db;connection timeout =30");
        private SqlCommand sqlCommand;
        private SqlDataReader sqlReader;
        /// <summary>
        /// Constructor.
        /// </summary>
        public Data_Access()
        {
            try
            {
                myConnection.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        #region global lists functions

        public void Add<T>(T t)
        {
            try
            {
                if (typeof(T) == typeof(Employee))
                {
                    Employee e = (Employee)(object)t;

                    sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE Username = '" + e.Username + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();

                    if (e.DepartmentID != -1)
                    {
                        sqlCommand = new SqlCommand("select ID FROM [dbo].DepartmentTbl WHERE ID = '" + e.DepartmentID + "'", myConnection);
                        sqlReader = sqlCommand.ExecuteReader();
                        if (!sqlReader.Read())
                        {
                            sqlReader.Close();
                            return;
                        }
                        else
                            sqlReader.Close();
                    }

                    if (e.SuperVisorID != -1)
                    {
                        sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE ID = '" + e.SuperVisorID + "'", myConnection);
                        sqlReader = sqlCommand.ExecuteReader();
                        if (!sqlReader.Read())
                        {
                            sqlReader.Close();
                            return;
                        }
                        else
                            sqlReader.Close();
                    }

                    sqlCommand = new SqlCommand("select ID FROM [dbo].EmployeeTbl WHERE TehudatZehut = '" + e.TehudatZehut + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();

                    sqlCommand = new SqlCommand("insert into [UserTbl](UserName, Password, Type) values ('" + e.Username + "' , HASHBYTES('SHA1', '" + e.Password + "'), '" + e.Type.ToString() + "'); insert into [EmployeeTbl] (ID, FirstName, LastName," + (e.DepartmentID != -1 ? "DepartmentID, " : "") + "Salary, Gender, TehudatZehut" + (e.SuperVisorID != -1 ? ", SuperVisorID" : "") + ") values ((select ID from [UserTbl] where UserName = '" + e.Username + "'), '" + e.FirstName + "', '" + e.LastName + "', " + (e.DepartmentID != -1 ? e.DepartmentID.ToString() + ", " : "") + e.Salary + ", '" + e.Gender + "', " + e.TehudatZehut + (e.SuperVisorID != -1 ? ", " + e.SuperVisorID : "") + ");", myConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                else if (typeof(T) == typeof(ClubMember))
                {
                    ClubMember c = (ClubMember)(object)t;

                    sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE Username = '" + c.Username + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();

                    sqlCommand = new SqlCommand("select ID FROM [dbo].ClubMemberTbl WHERE TehudatZehut= '" + c.TehudatZehut + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();

                    sqlCommand = new SqlCommand("insert into [UserTbl](UserName, Password, Type) values ('" + c.Username + "' , HASHBYTES('SHA1', '" + c.Password + "'), '" + c.Type.ToString() + "'); insert into [CustomerTbl] (ID, FirstName, LastName, PaymentType, PaymentAdditionalInfo) values ((select ID from [UserTbl] where UserName = '" + c.Username + "'), '" + c.FirstName + "', '" + c.LastName + "', '" + c.Payment.PaymentMethodType.ToString() + "', '" + c.Payment.AdditionInfo + "'); insert into [ClubMemberTbl] (ID, TehudatZehut, Gender, DateOfBirth) values ((select ID from [UserTbl] where UserName = '" + c.Username + "'), " + c.TehudatZehut + ", '" + c.Gender.ToString() + "', '" + c.DateOfBirth.ToString("yyyy-MM-dd HH:mm") + "');", myConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                else if (typeof(T) == typeof(Transaction))
                {
                    Transaction tr = (Transaction)(object)t;

                    sqlCommand = new SqlCommand("select TransactionID FROM [dbo].TransactionTbl WHERE TransactionID = '" + tr.TransactionID + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();

                    sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE ID = '" + tr.CustomerID + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (!sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();

                    sqlCommand = new SqlCommand("insert into [TransactionTbl](CustomerID, Date, IsAReturn, PaymentType) values (" + tr.CustomerID + ", '" + tr.Date.ToString("yyyy-MM-dd HH:mm") + "', " + (tr.IsAReturn.Equals(Transaction.ReturnTypes.Purchase) ? 0 : 1) + ", '" + tr.Payment.PaymentMethodType + "');", myConnection);
                    sqlCommand.ExecuteNonQuery();

                    foreach (Transaction.ProductInReceipt row in tr.Receipt)
                    {
                        sqlCommand = new SqlCommand("insert into [RecieptTbl](TransactionID, ProductID, Quantity, ProductName, Price) values (SCOPE_IDENTITY(), " + row.InventoryID + ", " + row.Quantity + ", '" + row.ProductName + "', " + row.Price + ");", myConnection);
                        sqlCommand.ExecuteNonQuery();
                    }
                }
                else if (typeof(T) == typeof(Customer))
                {
                    Customer c = (Customer)(object)t;

                    sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE Username = '" + c.Username + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();

                    sqlCommand = new SqlCommand("insert into [UserTbl](UserName, Password, Type) values ('" + c.Username + "' , HASHBYTES('SHA1', '" + c.Password + "'), '" + c.Type.ToString() + "'); insert into [CustomerTbl] (ID, FirstName, LastName, PaymentType, PaymentAdditionalInfo) values ((select ID from [UserTbl] where UserName = '" + c.Username + "'), '" + c.FirstName + "', '" + c.LastName + "', '" + c.Payment.PaymentMethodType.ToString() + "', '" + c.Payment.AdditionInfo + "');", myConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                else if (typeof(T) == typeof(Product))
                {
                    Product p = (Product)(object)t;

                    if (p.Location != -1)
                    {
                        sqlCommand = new SqlCommand("select ID FROM [dbo].DepartmentTbl WHERE ID = '" + p.Location + "'", myConnection);
                        sqlReader = sqlCommand.ExecuteReader();
                        if (!sqlReader.Read())
                        {
                            sqlReader.Close();
                            return;
                        }
                        else
                            sqlReader.Close();
                    }

                    sqlCommand = new SqlCommand("insert into [ProductTbl] (Name, Type, " + (p.Location != -1 ? "Location, " : "") + "StockCount, Price, ImageUrl) values ('" + p.Name + "', '" + p.Type.ToString() + "', " + (p.Location != -1 ? p.Location.ToString() + ", " : "") + p.StockCount + ", " + p.Price + ", '" + p.ImageURL + "');", myConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                else if (typeof(T) == typeof(Department))
                {
                    Department d = (Department)(object)t;

                    sqlCommand = new SqlCommand("select ID FROM [dbo].DepartmentTbl WHERE Name= '" + d.Name + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();

                    sqlCommand = new SqlCommand("insert into [DepartmentTbl] (Name) values ('" + d.Name + "');", myConnection);
                    sqlCommand.ExecuteNonQuery();
                }
            }
            catch(Exception e)
            {
                return;
            }
        }

        public void Edit<T>(T t)
        {
            try
            {
            if (typeof(T) == typeof(Employee))
            {
                Employee e = (Employee)(object)t;
                
                if (e.DepartmentID != -1)
                {
                    sqlCommand = new SqlCommand("select ID FROM [dbo].DepartmentTbl WHERE ID = '" + e.DepartmentID + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (!sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();
                }

                if (e.SuperVisorID != -1)
                {
                    sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE ID = '" + e.SuperVisorID + "'", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (!sqlReader.Read())
                    {
                        sqlReader.Close();
                        return;
                    }
                    else
                        sqlReader.Close();
                }

                sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE Username = '" + e.Username + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("select ID FROM [dbo].EmployeeTbl WHERE TehudatZehut= '" + e.TehudatZehut + "' and ID != (select ID from [UserTbl] where UserName = '" + e.Username + "');", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("update [UserTbl] set Password = HASHBYTES('SHA1', '" + e.Password + "'), Type = '" + e.Type.ToString() + "' where UserName = '" + e.Username + "'; update [EmployeeTbl] set TehudatZehut = " + e.TehudatZehut + ", FirstName = '" + e.FirstName + "', LastName = '" + e.LastName + "', " + (e.DepartmentID != -1 ? "DepartmentID = " + e.DepartmentID + ", " : "") + "Salary = " + e.Salary + " Gender = '" + e.Gender.ToString() + (e.SuperVisorID != -1 ? "', SuperVisorID = " + e.SuperVisorID : "") + " where ID = (select ID from [UserTbl] where UserName = '" + e.Username + "');", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(ClubMember))
            {
                ClubMember c = (ClubMember)(object)t;

                sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE Username = '" + c.Username + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();
                
                sqlCommand = new SqlCommand("select ID FROM [dbo].ClubMemberTbl WHERE TehudatZehut= '" + c.TehudatZehut + "' and ID != (select ID from [UserTbl] where UserName = '" + c.Username + "');", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("update [UserTbl] set Password = HASHBYTES('SHA1', '" + c.Password + "'), Type = '" + c.Type.ToString() + "' where UserName = '" + c.Username + "'; update [CustomerTbl] set FirstName = '" + c.FirstName + "', LastName = '" + c.LastName + "', PaymentType = '" + c.Payment.PaymentMethodType.ToString() + "', PaymentAdditionalInfo = '" + c.Payment.AdditionInfo + "' where ID = (select ID from [UserTbl] where UserName = '" + c.Username + "'); update [ClubMemberTbl] set TehudatZehut = " + c.TehudatZehut + ", Gender = '" + c.Gender + "', DateOfBirth = '" + c.DateOfBirth.Date.ToString("yyyy-MM-dd HH:mm") + "' where ID = (select ID from [UserTbl] where UserName = '" + c.Username + "');", myConnection);
                sqlCommand.ExecuteNonQuery();

                string cartQueryUpdate = "delete from [CartTbl] where CustomerID = " + c.Id + "; insert into [CartTbl] (CustomerID, ProductID, Quantity) values ";
                // update cart
                for(int i = 0; i < c.Cart.Count; i++)
                {
                    cartQueryUpdate += "(" + c.Id + ", " + c.Cart[i].ProductID + ", " + c.Cart[i].Quantity + ")";
                    if(i != c.Cart.Count - 1)
                        cartQueryUpdate += ", ";
                }
                sqlCommand = new SqlCommand(cartQueryUpdate, myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(Transaction))
            {
                Transaction tr = (Transaction)(object)t;

                sqlCommand = new SqlCommand("select TransactionID FROM [dbo].TransactionTbl WHERE TransactionID = '" + tr.TransactionID + "' and CustomerID = '" + tr.CustomerID + "';", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("update [TransactionTbl] set Date = " + tr.Date.Date.ToString("yyyy-MM-dd HH:mm") + ", IsAReturn = " + (tr.IsAReturn.Equals(Transaction.ReturnTypes.Purchase) ? 0 : 1) + ", PaymentType = '" + tr.Payment.PaymentMethodType + "' where TransactionID = " + tr.TransactionID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                foreach(Transaction.ProductInReceipt row in tr.Receipt)
                {
                    sqlCommand = new SqlCommand("select TransactionID FROM [dbo].RecieptTbl WHERE TransactionID = '" + tr.TransactionID + "' and CustomerID = '" + tr.CustomerID + "';", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    if (sqlReader.Read())
                    {
                        sqlCommand = new SqlCommand("update [RecieptTbl] set Quantity = " + row.Quantity + ", ProductName = '" + row.ProductName + "', Price = " + row.Price + " where TransactionID = " + tr.TransactionID + " and ProductID = " + row.InventoryID + ";", myConnection);
                        sqlCommand.ExecuteNonQuery();
                    }
                    else
                    {
                        sqlCommand = new SqlCommand("insert into [RecieptTbl](TransactionID, ProductID, Quantity, ProductName, Price) values (" + tr.TransactionID + ", " + row.InventoryID + ", " + row.Quantity + ", '" + row.ProductName + "', " + row.Price + ");", myConnection);
                        sqlCommand.ExecuteNonQuery();
                    }
                    sqlReader.Close();
                }
            }
            else if (typeof(T) == typeof(Customer))
            {
                Customer c = (Customer)(object)t;

                sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE Username = '" + c.Username + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("update [UserTbl] set Password = HASHBYTES('SHA1', '" + c.Password + "'), Type = '" + c.Type.ToString() + "' where UserName = '" + c.Username + "'; update [CustomerTbl] set FirstName = '" + c.FirstName + "', LastName = '" + c.LastName + "', PaymentType = '" + c.Payment.PaymentMethodType.ToString() + "', PaymentAdditionalInfo = '" + c.Payment.AdditionInfo + "' where ID = (select ID from [UserTbl] where UserName = '" + c.Username + "');", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(Product))
            {
                Product p = (Product)(object)t;

                sqlCommand = new SqlCommand("select ID FROM [dbo].ProductTbl WHERE ID = '" + p.InventoryID + "';", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("update [ProductTbl] set Name = '" + p.Name + "', Type = '" + p.Type.ToString() + "', " + (p.Location != -1 ? "Location = " + p.Location + ", " : "") + " StockCount = " + p.StockCount + ", Price = " + p.Price + ", ImageUrl = '" + p.ImageURL + "' where ID = " + p.InventoryID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(Department))
            {
                Department d = (Department)(object)t;

                sqlCommand = new SqlCommand("select ID FROM [dbo].DepartmentTbl WHERE Name= '" + d.Name + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("update [DepartmentTbl] set Name = '" + d.Name + "' where ID = " + d.ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            }
            catch (Exception e)
            {
                return;
            }
        }

        public void Remove<T>(int ID)
        {
            try {
            if (typeof(T) == typeof(Employee))
            {
                sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE ID = '" + ID + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("select ID FROM [dbo].EmployeeTbl WHERE ID = '" + ID + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                // removes the employee.
                sqlCommand = new SqlCommand("update [EmployeeTbl] set SuperVisorID = NULL where SuperVisorID = " + ID + "; delete from [EmployeeTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
                
                // removes user
                sqlCommand = new SqlCommand("delete from [UserTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(ClubMember))
            {
                sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE ID = '" + ID + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("select ID FROM [dbo].ClubMemberTbl WHERE ID = '" + ID + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("select ID FROM [dbo].CustomerTbl WHERE ID = '" + ID + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                // removes Cart
                sqlCommand = new SqlCommand("delete from [CartTbl] where CustomerID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes Reciepts
                sqlCommand = new SqlCommand("select TransactionID from [TransactionTbl] where CustomerID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while(sqlReader.Read())
                {
                    sqlCommand = new SqlCommand("delete from [RecieptTbl] where TransactionID = " + sqlReader[0] + ";", myConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                sqlReader.Close();

                // removes Transactions
                sqlCommand = new SqlCommand("delete from [TransactionTbl] where CustomerID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes ClubMember
                sqlCommand = new SqlCommand("delete from [ClubMemberTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes Customer
                sqlCommand = new SqlCommand("delete from [CustomerTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes User
                sqlCommand = new SqlCommand("delete from [UserTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(Transaction))
            {
                // removes Reciepts
                sqlCommand = new SqlCommand("", myConnection);
                sqlCommand.ExecuteNonQuery();
                sqlCommand = new SqlCommand("delete from [RecieptTbl] where TransactionID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes Transactions
                sqlCommand = new SqlCommand("delete from [TransactionTbl] where TransactionID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(Customer))
            {
                sqlCommand = new SqlCommand("select ID FROM [dbo].UserTbl WHERE ID = '" + ID + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                sqlCommand = new SqlCommand("select ID FROM [dbo].CustomerTbl WHERE ID = '" + ID + "'", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (!sqlReader.Read())
                {
                    sqlReader.Close();
                    return;
                }
                else
                    sqlReader.Close();

                // removes Cart
                sqlCommand = new SqlCommand("delete from [CartTbl] where CustomerID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes Reciepts
                sqlCommand = new SqlCommand("select TransactionID from [TransactionTbl] where CustomerID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    sqlCommand = new SqlCommand("delete from [RecieptTbl] where TransactionID = " + sqlReader[0] + ";", myConnection);
                    sqlCommand.ExecuteNonQuery();
                }
                sqlReader.Close();

                // removes Transactions
                sqlCommand = new SqlCommand("delete from [TransactionTbl] where CustomerID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes ClubMember
                sqlCommand = new SqlCommand("delete from [ClubMemberTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes Customer
                sqlCommand = new SqlCommand("delete from [CustomerTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes User
                sqlCommand = new SqlCommand("delete from [UserTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(Product))
            {
                // removes Cart pritim
                sqlCommand = new SqlCommand("delete from [CartTbl] where ProductID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes Product
                sqlCommand = new SqlCommand("delete from [ProductTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
            }
            else if (typeof(T) == typeof(Department))
            {
                // puts null on all productTbl who belong this department
                sqlCommand = new SqlCommand("update [ProductTbl] set Location = NULL where Location = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // puts null on all EmployeeTbl who belong this department
                sqlCommand = new SqlCommand("update [EmployeeTbl] set DepartmentID = NULL where DepartmentID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();

                // removes Department
                sqlCommand = new SqlCommand("delete from [DepartmentTbl] where ID = " + ID + ";", myConnection);
                sqlCommand.ExecuteNonQuery();
            }

            }
            catch (Exception e)
            {
                return;
            }
        }

        public T Get<T>(int ID)
        {
            try { 
            if (typeof(T) == typeof(Employee))
            {
                sqlCommand = new SqlCommand("select u.ID, u.UserName, u.Password, u.Type, " +
                    "e.TehudatZehut, e.FirstName, e.LastName, e.DepartmentID, " +
                    "e.Salary, e.Gender, e.SuperVisorID from [UserTbl] u join [EmployeeTbl] e " +
                    "on u.ID = e.ID where u.ID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (sqlReader.Read())
                {
                    T e = (T)(object)new Employee((int)sqlReader[0], sqlReader[1].ToString(), sqlReader[2].ToString(), sqlReader[3].ToString().Equals("Worker") ? User.PermissionType.Worker : (sqlReader[3].ToString().Equals("Manager") ? User.PermissionType.Manager : (sqlReader[3].ToString().Equals("Customer") ? User.PermissionType.Customer : (sqlReader[3].ToString().Equals("Admin") ? User.PermissionType.Admin : (User.PermissionType.Customer)))), (int)sqlReader[4], sqlReader[5].ToString(), sqlReader[6].ToString(), sqlReader.IsDBNull(7) ? -1 : (int)sqlReader[7], (int)sqlReader[8], sqlReader[9].ToString().Equals("female") ? Employee.GenderTypes.female : Employee.GenderTypes.male, sqlReader.IsDBNull(10) ? -1 : (int)sqlReader[10]);
                    sqlReader.Close();
                    return e;
                }
                else
                {
                    sqlReader.Close();
                    return default(T);
                }
            }
            else if (typeof(T) == typeof(ClubMember))
            {
                sqlCommand = new SqlCommand("select TransactionID FROM TransactionTbl where [TransactionTbl].CustomerID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                List<int> transactionHistory = new List<int>();
                while (sqlReader.Read())
                    transactionHistory.Add((int) sqlReader[0]);
                sqlReader.Close();

                sqlCommand = new SqlCommand("select [CartTbl].ProductID, [CartTbl].Quantity FROM CartTbl where [CartTbl].CustomerID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                List<Customer.ProductIdQuantity> cart = new List<Customer.ProductIdQuantity>();
                while (sqlReader.Read())
                    cart.Add(new Customer.ProductIdQuantity((int) sqlReader[0], (int) sqlReader[1]));
                sqlReader.Close();

                sqlCommand = new SqlCommand("select u.ID, u.UserName, u.Password, u.Type, cl.TehudatZehut, cu.FirstName, cu.LastName, cl.Gender, cl.DateOfBirth, cu.PaymentType, cu.PaymentAdditionalInfo from [UserTbl] u join [ClubMemberTbl] cl on u.ID = cl.ID join [CustomerTbl] cu on cu.ID = u.ID  where u.ID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.Read())
                {
                    T e = (T)(object)new ClubMember((int)sqlReader[0], sqlReader[1].ToString(), sqlReader[2].ToString(), sqlReader[3].ToString().Equals("Worker") ? User.PermissionType.Worker : (sqlReader[3].ToString().Equals("Manager") ? User.PermissionType.Manager : (sqlReader[3].ToString().Equals("Customer") ? User.PermissionType.Customer : (sqlReader[3].ToString().Equals("Admin") ? User.PermissionType.Admin : (User.PermissionType.Customer)))), 
                        (int)sqlReader[4], sqlReader[5].ToString(), sqlReader[6].ToString(),
                        transactionHistory, sqlReader[7].ToString().Equals("female") ? ClubMember.GenderTypes.female : ClubMember.GenderTypes.male,
                        sqlReader.GetDateTime(8), new Customer.PaymentMethod(sqlReader[9].ToString().Equals("visa") ? Customer.paymentMethodTypes.visa : Customer.paymentMethodTypes.masterCard, sqlReader[10].ToString()), cart);
                    sqlReader.Close();
                    return e;
                }
                else
                {
                    sqlReader.Close();
                    return default(T);
                }
            }
            else if (typeof(T) == typeof(Transaction))
            {
                sqlCommand = new SqlCommand("select [RecieptTbl].ProductID, [RecieptTbl].ProductName, [RecieptTbl].Price FROM RecieptTbl where [RecieptTbl].TransactionID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                List<Transaction.ProductInReceipt> reciept = new List<Transaction.ProductInReceipt>();
                while (sqlReader.Read())
                    reciept.Add(new Transaction.ProductInReceipt((int) sqlReader[0], sqlReader[1].ToString(), (int) sqlReader[2], (int) sqlReader[3]));
                sqlReader.Close();

                sqlCommand = new SqlCommand("select [TransactionTbl].TransactionID, [TransactionTbl].CustomerID, [TransactionTbl].Date, [TransactionTbl].IsAReturn, [TransactionTbl].PaymentType from [TransactionTbl] where [TransactionTbl].TransactionID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.Read())
                {
                    T e = (T)(object)new Transaction((int)sqlReader[0], (int)sqlReader[1], sqlReader.GetDateTime(2), ((int)sqlReader[3]) == 0 ? Transaction.ReturnTypes.Purchase : Transaction.ReturnTypes.Returned, reciept, new Transaction.PaymentMethod(sqlReader[4].ToString().Equals("visa") ? Transaction.paymentMethodTypes.visa : Transaction.paymentMethodTypes.masterCard, sqlReader[5].ToString()));
                    sqlReader.Close();
                    return e;
                }
                else
                {
                    sqlReader.Close();
                    return default(T);
                }
            }
            else if (typeof(T) == typeof(Customer))
            {
                sqlCommand = new SqlCommand("select TransactionID FROM TransactionTbl where [TransactionTbl].CustomerID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                List<int> transactionHistory = new List<int>();
                while (sqlReader.Read())
                    transactionHistory.Add((int)sqlReader[0]);
                sqlReader.Close();

                sqlCommand = new SqlCommand("select [CartTbl].ProductID, [CartTbl].Quantity FROM CartTbl where [CartTbl].CustomerID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                List<Customer.ProductIdQuantity> cart = new List<Customer.ProductIdQuantity>();
                while (sqlReader.Read())
                    cart.Add(new Customer.ProductIdQuantity((int)sqlReader[0], (int)sqlReader[1]));
                sqlReader.Close();

                sqlCommand = new SqlCommand("select u.ID, u.UserName, u.Password, u.Type, cu.FirstName, cu.LastName, cu.PaymentType, cu.PaymentAdditionalInfo from [UserTbl] u join [CustomerTbl] cu on cu.ID = u.ID  where u.ID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.Read())
                {
                    T e = (T)(object)new Customer((int)sqlReader[0], sqlReader[1].ToString(), sqlReader[2].ToString(), sqlReader[3].ToString().Equals("Worker") ? User.PermissionType.Worker : (sqlReader[3].ToString().Equals("Manager") ? User.PermissionType.Manager : (sqlReader[3].ToString().Equals("Customer") ? User.PermissionType.Customer : (sqlReader[3].ToString().Equals("Admin") ? User.PermissionType.Admin : (User.PermissionType.Customer)))),
                        sqlReader[4].ToString(), sqlReader[5].ToString(), transactionHistory, new Customer.PaymentMethod(sqlReader[6].ToString().Equals("visa") ? Customer.paymentMethodTypes.visa : Customer.paymentMethodTypes.masterCard, sqlReader[7].ToString()), cart);
                    sqlReader.Close();
                    return e;
                }
                else
                {
                    sqlReader.Close();
                    return default(T);
                }
            }
            else if (typeof(T) == typeof(Product))
            {
                sqlCommand = new SqlCommand("select [ProductTbl].ID, [ProductTbl].Name, [ProductTbl].Type, [ProductTbl].Location, [ProductTbl].StockCount, [ProductTbl].Price, [ProductTbl].ImageUrl FROM ProductTbl where [ProductTbl].ID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                if (sqlReader.Read())
                {
                    Product.productTypes type;
                    switch (sqlReader[2].ToString())
	                {
		                case "Uncategroized":
                            type = Product.productTypes.Uncategroized;
                         break;
                        case "Antiques":
                            type = Product.productTypes.Antiques;
                         break;
                        case "Art":
                            type = Product.productTypes.Art;
                         break;
                        case "Baby":
                            type = Product.productTypes.Baby;
                         break;
                        case "Books":
                            type = Product.productTypes.Books;
                         break;
                        case "Cameras":
                            type = Product.productTypes.Cameras;
                         break;
                        case "Cell":
                            type = Product.productTypes.Cell;
                         break;
                        case "Clothing":
                            type = Product.productTypes.Clothing;
                         break;
                        case "Computers":
                            type = Product.productTypes.Computers;
                         break;
                        case "Movies":
                            type = Product.productTypes.Movies;
                         break;
                        case "Motors":
                            type = Product.productTypes.Motors;
                         break;
                        case "Home":
                            type = Product.productTypes.Home;
                         break;
                        case "Garden":
                            type = Product.productTypes.Garden;
                         break;
                        case "Jewelry":
                            type = Product.productTypes.Jewelry;
                         break;
                        case "Watches":
                            type = Product.productTypes.Watches;
                         break;
                        case "Music":
                            type = Product.productTypes.Music;
                         break;
                        case "Pet":
                            type = Product.productTypes.Pet;
                         break;
                        case "Sport":
                            type = Product.productTypes.Sport;
                         break;
                        case "Stamps":
                            type = Product.productTypes.Stamps;
                         break;
                        case "Toys":
                            type = Product.productTypes.Toys;
                         break;
                        case "Else":
                            type = Product.productTypes.Else;
                         break;
                        case "Empty":
                            type = Product.productTypes.Empty;
                         break;
                        default:
                            type = Product.productTypes.Empty;
                         break;
	                }
                    T e = (T)(object)new Product((int)sqlReader[0], sqlReader[1].ToString(), type, sqlReader.IsDBNull(3) ? -1 : (int)sqlReader[3], (int)sqlReader[4], (int)sqlReader[5], sqlReader[6].ToString());
                    sqlReader.Close();
                    return e;
                }
                else
                {
                    sqlReader.Close();
                    return default(T);
                }
            }
            else if (typeof(T) == typeof(Department))
            {
                sqlCommand = new SqlCommand("select [DepartmentTbl].ID, [DepartmentTbl].Name FROM DepartmentTbl where [DepartmentTbl].ID = " + ID + ";", myConnection);
                sqlReader = sqlCommand.ExecuteReader();

                if (sqlReader.Read())
                {
                    T e = (T)(object)new Department((int)sqlReader[0], sqlReader[1].ToString());
                    sqlReader.Close();
                    return e;
                }
                else
                {
                    sqlReader.Close();
                    return default(T);
                }
            }
            else return default(T);

            }
            catch (Exception e)
            {
                return default(T);
            }
        }

        public List<T> Get<T, S>(List<TypeValue<S>> typeValues)
        {
            try { 
            if (typeof(T) == typeof(Employee) && typeof(S) == typeof(EmployeeFields))
            {
                string sqlQuery = "select u.ID, u.UserName, u.Password, u.Type, " +
                    "e.TehudatZehut, e.FirstName, e.LastName, e.DepartmentID, " +
                    "e.Salary, e.Gender, e.SuperVisorID from [UserTbl] u join [EmployeeTbl] e " +
                    "on u.ID = e.ID" + (typeValues.Count == 0 ? ";" : " where ");
                bool firstTime = true;

                List<TypeValue<EmployeeFields>> employeeFieldsList = typeValues.Cast<TypeValue<EmployeeFields>>().ToList();
                List<Employee> toReturnList = new List<Employee>();

                foreach (TypeValue<EmployeeFields> typeValue in employeeFieldsList)
                {
                    switch (typeValue.Type)
                    {
                        case EmployeeFields.TehudatZehut:
                            sqlQuery += (firstTime ? "" : " and ") + "e.TehudatZehut = " + typeValue.Value;
                            break;
                        case EmployeeFields.firstName:
                            sqlQuery += (firstTime ? "" : " and ") + "e.FirstName = '" + typeValue.Value + "'";
                            break;
                        case EmployeeFields.lastName:
                            sqlQuery += (firstTime ? "" : " and ") + "e.LastName = '" + typeValue.Value + "'";
                            break;
                        case EmployeeFields.departmentID:
                            sqlQuery += (firstTime ? "" : " and ") + "e.DepartmentID = " + typeValue.Value;
                            break;
                        case EmployeeFields.salary:
                            sqlQuery += (firstTime ? "" : " and ") + "e.Salary = " + typeValue.Value;
                            break;
                        case EmployeeFields.gender:
                            sqlQuery += (firstTime ? "" : " and ") + "e.Gender = '" + typeValue.Value + "'";
                            break;
                        case EmployeeFields.supervisor:
                            sqlQuery += (firstTime ? "" : " and ") + "e.SuperVisorID = " + typeValue.Value;
                            break;
                    }
                    firstTime = false;
                }

                sqlCommand = new SqlCommand(sqlQuery, myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                    toReturnList.Add(new Employee((int)sqlReader[0], sqlReader[1].ToString(), sqlReader[2].ToString(), sqlReader[3].ToString().Equals("Worker") ? User.PermissionType.Worker : (sqlReader[3].ToString().Equals("Manager") ? User.PermissionType.Manager : (sqlReader[3].ToString().Equals("Customer") ? User.PermissionType.Customer : (sqlReader[3].ToString().Equals("Admin") ? User.PermissionType.Admin : (User.PermissionType.Customer)))), (int)sqlReader[4], sqlReader[5].ToString(), sqlReader[6].ToString(), sqlReader.IsDBNull(7) ? -1 : (int)sqlReader[7], (int)sqlReader[8], sqlReader[9].ToString().Equals("female") ? Employee.GenderTypes.female : Employee.GenderTypes.male, sqlReader.IsDBNull(10) ? -1 : (int)sqlReader[10]));

                sqlReader.Close();
                return toReturnList.Cast<T>().ToList();
            }
            else if (typeof(T) == typeof(ClubMember) && typeof(S) == typeof(ClubMemberFields))
            {
                string sqlQuery = "select u.ID, u.UserName, u.Password, u.Type, cl.TehudatZehut, cu.FirstName, cu.LastName, cl.Gender, cl.DateOfBirth, cu.PaymentType, cu.PaymentAdditionalInfo from [UserTbl] u join [ClubMemberTbl] cl on u.ID = cl.ID join [CustomerTbl] cu on cu.ID = u.ID" + (typeValues.Count == 0 ? ";" : " where ");
                bool firstTime = true;

                List<TypeValue<ClubMemberFields>> clubMemberFieldsList = typeValues.Cast<TypeValue<ClubMemberFields>>().ToList();
                List<ClubMember> toReturnList = new List<ClubMember>();

                foreach (TypeValue<ClubMemberFields> typeValue in clubMemberFieldsList)
                {
                    switch (typeValue.Type)
                    {
                        case ClubMemberFields.memberID:
                            sqlQuery += (firstTime ? "" : " and ") + "u.ID = " + typeValue.Value;
                            break;
                        case ClubMemberFields.teudatZehutID:
                            sqlQuery += (firstTime ? "" : " and ") + "cl.TehudatZehut = " + typeValue.Value + "'";
                            break;
                        case ClubMemberFields.firstName:
                            sqlQuery += (firstTime ? "" : " and ") + "cu.FirstName = '" + typeValue.Value + "'";
                            break;
                        case ClubMemberFields.lastName:
                            sqlQuery += (firstTime ? "" : " and ") + "cu.LastName = '" + typeValue.Value + "'";
                            break;
                        case ClubMemberFields.gender:
                            sqlQuery += (firstTime ? "" : " and ") + "cl.Gender = '" + typeValue.Value + "'";
                            break;
                        case ClubMemberFields.dateOfBirth:
                            sqlQuery += (firstTime ? "" : " and ") + "cl.DateOfBirth = '" + typeValue.Value + "'";
                            break;
                        case ClubMemberFields.payment:
                            sqlQuery += (firstTime ? "" : " and ") + "cu.PaymentType = '" + typeValue.Value + "'";
                            break;
                    }
                    firstTime = false;
                }

                sqlCommand = new SqlCommand(sqlQuery, myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    toReturnList.Add(new ClubMember((int)sqlReader[0], sqlReader[1].ToString(), sqlReader[2].ToString(), sqlReader[3].ToString().Equals("Worker") ? User.PermissionType.Worker : (sqlReader[3].ToString().Equals("Manager") ? User.PermissionType.Manager : (sqlReader[3].ToString().Equals("Customer") ? User.PermissionType.Customer : (sqlReader[3].ToString().Equals("Admin") ? User.PermissionType.Admin : (User.PermissionType.Customer)))), 
                        (int)sqlReader[4], sqlReader[5].ToString(), sqlReader[6].ToString(),
                        new List<int>(), sqlReader[7].ToString().Equals("female") ? ClubMember.GenderTypes.female : ClubMember.GenderTypes.male,
                        sqlReader.GetDateTime(2), new Customer.PaymentMethod(sqlReader[9].ToString().Equals("visa") ? Customer.paymentMethodTypes.visa : Customer.paymentMethodTypes.masterCard, sqlReader[10].ToString()), new List<Customer.ProductIdQuantity>()));
                }

                sqlReader.Close();

                foreach(ClubMember clubMember in toReturnList)
                {
                    sqlCommand = new SqlCommand("select TransactionID FROM TransactionTbl where [TransactionTbl].CustomerID = " + clubMember.Id + ";", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                        clubMember.TransactionHistory.Add((int)sqlReader[0]);
                    sqlReader.Close();

                    sqlCommand = new SqlCommand("select [CartTbl].ProductID, [CartTbl].Quantity FROM CartTbl where [CartTbl].CustomerID = " + clubMember.Id + ";", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                        clubMember.Cart.Add(new Customer.ProductIdQuantity((int)sqlReader[0], (int)sqlReader[1]));
                    sqlReader.Close();
                }
                return toReturnList.Cast<T>().ToList();
            }
            else if (typeof(T) == typeof(Customer) && typeof(S) == typeof(CustomerFields))
            {
                string sqlQuery = "select u.ID, u.UserName, u.Password, u.Type, cu.FirstName, cu.LastName, cu.PaymentType, cu.PaymentAdditionalInfo from [UserTbl] u join [CustomerTbl] cu on cu.ID = u.ID" + (typeValues.Count == 0 ? ";" : " where ");
                bool firstTime = true;

                List<TypeValue<CustomerFields>> clubMemberFieldsList = typeValues.Cast<TypeValue<CustomerFields>>().ToList();
                List<Customer> toReturnList = new List<Customer>();

                foreach (TypeValue<CustomerFields> typeValue in clubMemberFieldsList)
                {
                    switch (typeValue.Type)
                    {
                        case CustomerFields.memberID:
                            sqlQuery += (firstTime ? "" : " and ") + "u.ID = " + typeValue.Value;
                            break;
                        case CustomerFields.firstName:
                            sqlQuery += (firstTime ? "" : " and ") + "cu.FirstName = '" + typeValue.Value + "'";
                            break;
                        case CustomerFields.lastName:
                            sqlQuery += (firstTime ? "" : " and ") + "cu.LastName = '" + typeValue.Value + "'";
                            break;
                        case CustomerFields.payment:
                            sqlQuery += (firstTime ? "" : " and ") + "cu.PaymentType = '" + typeValue.Value + "'";
                            break;
                    }
                    firstTime = false;
                }

                sqlCommand = new SqlCommand(sqlQuery, myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    toReturnList.Add(new Customer((int)sqlReader[0], sqlReader[1].ToString(), sqlReader[2].ToString(), sqlReader[3].ToString().Equals("Worker") ? User.PermissionType.Worker : (sqlReader[3].ToString().Equals("Manager") ? User.PermissionType.Manager : (sqlReader[3].ToString().Equals("Customer") ? User.PermissionType.Customer : (sqlReader[3].ToString().Equals("Admin") ? User.PermissionType.Admin : (User.PermissionType.Customer)))),
                        sqlReader[4].ToString(), sqlReader[5].ToString(), new List<int>(), new Customer.PaymentMethod(sqlReader[6].ToString().Equals("visa") ? Customer.paymentMethodTypes.visa : Customer.paymentMethodTypes.masterCard, sqlReader[7].ToString()), new List<Customer.ProductIdQuantity>()));
                }

                sqlReader.Close();

                foreach (Customer customer in toReturnList)
                {
                    sqlCommand = new SqlCommand("select TransactionID FROM TransactionTbl where [TransactionTbl].CustomerID = " + customer.Id + ";", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                        customer.TransactionHistory.Add((int)sqlReader[0]);
                    sqlReader.Close();

                    sqlCommand = new SqlCommand("select [CartTbl].ProductID, [CartTbl].Quantity FROM CartTbl where [CartTbl].CustomerID = " + customer.Id + ";", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                        customer.Cart.Add(new Customer.ProductIdQuantity((int)sqlReader[0], (int)sqlReader[1]));
                    sqlReader.Close();
                }
                return toReturnList.Cast<T>().ToList();
            }
            else if (typeof(T) == typeof(Transaction) && typeof(S) == typeof(TransactionFields))
            {
                string sqlQuery = "select [TransactionTbl].TransactionID, [TransactionTbl].CustomerID, [TransactionTbl].Date, [TransactionTbl].IsAReturn, [TransactionTbl].PaymentType from [TransactionTbl]" + (typeValues.Count == 0 ? ";" : " where ");
                bool firstTime = true;

                List<TypeValue<TransactionFields>> transactionFieldsList = typeValues.Cast<TypeValue<TransactionFields>>().ToList();
                List<Transaction> toReturnList = new List<Transaction>();

                foreach (TypeValue<TransactionFields> typeValue in transactionFieldsList)
                {
                    switch (typeValue.Type)
                    {
                        case TransactionFields.transactionID:
                            sqlQuery += (firstTime ? "" : " and ") + "TransactionID = " + typeValue.Value;
                            break;
                        case TransactionFields.dateOfBirth:
                            sqlQuery += (firstTime ? "" : " and ") + "Date = " + typeValue.Value;
                            break;
                        case TransactionFields.isAReturn:
                            sqlQuery += (firstTime ? "" : " and ") + "IsAReturn = " + (typeValue.Value.ToString().Equals("Purchase") ? 0 : 1);
                            break;
                        case TransactionFields.PaymentMethod:
                            sqlQuery += (firstTime ? "" : " and ") + "PaymentType = '" + typeValue.Value + "'";
                            break;
                    }
                    firstTime = false;
                }

                sqlCommand = new SqlCommand(sqlQuery, myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    int a = (int)sqlReader[0];
                    int b = (int)sqlReader[1];
                    DateTime c = sqlReader.GetDateTime(2);
                    int t = ((int)sqlReader[3]);
                    Transaction.ReturnTypes d = ((int)sqlReader[3]) == 0 ? Transaction.ReturnTypes.Purchase : Transaction.ReturnTypes.Returned;
                    Transaction.PaymentMethod e = new Transaction.PaymentMethod(sqlReader[4].ToString().Equals("visa") ? Transaction.paymentMethodTypes.visa : Transaction.paymentMethodTypes.masterCard, "");
                    toReturnList.Add(new Transaction((int)sqlReader[0], (int)sqlReader[1], sqlReader.GetDateTime(2), ((int)sqlReader[3]) == 0 ? Transaction.ReturnTypes.Purchase : Transaction.ReturnTypes.Returned, new List<Transaction.ProductInReceipt>(), new Transaction.PaymentMethod(sqlReader[4].ToString().Equals("visa") ? Transaction.paymentMethodTypes.visa : Transaction.paymentMethodTypes.masterCard, "")));
                }
                    
                    
                sqlReader.Close();

                foreach(Transaction transaction in toReturnList)
                {
                    sqlCommand = new SqlCommand("select [RecieptTbl].ProductID, [RecieptTbl].ProductName, [RecieptTbl].Quantity, [RecieptTbl].Price FROM RecieptTbl where [RecieptTbl].TransactionID = " + transaction.TransactionID + ";", myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    List<Transaction.ProductInReceipt> reciept = new List<Transaction.ProductInReceipt>();
                    while (sqlReader.Read())
                        transaction.Receipt.Add(new Transaction.ProductInReceipt((int) sqlReader[0], sqlReader[1].ToString(), (int) sqlReader[2], (int) sqlReader[3]));
                    sqlReader.Close();
                }

                return toReturnList.Cast<T>().ToList();
            }
            else if (typeof(T) == typeof(Product) && typeof(S) == typeof(ProductFields))
            {
                string sqlQuery = "select [ProductTbl].ID, [ProductTbl].Name, [ProductTbl].Type, [ProductTbl].Location, [ProductTbl].StockCount, [ProductTbl].Price, [ProductTbl].ImageUrl FROM ProductTbl" + (typeValues.Count == 0 ? ";" : " where ");
                bool firstTime = true;

                List<TypeValue<ProductFields>> productFieldsList = typeValues.Cast<TypeValue<ProductFields>>().ToList();
                List<Product> toReturnList = new List<Product>();

                foreach (TypeValue<ProductFields> typeValue in productFieldsList)
                {
                    switch (typeValue.Type)
                    {
                        case ProductFields.inventoryID:
                            sqlQuery += (firstTime ? "" : " and ") + "ID = " + typeValue.Value;
                            break;
                        case ProductFields.name:
                            sqlQuery += (firstTime ? "" : " and ") + "Name = '" + typeValue.Value + "'";
                            break;
                        case ProductFields.type:
                            sqlQuery += (firstTime ? "" : " and ") + "Type = '" + typeValue.Value + "'";
                            break;
                        case ProductFields.location:
                            sqlQuery += (firstTime ? "" : " and ") + "Location = " + typeValue.Value;
                            break;
                        case ProductFields.inStock:
                            sqlQuery += (firstTime ? "" : " and ") + "StockCount > 0";
                            break;
                        case ProductFields.stockCount:
                            sqlQuery += (firstTime ? "" : " and ") + "StockCount = " + typeValue.Value;
                            break;
                        case ProductFields.price:
                            sqlQuery += (firstTime ? "" : " and ") + "Price = " + typeValue.Value;
                            break;
                    }
                    firstTime = false;
                }

                sqlCommand = new SqlCommand(sqlQuery, myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                {
                    Product.productTypes type;
                    switch (sqlReader[2].ToString())
	                {
		                case "Uncategroized":
                            type = Product.productTypes.Uncategroized;
                         break;
                        case "Antiques":
                            type = Product.productTypes.Antiques;
                         break;
                        case "Art":
                            type = Product.productTypes.Art;
                         break;
                        case "Baby":
                            type = Product.productTypes.Baby;
                         break;
                        case "Books":
                            type = Product.productTypes.Books;
                         break;
                        case "Cameras":
                            type = Product.productTypes.Cameras;
                         break;
                        case "Cell":
                            type = Product.productTypes.Cell;
                         break;
                        case "Clothing":
                            type = Product.productTypes.Clothing;
                         break;
                        case "Computers":
                            type = Product.productTypes.Computers;
                         break;
                        case "Movies":
                            type = Product.productTypes.Movies;
                         break;
                        case "Motors":
                            type = Product.productTypes.Motors;
                         break;
                        case "Home":
                            type = Product.productTypes.Home;
                         break;
                        case "Garden":
                            type = Product.productTypes.Garden;
                         break;
                        case "Jewelry":
                            type = Product.productTypes.Jewelry;
                         break;
                        case "Watches":
                            type = Product.productTypes.Watches;
                         break;
                        case "Music":
                            type = Product.productTypes.Music;
                         break;
                        case "Pet":
                            type = Product.productTypes.Pet;
                         break;
                        case "Sport":
                            type = Product.productTypes.Sport;
                         break;
                        case "Stamps":
                            type = Product.productTypes.Stamps;
                         break;
                        case "Toys":
                            type = Product.productTypes.Toys;
                         break;
                        case "Else":
                            type = Product.productTypes.Else;
                         break;
                        case "Empty":
                            type = Product.productTypes.Empty;
                         break;
                        default:
                            type = Product.productTypes.Empty;
                         break;
	                }
                    toReturnList.Add(new Product((int)sqlReader[0], sqlReader[1].ToString(), type, sqlReader.IsDBNull(3) ? -1 : (int)sqlReader[3], (int)sqlReader[4], (int)sqlReader[5], sqlReader[6].ToString()));
                }

                sqlReader.Close();
                return toReturnList.Cast<T>().ToList();
            }
            else if (typeof(T) == typeof(Department) && typeof(S) == typeof(DepartmentFields))
            {
                string sqlQuery = "select [DepartmentTbl].ID, [DepartmentTbl].Name FROM DepartmentTbl" + (typeValues.Count == 0 ? ";" : " where ");
                bool firstTime = true;

                List<TypeValue<DepartmentFields>> departmentFieldsList = typeValues.Cast<TypeValue<DepartmentFields>>().ToList();
                List<Department> toReturnList = new List<Department>();

                foreach (TypeValue<DepartmentFields> typeValue in departmentFieldsList)
                {
                    switch (typeValue.Type)
                    {
                        case DepartmentFields.id:
                            sqlQuery += (firstTime ? "" : " and ") + "ID = " + typeValue.Value;
                            break;
                        case DepartmentFields.name:
                            sqlQuery += (firstTime ? "" : " and ") + "Name = '" + typeValue.Value + "'";
                            break;
                    }
                    firstTime = false;
                }

                sqlCommand = new SqlCommand(sqlQuery, myConnection);
                sqlReader = sqlCommand.ExecuteReader();
                while (sqlReader.Read())
                    toReturnList.Add(new Department((int)sqlReader[0], sqlReader[1].ToString()));
                sqlReader.Close();

                return toReturnList.Cast<T>().ToList();
            }
            return new List<T>();
            }
            catch (Exception e)
            {
                return new List<T>();
            }
        }

        public List<T> GetRangeByDateTime<T>(int stockCountFrom, int stockCountTo, DateTime fromDateTime, DateTime toDateTime, int year, int month, int day, int priceFrom, int priceTo)
        {
            try
            {
                if (typeof(T) == typeof(ClubMember))
                {
                    string sqlQuery = "select u.ID, u.UserName, u.Password, u.Type, cl.TehudatZehut, cu.FirstName, cu.LastName, cl.Gender, cl.DateOfBirth, cu.PaymentType, cu.PaymentAdditionalInfo from [UserTbl] u join [ClubMemberTbl] cl on u.ID = cl.ID join [CustomerTbl] cu on cu.ID = u.ID" + ((fromDateTime.Equals(DateTime.MinValue) && toDateTime.Equals(DateTime.MinValue) && year == -1 && month == -1 && day == -1 && priceFrom == -1 && priceTo == -1) ? ";" : " where ");
                    bool firstTime = true;

                    List<ClubMember> toReturnList = new List<ClubMember>();

                    if (!fromDateTime.Equals(DateTime.MinValue))
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "cl.DateOfBirth >= " + fromDateTime;
                        firstTime = false;
                    }
                    if (!toDateTime.Equals(DateTime.MinValue))
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "cl.DateOfBirth <= " + toDateTime;
                        firstTime = false;
                    }
                    if (year != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "year(cl.DateOfBirth) == " + year;
                        firstTime = false;
                    }
                    if (month != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "month(cl.DateOfBirth) == " + month;
                        firstTime = false;
                    }
                    if (day != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "day(cl.DateOfBirth) == " + year;
                        firstTime = false;
                    }

                    sqlCommand = new SqlCommand(sqlQuery, myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                    {
                        toReturnList.Add(new ClubMember((int)sqlReader[0], sqlReader[1].ToString(), sqlReader[2].ToString(), sqlReader[3].ToString().Equals("Worker") ? User.PermissionType.Worker : (sqlReader[3].ToString().Equals("Manager") ? User.PermissionType.Manager : (sqlReader[3].ToString().Equals("Customer") ? User.PermissionType.Customer : (sqlReader[3].ToString().Equals("Admin") ? User.PermissionType.Admin : (User.PermissionType.Customer)))),
                            (int)sqlReader[4], sqlReader[5].ToString(), sqlReader[6].ToString(),
                            new List<int>(), sqlReader[7].ToString().Equals("female") ? ClubMember.GenderTypes.female : ClubMember.GenderTypes.male,
                            sqlReader.GetDateTime(8), new Customer.PaymentMethod(sqlReader[9].ToString().Equals("visa") ? Customer.paymentMethodTypes.visa : Customer.paymentMethodTypes.masterCard, sqlReader[10].ToString()), new List<Customer.ProductIdQuantity>()));
                    }

                    sqlReader.Close();

                    foreach (ClubMember clubMember in toReturnList)
                    {
                        sqlCommand = new SqlCommand("select TransactionID FROM TransactionTbl where [TransactionTbl].CustomerID = " + clubMember.Id + ";", myConnection);
                        sqlReader = sqlCommand.ExecuteReader();
                        while (sqlReader.Read())
                            clubMember.TransactionHistory.Add((int)sqlReader[0]);
                        sqlReader.Close();

                        sqlCommand = new SqlCommand("select [CartTbl].ProductID, [CartTbl].Quantity FROM CartTbl where [CartTbl].CustomerID = " + clubMember.Id + ";", myConnection);
                        sqlReader = sqlCommand.ExecuteReader();
                        while (sqlReader.Read())
                            clubMember.Cart.Add(new Customer.ProductIdQuantity((int)sqlReader[0], (int)sqlReader[1]));
                        sqlReader.Close();
                    }
                    return toReturnList.Cast<T>().ToList();
                }
                else if (typeof(T) == typeof(Transaction))
                {
                    string sqlQuery = "select [TransactionTbl].TransactionID, [TransactionTbl].CustomerID, [TransactionTbl].Date, [TransactionTbl].IsAReturn, [TransactionTbl].PaymentType from [TransactionTbl]" + ((fromDateTime.Equals(DateTime.MinValue) && toDateTime.Equals(DateTime.MinValue) && year == -1 && month == -1 && day == -1 && priceFrom == -1 && priceTo == -1) ? ";" : " where ");
                    bool firstTime = true;

                    List<Transaction> toReturnList = new List<Transaction>();

                    if (!fromDateTime.Equals(DateTime.MinValue))
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "Date >= " + fromDateTime;
                        firstTime = false;
                    }
                    if (!toDateTime.Equals(DateTime.MinValue))
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "Date <= " + toDateTime;
                        firstTime = false;
                    }
                    if (year != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "year(Date) == " + year;
                        firstTime = false;
                    }
                    if (month != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "month(Date) == " + month;
                        firstTime = false;
                    }
                    if (day != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "day(Date) == " + year;
                        firstTime = false;
                    }

                    sqlCommand = new SqlCommand(sqlQuery, myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                        toReturnList.Add(new Transaction((int)sqlReader[0], (int)sqlReader[1], sqlReader.GetDateTime(2), ((int)sqlReader[3]) == 0 ? Transaction.ReturnTypes.Purchase : Transaction.ReturnTypes.Returned, new List<Transaction.ProductInReceipt>(), new Transaction.PaymentMethod(sqlReader[4].ToString().Equals("visa") ? Transaction.paymentMethodTypes.visa : Transaction.paymentMethodTypes.masterCard, sqlReader[5].ToString())));
                    sqlReader.Close();

                    foreach (Transaction transaction in toReturnList)
                    {
                        sqlCommand = new SqlCommand("select [RecieptTbl].ProductID, [RecieptTbl].ProductName, [RecieptTbl].Quantity, [RecieptTbl].Price FROM RecieptTbl where [RecieptTbl].TransactionID = " + transaction.TransactionID + ";", myConnection);
                        sqlReader = sqlCommand.ExecuteReader();
                        List<Transaction.ProductInReceipt> reciept = new List<Transaction.ProductInReceipt>();
                        while (sqlReader.Read())
                            transaction.Receipt.Add(new Transaction.ProductInReceipt((int)sqlReader[0], sqlReader[1].ToString(), (int)sqlReader[2], (int)sqlReader[3]));
                        sqlReader.Close();
                    }

                    return toReturnList.Cast<T>().ToList();
                }
                else if (typeof(T) == typeof(Product))
                {
                    string sqlQuery = "select [ProductTbl].ID, [ProductTbl].Name, [ProductTbl].Type, [ProductTbl].Location, [ProductTbl].StockCount, [ProductTbl].Price, [ProductTbl].ImageUrl FROM ProductTbl" + ((fromDateTime.Equals(DateTime.MinValue) && toDateTime.Equals(DateTime.MinValue) && year == -1 && month == -1 && day == -1 && priceFrom == -1 && priceTo == -1) ? ";" : " where ");
                    bool firstTime = true;

                    List<Product> toReturnList = new List<Product>();

                    if (stockCountFrom != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "StockCount >= " + stockCountFrom;
                        firstTime = false;
                    }
                    if (stockCountTo != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "StockCount <= " + stockCountTo;
                        firstTime = false;
                    }
                    if (priceFrom != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "Price >= " + priceFrom;
                        firstTime = false;
                    }
                    if (month != -1)
                    {
                        sqlQuery += (firstTime ? "" : " and ") + "Price <= " + priceTo;
                        firstTime = false;
                    }

                    sqlCommand = new SqlCommand(sqlQuery, myConnection);
                    sqlReader = sqlCommand.ExecuteReader();
                    while (sqlReader.Read())
                    {
                        Product.productTypes type;
                        switch (sqlReader[2].ToString())
                        {
                            case "Uncategroized":
                                type = Product.productTypes.Uncategroized;
                                break;
                            case "Antiques":
                                type = Product.productTypes.Antiques;
                                break;
                            case "Art":
                                type = Product.productTypes.Art;
                                break;
                            case "Baby":
                                type = Product.productTypes.Baby;
                                break;
                            case "Books":
                                type = Product.productTypes.Books;
                                break;
                            case "Cameras":
                                type = Product.productTypes.Cameras;
                                break;
                            case "Cell":
                                type = Product.productTypes.Cell;
                                break;
                            case "Clothing":
                                type = Product.productTypes.Clothing;
                                break;
                            case "Computers":
                                type = Product.productTypes.Computers;
                                break;
                            case "Movies":
                                type = Product.productTypes.Movies;
                                break;
                            case "Motors":
                                type = Product.productTypes.Motors;
                                break;
                            case "Home":
                                type = Product.productTypes.Home;
                                break;
                            case "Garden":
                                type = Product.productTypes.Garden;
                                break;
                            case "Jewelry":
                                type = Product.productTypes.Jewelry;
                                break;
                            case "Watches":
                                type = Product.productTypes.Watches;
                                break;
                            case "Music":
                                type = Product.productTypes.Music;
                                break;
                            case "Pet":
                                type = Product.productTypes.Pet;
                                break;
                            case "Sport":
                                type = Product.productTypes.Sport;
                                break;
                            case "Stamps":
                                type = Product.productTypes.Stamps;
                                break;
                            case "Toys":
                                type = Product.productTypes.Toys;
                                break;
                            case "Else":
                                type = Product.productTypes.Else;
                                break;
                            case "Empty":
                                type = Product.productTypes.Empty;
                                break;
                            default:
                                type = Product.productTypes.Empty;
                                break;
                        }
                        toReturnList.Add(new Product((int)sqlReader[0], sqlReader[1].ToString(), type, sqlReader.IsDBNull(3) ? -1 : (int)sqlReader[3], (int)sqlReader[4], (int)sqlReader[5], sqlReader[6].ToString()));
                    }
                    sqlReader.Close();
                    return toReturnList.Cast<T>().ToList();
                }
                return new List<T>();
            }
            catch(Exception e)
            {
                return new List<T>();
            }
        }

        #endregion

        #region user functions

        public User UserCheckCredentials(User user)
        {
            SqlCommand getUser = new SqlCommand("SELECT ID, Username, Password, Type FROM [dbo].UserTbl WHERE Username = '" + user.Username + "' and Password = HASHBYTES('SHA1', '" + user.Password + "')", myConnection);
            sqlReader = getUser.ExecuteReader();
            if (sqlReader.Read())
            {
                string type = (string)sqlReader[3];
                User.PermissionType typeNumber = User.PermissionType.Admin;
                if (type.Equals("Admin")) { typeNumber = User.PermissionType.Admin; }
                if (type.Equals("Manager")) { typeNumber = User.PermissionType.Manager; }
                if (type.Equals("Worker")) { typeNumber = User.PermissionType.Worker; }
                if (type.Equals("Customer")) { typeNumber = User.PermissionType.Customer; }
                User newUser = new User((int)sqlReader[0], (string)sqlReader[1], (string)sqlReader[2], typeNumber);
                sqlReader.Close();
                return newUser;
            }
            sqlReader.Close();
            return null;
        }

        public User GetUser(User user)
        {
            SqlCommand getUser = new SqlCommand("SELECT ID, Username, Password, Type FROM [dbo].UserTbl WHERE Username = '" + user.Username + "'", myConnection);
            SqlDataReader reader = getUser.ExecuteReader();
            if (reader.Read())
            {
                string type = (string)reader[3];
                User.PermissionType typeNumber = User.PermissionType.Admin;
                if (type.Equals("Admin")){typeNumber = User.PermissionType.Admin;}
                if (type.Equals("Manager")){typeNumber = User.PermissionType.Manager;}
                if (type.Equals("Worker")){typeNumber = User.PermissionType.Worker;}
                if (type.Equals("Customer")) { typeNumber = User.PermissionType.Customer; }
                User newUser = new User((int)reader[0], (string)reader[1], (string)reader[2], typeNumber);
                reader.Close();
                return newUser;
            }
            reader.Close();
            return null;
        }
        public bool UserAny()
        {
            SqlCommand getUser = new SqlCommand("SELECT top 1 ID FROM [dbo].UserTbl", myConnection);
            SqlDataReader reader = getUser.ExecuteReader();
            if (reader.Read())
            {
                reader.Close();
                return true;
            }
            else
            {
                reader.Close();
                return false;
            }
        }

        #endregion
    }
}
