﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;

namespace Data_Access_Layer
{
    public interface Data_Access_Interface
    {
        /// <summary>
        /// Adds new data to memory.
        /// </summary>
        /// <typeparam name="T">Be aware! It can only be one of the known class types: Employee, ClubMember, Transaction, Product or Department. Put In Other Stuff and it won't work!</typeparam>
        /// <param name="t">Same T like above</param>
        void Add<T>(T t);
        /// <summary>
        /// Edit new data to memory.
        /// </summary>
        /// <typeparam name="T">Be aware! It can only be one of the known class types: Employee, ClubMember, Transaction, Product or Department. Put In Other Stuff and it won't work!</typeparam>
        /// <param name="t">Same T like above</param>
        void Edit<T>(T t);
        /// <summary>
        /// Removes data from memory.
        /// </summary>
        /// <typeparam name="T">Be aware! It can only be one of the known class types: Employee, ClubMember, Transaction, Product or Department. Put In Other Stuff and it won't work!</typeparam>
        /// <param name="ID">The ID of the element</param>
        void Remove<T>(int ID);
        /// <summary>
        /// Get data from memory by ID.
        /// </summary>
        /// <typeparam name="T">Be aware! It can only be one of the known class types: Employee, ClubMember, Transaction, Product or Department. Put In Other Stuff and it won't work!</typeparam>
        /// <param name="ID">The ID of the element</param>
        T Get<T>(int ID);
        /// <summary>
        /// Get data from memory by List of Type and Values.
        /// </summary>
        /// <typeparam name="T">Be aware! It can only be one of the known class types: Employee, ClubMember, Transaction, Product or Department. Put In Other Stuff and it won't work!</typeparam>
        /// <typeparam name="S">Be aware! It can only be one of the known class type fields: EmployeeFields, ClubMemberFields, TransactionFields, ProductFields or DepartmentFields. Put In Other Stuff and it won't work!</typeparam>
        /// <param name="typeValues"></param>
        /// <returns></returns>
        List<T> Get<T, S>(List<TypeValue<S>> typeValues);
        /// <summary>
        /// Get data from memory by given data.
        /// </summary>
        /// <typeparam name="T">Only supported on ClubMember and Transaction classes.</typeparam>
        /// <param name="fromDateTime">From this date, if dont want to use it, enter DateTime.MinValue.</param>
        /// <param name="toDateTime">To this date, if dont want to use it, enter DateTime.MinValue.</param>
        /// <param name="year">Query by year, if you don't want to use it, enter -1.</param>
        /// <param name="month">Query by month, if you don't want to use it, enter -1.</param>
        /// <param name="day">Query by day, if you don't want to use it, enter -1.</param>
        /// <returns>List of needed classes which apply the query.</returns>
        List<T> GetRangeByDateTime<T>(int stockCountFrom, int stockCountTo, DateTime fromDateTime, DateTime toDateTime, int year, int month, int day, int priceFrom, int priceTo);
        /// <summary>
        /// Check if user data is empty.
        /// </summary>
        /// <returns>boolean indicating user data is empty.</returns>
        bool UserAny();
        /// <summary>
        /// Get list of all users in database.
        /// </summary>
        /// <returns>List of a users in database</returns>
        //List<User> GetAllUsers();
        //List<T> GetAll<T>();
        User GetUser(User user);
        User UserCheckCredentials(User user);
    }
    public enum EmployeeFields { TehudatZehut, firstName, lastName, departmentID, salary, gender, supervisor }
    public enum ClubMemberFields { memberID, teudatZehutID, firstName, lastName, gender, dateOfBirth, payment }
    public enum CustomerFields { memberID, teudatZehutID, firstName, lastName, payment }
    public enum TransactionFields { transactionID, dateOfBirth, isAReturn, PaymentMethod }
    public enum ProductFields { inventoryID, name, type, location, inStock, stockCount, price }
    public enum DepartmentFields { name, id }
    public class TypeValue<S>
    {
        S type;
        public S Type
        {
            get { return type; }
            set { type = value; }
        }

        object value;
        public object Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

        public TypeValue(S type, object value)
        {
            this.type = type;
            this.value = value;
        }
    }
}
