﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend
{
    [Serializable()]
    public class Department
    {
        #region fields, getters and setters

        private int id;
        public int ID
        {
            get { return id; }
            set { id = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        #endregion

        public Department(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public Department(string name)
        {
            this.id = -1;
            this.name = name;
        }

        public bool Equals(Department otherDepartment)
        {
            return (ID == otherDepartment.ID);
        }

        public override string ToString()
        {
            return "Department ID: " + id + "\nName: " + name;
        }
    }
}
