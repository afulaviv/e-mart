﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend
{
    public class ReturnMessage
    {
        private bool success;
        public bool Success
        {
            get { return success; }
            set { success = value; }
        }

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        public ReturnMessage(bool success, string message)
        {
            this.success = success;
            this.message = message;
        }
    }
}
