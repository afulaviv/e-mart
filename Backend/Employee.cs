﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Backend
{
    [Serializable()]
    public class Employee : User
    {
        #region fields, getters and setters

        private int tehudatZehut;
        public int TehudatZehut
        {
            get { return tehudatZehut; }
            set { tehudatZehut = value; }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        
        private int departmentID;
        public int DepartmentID
        {
            get { return departmentID; }
            set { departmentID = value; }
        }

        private int salary;
        public int Salary
        {
            get { return salary; }
            set { salary = value; }
        }

        public enum GenderTypes { Empty, male, female };
        private GenderTypes gender;
        public GenderTypes Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        private List<Employee> managerInCharge;
        public List<Employee> ManagerInCharge
        {
            get { return managerInCharge; }
            set { managerInCharge = value; }
        }

        private int superVisorID;
        public int SuperVisorID
        {
            get { return superVisorID; }
            set { superVisorID = value; }
        }

        #endregion

        public Employee(int id, string userName, string password, User.PermissionType type, int TehudatZehut, string firstName, string lastName, int departmentID, int salary, GenderTypes gender, int superVisorID)
            : base(id, userName, password, type)
        {
            this.TehudatZehut = TehudatZehut;
            this.firstName = firstName;
            this.lastName = lastName;
            this.departmentID = departmentID;
            this.salary = salary;
            this.gender = gender;
            this.superVisorID = superVisorID;
            this.managerInCharge = new List<Employee>();
        }

        public bool Equals(Employee otherEmployee)
        {
            return (TehudatZehut == otherEmployee.TehudatZehut);
        }

        public override string ToString()
        {
            return "Teudat zehute ID: " + TehudatZehut + "\nFirst name: " + firstName + "\nLast name: " + lastName + "\nDepartment ID: " + departmentID + "\nSalary: " + salary + "\nGender: " + gender + "\nSupervisor ID: " + superVisorID;
        }
    }
}
