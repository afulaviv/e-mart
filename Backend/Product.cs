﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.IO;
using System.Drawing;


namespace Backend
{
    [Serializable()]
    public class Product
    {
        #region fields, getters and setters

        private int inventoryID;
        public int InventoryID
        {
            get { return inventoryID; }
            set { inventoryID = value; }
        }
        
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        
        // TODO: create actual list of product types
        public enum productTypes { Uncategroized, Antiques, Art, Baby, Books, Cameras, Cell , Clothing, Computers, Movies, Motors, Home, Garden, Jewelry, Watches, Music, Pet, Sport, Stamps, Toys, Else, Empty}
        private productTypes type;
        public productTypes Type
        {
            get { return type; }
            set { type = value; }
        }
        
        // departmentID
        private int location;
        public int Location
        {
            get { return location; }
            set { location = value; }
        }

        public bool InStock 
        {
            get 
            {
                if (stockCount <= 0)
                    return false;
                else
                    return true;
            }
        }
        
        private int stockCount;
        public int StockCount
        {
            get { return stockCount; }
            set { stockCount = value; }
        }
        
        private int price;
        public int Price
        {
            get { return price; }
            set { price = value; }
        }

        private string imageURL;
        public string ImageURL
        {
            get { return imageURL; }
            set { imageURL = value;  }
        }

        #endregion

        public Product(int inventoryID, string name, productTypes type, int location, int stockCount, int price)
        {
            this.inventoryID = inventoryID;
            this.name = name;
            this.type = type;
            this.location = location;
            this.stockCount = stockCount;
            this.price = price;
            this.imageURL = System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName + "\\media\\noPreview.jpg";
            //this.image = Bitmap.FromFile(System.IO.Directory.GetParent(System.IO.Directory.GetCurrentDirectory()).Parent.FullName + "\\media\\noPreview.jpg");
        }

        public Product(int inventoryID, string name, productTypes type, int location, int stockCount, int price, string imageURL)
        {
            this.inventoryID = inventoryID;
            this.name = name;
            this.type = type;
            this.location = location;
            this.stockCount = stockCount;
            this.price = price;
            this.imageURL = imageURL;
        }

        public Product(Product product)
        {
            this.inventoryID = product.inventoryID;
            this.name = product.name;
            this.type = product.type;
            this.location = product.location;
            this.stockCount = product.stockCount;
            this.price = product.price;
            this.imageURL = product.imageURL;
        }

        public bool Equals(Product otherProduct)
        {
            return (name == otherProduct.name);
        }

        public override string ToString()
        {
            bool inStock = false;
            if (stockCount > 0)
                inStock = true;
            return "Inventory ID: " + inventoryID + "\nName: " + name + "\nType: " + type + "\nLocation (Department ID): " + location + "\nIn stock: " + inStock + "\nStock Count: " + stockCount + "\nPrice: " + price;
        }
    }
}
