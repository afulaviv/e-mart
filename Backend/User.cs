﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Backend
{
    [Serializable()]
    public class User
    {
        public enum PermissionType { Admin, Manager, Worker, Customer }
        public enum functionsType {Add_User, Remove_or_Edit_Employee, Add_or_Edit_Product, Add_Transaction, Add_or_Edit_Department, See_Profile, Query, Cart, Map_of_Stores, Edit_User, Edit_Customer, Edit_Club_Member}

        #region fields, getters and setters
        private int id;
        public int Id
        {
            get { return id; }
            set { id = value; }
        }

        private string username;
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

        private PermissionType type;
        public PermissionType Type
        {
          get { return type; }
          set { type = value; }
        }
        #endregion



        public User(int id, string username, string password)
        {
            this.id = id;
            this.username = username;
            this.password = password;
        }

        public User(int id, string username, string password, PermissionType type)
        {
            this.id = id;
            this.username = username;
            this.password = password;
            this.type = type;
        }

        public bool Equals(User otherUser)
        {
            return (username == otherUser.username);
        }
        
        public List<functionsType> getFunctions()
        {
            List<functionsType> functions = new List<functionsType>();
            switch (type)
            {
                case(PermissionType.Admin):
                    foreach (functionsType f in Enum.GetValues(typeof(functionsType)))
                    {
                        if (!f.Equals(functionsType.Cart) && !f.Equals(functionsType.Edit_Club_Member) && !f.Equals(functionsType.Edit_Customer))
                            functions.Add(f);
                    }
                    break;
                case(PermissionType.Manager):
                    functions.Add(functionsType.Add_or_Edit_Department);
                    functions.Add(functionsType.Add_or_Edit_Product);
                    functions.Add(functionsType.Query);
                    functions.Add(functionsType.See_Profile);
                    functions.Add(functionsType.Map_of_Stores);
                    break;
                case(PermissionType.Worker):
                    functions.Add(functionsType.Add_or_Edit_Product);
                    functions.Add(functionsType.See_Profile);
                    functions.Add(functionsType.Map_of_Stores);
                    break;
                case (PermissionType.Customer):
                    functions.Add(functionsType.Edit_Customer);
                    functions.Add(functionsType.Edit_Club_Member);
                    functions.Add(functionsType.See_Profile);
                    functions.Add(functionsType.Cart);
                    functions.Add(functionsType.Map_of_Stores);
                    break;
            }
            return functions;
        }
        public override string ToString()
        {
            return "Username: " + username;
        }
    }
}
