﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend
{
    [Serializable()]
    public class ClubMember : Customer
    {
        #region fields, getters and setters
        
        private int tehudatZehut;
        public int TehudatZehut
        {
            get { return tehudatZehut; }
            set { tehudatZehut = value; }
        }

        public enum GenderTypes { male, female, Empty };
        private GenderTypes gender;
        public GenderTypes Gender
        {
            get { return gender; }
            set { gender = value; }
        }
        
        private DateTime dateOfBirth;
        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }

        #endregion

        public ClubMember(int id, string userName, string password, User.PermissionType type, int TehudatZehut, string firstName, string lastName, List<int> transactionHistory, GenderTypes gender, DateTime dateOfBirth, PaymentMethod payment, List<Customer.ProductIdQuantity> cart)
            : base(id, userName, password, type, firstName, lastName, transactionHistory, payment, cart)
        {
            this.TehudatZehut = TehudatZehut;
            this.gender = gender;
            this.dateOfBirth = dateOfBirth;
        }

        public ClubMember(int id, string userName, string password, User.PermissionType type, int TehudatZehut, string firstName, string lastName, GenderTypes gender, DateTime dateOfBirth, PaymentMethod payment)
            : base(id, userName, password, type, firstName, lastName, new List<int>(), payment, new List<Customer.ProductIdQuantity>())
        {
            this.TehudatZehut = TehudatZehut;
            this.gender = gender;
            this.dateOfBirth = dateOfBirth;
        }

        public bool Equals(ClubMember otherClubMember)
        {
            return (TehudatZehut == otherClubMember.TehudatZehut);
        }

        public override string ToString()
        {
            string ans = "Tehudat zehute (ID): " + TehudatZehut + "\nFirst name: " + base.FirstName + "\nLast name: " + base.LastName + "\nTransaction History:";

            if (base.TransactionHistory != null)
                if (base.TransactionHistory.Count > 0)
                    for (int i = 0; i < base.TransactionHistory.Count; i++)
                    {
                        ans += "\n\t" + i + " " + base.TransactionHistory[i].ToString();
                    }
                else
                    ans += " There is no transaction History";
            else
                ans += " There is no transaction History";
            ans += "\nGender: " + gender + "\nDate of Birth: " + dateOfBirth;

                return ans;
        }
    }
}
