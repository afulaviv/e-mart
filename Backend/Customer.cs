﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Backend
{
    [Serializable()]
    public class Customer : User
    {
        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        private List<int> transactionHistory;
        public List<int> TransactionHistory
        {
            get { return transactionHistory; }
            set { transactionHistory = value; }
        }

        public enum paymentMethodTypes { Empty, visa, masterCard }
        [Serializable()]
        public class PaymentMethod
        {
            private paymentMethodTypes paymentMethodType;
            public paymentMethodTypes PaymentMethodType
            {
                get { return paymentMethodType; }
                set { paymentMethodType = value; }
            }

            private string additionInfo;
            public string AdditionInfo
            {
                get { return additionInfo; }
                set { additionInfo = value; }
            }

            public PaymentMethod(paymentMethodTypes paymentMethodType, string additionInfo)
            {
                this.PaymentMethodType = paymentMethodType;
                this.AdditionInfo = additionInfo;
            }

            public override bool Equals(Object other)
            {
                return ((other.GetType() == typeof(PaymentMethod)) && (paymentMethodType == ((PaymentMethod)other).paymentMethodType && additionInfo.Equals(((PaymentMethod)other).additionInfo)));
            }
        }
        private PaymentMethod payment;
        public PaymentMethod Payment
        {
            get { return payment; }
            set { payment = value; }
        }

        public class ProductIdQuantity
        {
            private int productID;
            public int ProductID
            {
              get { return productID; }
              set { productID = value; }
            }

            private int quantity;
            public int Quantity
            {
              get { return quantity; }
              set { quantity = value; }
            }

            public ProductIdQuantity(int productID, int quantity)
            {
                this.productID = productID;
                this.quantity = quantity;
            }
        }
        private List<ProductIdQuantity> cart;
        public List<ProductIdQuantity> Cart
        {
            get { return cart; }
            set { cart = value; }
        }

        private Transaction transaction;
        public Transaction Transaction
        {
            get { return transaction; }
            set { transaction = value; }
        }
        public Customer(int id, string userName, string password, User.PermissionType type, string firstName, string lastName, List<int> transactionHistory, PaymentMethod payment, List<ProductIdQuantity> cart)
            : base(id, userName, password, type)
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.transactionHistory = transactionHistory;
            this.payment = payment;
            this.cart = cart;
        }
    }
}
