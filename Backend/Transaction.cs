﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backend
{
    [Serializable()]
    public class Transaction
    {
        #region fields, getters and setters

        private int transactionID;
        public int TransactionID
        {
            get { return transactionID; }
            set { transactionID = value; }
        }

        private int customerID;
        public int CustomerID
        {
            get { return customerID; }
            set { customerID = value; }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }

        public enum ReturnTypes { Returned, Purchase, Empty}
        private ReturnTypes isAReturn;
        public ReturnTypes IsAReturn
        {
            get { return isAReturn; }
            set { isAReturn = value; }
        }
        
        private List<ProductInReceipt> receipt;
        public List<ProductInReceipt> Receipt
        {
            get { return receipt; }
            set { receipt = value; }
        }
        [Serializable()]
        public class ProductInReceipt
        {
            private int inventoryID;
            public int InventoryID
            {
                get { return inventoryID; }
                set { inventoryID = value; }
            }
            
            private string productName;
            public string ProductName
            {
                get { return productName; }
                set { productName = value; }
            }

            private int quantity;
            public int Quantity
            {
                get { return quantity; }
                set { quantity = value; }
            }
            
            private int price;
            public int Price
            {
                get { return price; }
                set { price = value; }
            }

            public ProductInReceipt(int inventoryID, string productName, int quantity, int price)
            {
                this.inventoryID = inventoryID;
                this.productName = productName;
                this.quantity = quantity;
                this.price = price;
            }
        }
        
        public enum paymentMethodTypes { Empty, visa, masterCard }
        [Serializable()]
        public class PaymentMethod
        {
            private paymentMethodTypes paymentMethodType;
            public paymentMethodTypes PaymentMethodType
            {
                get { return paymentMethodType; }
                set { paymentMethodType = value; }
            }

            private string additionInfo;
            public string AdditionInfo
            {
                get { return additionInfo; }
                set { additionInfo = value; }
            }

            public PaymentMethod(paymentMethodTypes paymentMethodType, string additionInfo)
            {
                this.PaymentMethodType = paymentMethodType;
                this.AdditionInfo = additionInfo;
            }
            
            public override bool Equals(Object other)
            {
                return ((other.GetType() == typeof(PaymentMethod)) && paymentMethodType == ((PaymentMethod) other).paymentMethodType && additionInfo.Equals(((PaymentMethod) other).additionInfo));
            }
        }
        private PaymentMethod payment;
        public PaymentMethod Payment
        {
            get { return payment; }
            set { payment = value; }
        }

#endregion

        public Transaction(int transactionID, int customerID, DateTime date, ReturnTypes isAReturn, List<ProductInReceipt> receipt, PaymentMethod payment)
        {
            this.transactionID = transactionID;
            this.customerID = customerID;
            this.date = date;
            this.isAReturn = isAReturn;
            this.receipt = receipt;
            this.payment = payment; 
        }

        public Transaction(DateTime date, int customerID, ReturnTypes isAReturn, List<ProductInReceipt> receipt, PaymentMethod payment)
        {
            this.transactionID = -1;
            this.customerID = customerID;
            this.date = date;
            this.isAReturn = isAReturn;
            this.receipt = receipt;
            this.payment = payment;
        }

        public override bool Equals(Object otherTransaction)
        {
            return ((otherTransaction.GetType() == typeof(Transaction)) && (transactionID == (((Transaction) otherTransaction).transactionID)));
        }
    }
}
