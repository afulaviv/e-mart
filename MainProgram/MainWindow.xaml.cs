﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using PresentationLayer;
using Buisness_Layer;
using Data_Access_Layer;

namespace MainProgram
{
    /// <summary>
    /// Interaction logic for Main.xaml
    /// </summary>
    public partial class Main : Window
    {
        public Main()
        {
            InitializeComponent();
            Data_Access_Interface dal = new Data_Access();
            Buisness_Interface bl = new Buisness(dal);
            this.Hide();
            ScreenInterface currentScreen = new LogonScreen(bl);
            currentScreen.Run();
        }
    }
}
